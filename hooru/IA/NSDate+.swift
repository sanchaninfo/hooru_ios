//
//  NSDate+.swift
//  InspiringApps
//
//  Created by Brad Weber on 7/5/16.
//  Copyright © 2016 InspiringApps. All rights reserved.
//

import Foundation

extension Date {
	fileprivate static let sharedDBDateTimeFormatter = DateFormatter()

	static func dateFromDatabaseDateTime(_ datebaseDateTime: String?) -> Date? {
		guard datebaseDateTime != nil else {
			return nil
		}
		
		sharedDBDateTimeFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		sharedDBDateTimeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

		return sharedDBDateTimeFormatter.date(from: datebaseDateTime!)
	}


	/// weekday: 1 - 7 (Sunday - Saturday)
	func lastWeekday(_ weekday: Int) -> Date {
		let calendar = Calendar(identifier: Calendar.Identifier.gregorian)

		var components = (calendar as NSCalendar).components([.year, .month, .day], from: self)
		components.hour = 0
		components.minute = 0
		components.second = 0

		var currentDate = calendar.date(from: components)!
		var currentWeekday = (calendar as NSCalendar).component(.weekday, from: currentDate)

		while currentWeekday != weekday {
			currentDate = (calendar as NSCalendar).date(byAdding: .day, value: -1, to: currentDate, options: NSCalendar.Options(rawValue: 0))!
			currentWeekday = (calendar as NSCalendar).component(.weekday, from: currentDate)
		}

		return currentDate
	}

	func weekdayInTimeZone(_ timeZone: TimeZone? = nil) -> Int {
		// Default to the current device timezone if none is passed
		var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
		let tz = timeZone ?? TimeZone.autoupdatingCurrent

		calendar.timeZone = tz
		let weekday = (calendar as NSCalendar).component(.weekday, from: self)

		return weekday
	}

	func databaseDateString() -> String {
		Date.sharedDBDateTimeFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		Date.sharedDBDateTimeFormatter.dateFormat = "yyyy-MM-dd"

		return Date.sharedDBDateTimeFormatter.string(from: self)
	}

	func databaseDateTimeString() -> String {
		Date.sharedDBDateTimeFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		Date.sharedDBDateTimeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

		return Date.sharedDBDateTimeFormatter.string(from: self)
	}

	func shortDateString() -> String {
		Date.sharedDBDateTimeFormatter.dateStyle = .short
		Date.sharedDBDateTimeFormatter.timeStyle = .none

		return Date.sharedDBDateTimeFormatter.string(from: self)
	}

	// MARK: - Relative Time
	func relativeString(_ format: RelativeFormat) -> String {
		return self.relativeTime().stringWithFormat(format)
	}

	enum RelativeFormat {
		case extraShort, short, long

		func suffix(plural: Bool) -> String {
			switch self {
			case .extraShort:
				return ""
			case .short, .long:
				return plural ? "s ago" : " ago"
			}
		}
	}

	// relatively private
	private static let hour: Double = 60 * 60
	private static let day: Double = hour * 24
	private static let month: Double = day * 31
	private static let year: Double = month * 12

	private static func relativeTime(_ offset: TimeInterval) -> RelativeTimeResult {
		var minutes = 0
		var hours = 0
		var days = 0
		var months = 0
		var years = 0

		switch offset {
		case 0 ..< hour:		minutes = Int(offset / 60.0)
		case hour ..< day:		hours = Int(offset / hour)
		case day ..< month:		days = Int(offset / day)
		case month ..< year:	months = Int(offset / month)
		default:				years = Int(offset / year)
		}

		return RelativeTimeResult(minutes: minutes, hours: hours, days: days, months: months, years: years)
	}

	private struct RelativeTimeResult {
		let minutes: Int
		let hours: Int
		let days: Int
		let months: Int
		let years: Int

		func stringWithFormat(_ format: RelativeFormat) -> String {

			guard minutes + hours + days + months + years > 0 else {
				return "now"
			}

			let component: RelativeTimeComponent
			let value: Int

			switch (minutes, hours, days, months, years) {
			case (1..<60, 0, 0, 0, 0):
				component = .minute
				value = minutes
			case (0, 1..<60, 0, 0, 0):
				component = .hour
				value = hours
			case (0, 0, 1...31, 0, 0):
				component = .day
				value = days
			case (0, 0, 0, 1...12, 0):
				component = .month
				value = months
			default:
				component = .year
				value = years
			}

			return "\(value)\(component.name(format: format))\(format.suffix(plural: value != 1))"
		}
}

	private func relativeTime() -> RelativeTimeResult {
		return Date.relativeTime(-self.timeIntervalSinceNow)
	}

	private enum RelativeTimeComponent: String {
		case second, minute, hour, day, week, month, year

		func name(format: RelativeFormat) -> String {
			switch format {
			case .extraShort:
				return String("\(self)".prefix(1))
			case .short:
				return String(" \(self)".prefix(1))
			case .long:
				return " \(self)"
			}
		}
	}

}
