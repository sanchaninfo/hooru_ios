//
//  UIImage+.swift
//  InspiringApps
//
//  Created by Brad Weber on 6/16/16.
//  Copyright © 2016 InspiringApps. All rights reserved.
//

import UIKit

extension UIImage {

	func tintedWithColor(_ color: UIColor) -> UIImage? {
		let size = self.size

		UIGraphicsBeginImageContextWithOptions(size, false, 0)

		color.setFill()
		UIRectFill(CGRect(origin: CGPoint.zero, size: size))
		self.draw(in: CGRect(origin: .zero, size: size), blendMode: .lighten, alpha: 1.0) // overlay

		let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()

		return tintedImage
	}

	func asMask() -> CGImage? {
		// This method is not returning a correct mask image
		// on Wide Color displays. It seems to perhaps be greyscale
		// as the resulting masked image looks faded.
		guard let imageRef = self.cgImage else {
			return nil
		}

		/*
		let colorSpace = CGColorSpaceCreateDeviceRGB()
		let mask = CGImage(width: imageRef.width,
		                height: imageRef.height,
		                bitsPerComponent: imageRef.bitsPerComponent,
		                bitsPerPixel: imageRef.bitsPerPixel,
		                bytesPerRow: imageRef.bytesPerRow,
		                space: imageRef.colorSpace ?? colorSpace,
		                bitmapInfo: imageRef.bitmapInfo,
		                provider: imageRef.dataProvider!,
		                decode: nil,
		                shouldInterpolate: true,
		                intent: .defaultIntent)
		*/

		let mask = CGImage(maskWidth: imageRef.width,
		                             height: imageRef.height,
		                             bitsPerComponent: imageRef.bitsPerComponent,
		                             bitsPerPixel: imageRef.bitsPerPixel,
		                             bytesPerRow: imageRef.bytesPerRow,
		                             provider: imageRef.dataProvider!, decode: nil, shouldInterpolate: true)

		return mask
	}

	func centeredSquare(_ sideLength: CGFloat) -> UIImage {
		let size = CGSize(width: sideLength, height: sideLength)

		UIGraphicsBeginImageContextWithOptions(size, false, 0)
		self.draw(in: CGRect(origin: .zero, size: size))

		if let scaledImage = UIGraphicsGetImageFromCurrentImageContext() {
			UIGraphicsEndImageContext()
			return scaledImage
		} else {
			UIGraphicsEndImageContext()
			return UIImage()
		}
	}

	func squared() -> UIImage {

		let size = self.size
		let side = min(size.width, size.height)
		let offset = abs(size.width - size.height) / 2

		let drawOrigin = size.width > size.height ? CGPoint(x: offset, y: 0) : CGPoint(x: 0, y: offset)
		let drawRect = CGRect(origin: drawOrigin, size: CGSize(width: side, height: side))

		if let imageRef = self.cgImage, let croppedImageRef = imageRef.cropping(to: drawRect) {
			return UIImage(cgImage: croppedImageRef, scale: self.scale, orientation: self.imageOrientation)
		}

		return UIImage()
	}


}
