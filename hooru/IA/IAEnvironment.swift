//
//  IAEnvironment.swift
//  InspiringApps
//
//  Created by Brad Weber on 5/12/15.
//  Copyright (c) 2015 InspiringApps. All rights reserved.
//

import Foundation
import UIKit

class IAEnvironment {
	// MARK: Device
	fileprivate static let DeviceList = [
		/* iPod 5 */          "iPod5,1": "iPod Touch 5",
		/* iPod 6 */          "iPod7,1": "iPod Touch 6",
		/* iPhone 4 */        "iPhone3,1":  "iPhone 4", "iPhone3,2": "iPhone 4", "iPhone3,3": "iPhone 4",
		/* iPhone 4S */       "iPhone4,1": "iPhone 4S",
		/* iPhone 5 */        "iPhone5,1": "iPhone 5", "iPhone5,2": "iPhone 5",
		/* iPhone 5C */       "iPhone5,3": "iPhone 5C", "iPhone5,4": "iPhone 5C",
		/* iPhone 5S */       "iPhone6,1": "iPhone 5S", "iPhone6,2": "iPhone 5S",
		/* iPhone 6 */        "iPhone7,2": "iPhone 6",
		/* iPhone 6 Plus */   "iPhone7,1": "iPhone 6 Plus",
		/* iPhone 6S */       "iPhone8,1": "iPhone 6S",
		/* iPhone 6S Plus */  "iPhone8,2": "iPhone 6S Plus",
		/* iPhone 7 */        "iPhone9,1": "iPhone 7", "iPhone9,3": "iPhone 7",
		/* iPhone 7 Plus */   "iPhone9,2": "iPhone 7 Plus", "iPhone9,4": "iPhone 7 Plus",
		/* iPhone SE */       "iPhone8,4": "iPhone SE",
		/* iPad 2 */          "iPad2,1": "iPad 2", "iPad2,2": "iPad 2", "iPad2,3": "iPad 2", "iPad2,4": "iPad 2",
		/* iPad 3 */          "iPad3,1": "iPad 3", "iPad3,2": "iPad 3", "iPad3,3": "iPad 3",
		/* iPad 4 */          "iPad3,4": "iPad 4", "iPad3,5": "iPad 4", "iPad3,6": "iPad 4",
		/* iPad Air */        "iPad4,1": "iPad Air", "iPad4,2": "iPad Air", "iPad4,3": "iPad Air",
		/* iPad Air 2 */      "iPad5,3": "iPad Air 2", "iPad5,4": "iPad Air 2",
		/* iPad Mini */       "iPad2,5": "iPad Mini", "iPad2,6": "iPad Mini", "iPad2,7": "iPad Mini",
		/* iPad Mini 2 */     "iPad4,4": "iPad Mini 2", "iPad4,5": "iPad Mini 2", "iPad4,6": "iPad Mini 2",
		/* iPad Mini 3 */     "iPad4,7": "iPad Mini 3", "iPad4,8": "iPad Mini 3", "iPad4,9": "iPad Mini 3",
		/* iPad Mini 4 */     "iPad5,1": "iPad Mini 4", "iPad5,2": "iPad Mini 4",
		/* iPad Pro */        "iPad6,3": "iPad Pro", "iPad6,4": "iPad Pro", "iPad6,7": "iPad Pro", "iPad6,8": "iPad Pro",
		/* Simulator */       "x86_64": "Simulator", "i386": "Simulator"
	]


	static var deviceIdentifier: String {
		return UIDevice.current.identifierForVendor?.uuidString ?? ""
	}

	static var deviceArchitecture: String {
		var architecture = "unknown"

		#if arch(i386)
			architecture = "x86"
		#endif

		#if arch(x86_64)
			architecture = "x86_64"
		#endif

		#if arch(arm)
			architecture = "ARM"
			#endif

		#if arch(arm64)
			architecture = "ARM_64"
		#endif

		return architecture
	}

	static var deviceLocale: String {
		return "\(Locale.current.identifier); \((TimeZone.autoupdatingCurrent as NSTimeZone).description)"
	}

	static var deviceManufacturer: String {
		return "Apple"
	}

	static var deviceModel: String {
		var systemInfo = utsname()
		uname(&systemInfo)

		let machine = systemInfo.machine
		let machineMirror = Mirror(reflecting: machine)

		let identifier = machineMirror.children.reduce("") { identifier, element in
			guard let value = element.value as? Int8, value != 0 else {
				return identifier
			}

			return identifier + String(UnicodeScalar(UInt8(value)))
		}

		return DeviceList[identifier] ?? identifier
	}


	// MARK: Operating System
	static var osArchitecture: String {
		if MemoryLayout<Int>.size == MemoryLayout<Int64>.size {
			return "64bit"
		}

		return "32bit"
	}

	static var osName: String {
		return UIDevice.current.systemName
	}

	static var osVersion: String {
		return UIDevice.current.systemVersion
	}


	// MARK: App
	static var appVersion: String {
		if let infoDictionary = Bundle.main.infoDictionary {
			if let version = infoDictionary["CFBundleShortVersionString"] as? String {
				return version
			}
		}

		return "unknown"
	}
}
