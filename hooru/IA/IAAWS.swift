//
//  IAAWS.swift
//  InspiringApps
//
//  Created by Brad Weber on 12/23/16.
//  Copyright © 2016 InspiringApps. All rights reserved.
//

import Foundation

struct IAAWS {
	private static let dateFormatter = DateFormatter()

	static var defaultAccessKey: String?
	static var defaultSecretKey: String?

	var accessKey: String?
	var secretKey: String?
	var regionName: String
	var serviceName: String

	init(accessKey: String?, secretKey: String?, regionName: String, serviceName: String) {
		self.accessKey = accessKey ?? IAAWS.defaultAccessKey
		self.secretKey = secretKey ?? IAAWS.defaultSecretKey
		self.regionName = regionName
		self.serviceName = serviceName
	}

	func signedHeaders(url: URL, httpMethod: String, date: Date, payloadHash: String) -> [String: String] {
		var headers = [
			"x-amz-content-sha256": payloadHash,
			"x-amz-date": date.databaseDateTimeISOString(),
			// "x-amz-acl" : "public-read",
			"Host": url.host!,
			]
		headers["Authorization"] = authorizationHeader(url: url, headers: headers, date: date, httpMethod: httpMethod, payloadHash: payloadHash)

		printMessage("signedHeaders: \(headers)")

		return headers
	}


	// MARK: - Private
	fileprivate func pathForURL(_ url: URL) -> String {
		let path = url.path.isEmpty ? "/" : url.path

		printMessage("path: \(path)")

		return path
	}

	fileprivate func scope(date: Date) -> String {
		let scope = [date.databaseDateCompactString(),
		             regionName,
		             serviceName,
		             "aws4_request"].joined(separator: "/")

		printMessage("scope: \(scope)")

		return scope
	}

	fileprivate func signedHeaders(_ headers: [String:String]) -> String {
		var signed = headers.keys.map { (key) in
			key.lowercased()
			}.sorted()

		if let index = signed.index(of: "authorization") {
			signed.remove(at: index)
		}

		let s = signed.joined(separator: ";")

		printMessage("signedHeaders: \(s)")

		return s
	}

	fileprivate func cononicalHeaders(_ headers: [String: String]) -> String {
		var cononical = [String]()
		let keys = headers.keys.sorted() { (key1, key2) -> Bool in
			return key1.localizedCompare(key2) == ComparisonResult.orderedAscending
		}

		for key in keys {
			if key.caseInsensitiveCompare("authorization") != ComparisonResult.orderedSame {
				// Note: This does not strip whitespace, but the spec says it should
				cononical.append("\(key.lowercased()):\(headers[key]!)")
			}
		}

		let c = cononical.joined(separator: "\n")

		printMessage("cononicalHeaders: \(c)")

		return c
	}

	fileprivate func cononicalRequest(httpMethod: String, url: URL, queryString: String, headers: [String: String], hashedPayload: String) -> String {
		let request = [httpMethod,
		               pathForURL(url),
		               queryString,
		               "\(cononicalHeaders(headers))\n",
			signedHeaders(headers),
			hashedPayload].joined(separator: "\n")

		printMessage("cononicalRequest: \(request)")

		return request
	}


	fileprivate func signatureKey(date: Date) -> Data {
		// http://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html#signature-v4-examples-jscript

		guard secretKey != nil else {
			print("IAAWS: No secretKey defined")
			return Data()
		}

		guard let dateKey = date.databaseDateCompactString().hmac256(key: ("AWS4" + secretKey!).data(using: String.Encoding.utf8)!) else {
			return Data()
		}

		guard let dateRegionKey = regionName.hmac256(key: dateKey) else {
			return Data()
		}

		guard let dateRegionServiceKey = serviceName.hmac256(key: dateRegionKey) else {
			return Data()
		}

		guard let signingKey = "aws4_request".hmac256(key: dateRegionServiceKey) else {
			return Data()
		}

		let s = signingKey

		printMessage("signatureKey: \(s.toHexString())")

		return s
	}

	fileprivate func stringToSign(date: Date, cononicalRequest: String) -> String {
		let signedRequest = cononicalRequest.sha256Hex() ?? ""

		let string = ["AWS4-HMAC-SHA256", // algorithm
			date.databaseDateTimeISOString(),
			scope(date: date),
			signedRequest].joined(separator: "\n")

		printMessage("stringToSign: \(string)")

		return string
	}


	fileprivate func authorizationHeader(url: URL, headers: [String: String], date: Date, httpMethod: String, payloadHash: String) -> String {
		guard accessKey != nil else {
			print("IAAWS: No accessKey defined")
			return ""
		}

		let credentials: String = [accessKey!,
		                           scope(date: date)].joined(separator: "/")

		let cononicalRequest = self.cononicalRequest(httpMethod: httpMethod, url: url, queryString: "", headers: headers, hashedPayload: payloadHash)

		let stringToSign = self.stringToSign(date: date, cononicalRequest: cononicalRequest)
		let signatureKey = self.signatureKey(date: date)

		let signature = stringToSign.hmac256(key: signatureKey)?.toHexString() ?? ""

		let h = [
			"AWS4-HMAC-SHA256 Credential=\(credentials)",
			"SignedHeaders=\(signedHeaders(headers))",
			"Signature=\(signature)",
			].joined(separator: ", ")

		return h
	}

	func printMessage(_ message: String) {
		IAAWS.printMessage(message)
	}

	static func printMessage(_ message: String) {
		#if DEBUG
//			print(message)
		#endif
	}

	static func test() {
//		let accessKey = ""
//		let secretKey = ""
//		let aws = IAAWS(accessKey: accessKey, secretKey: secretKey, regionName: "us-west-2", serviceName: "s3")
//
//		let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//		IAAWSS3.retrieveFileFromBucket(aws: aws, bucketName: "ia-coors", serverFilePath: "/development/data/coors/venue/1cf72bbd3de701bbd5be73cb700c4f78.png", toLocalURL: url) { (success) in
//			print(success)
//		}
	}
}
