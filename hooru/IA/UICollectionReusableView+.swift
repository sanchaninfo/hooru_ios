//  hooru
//  Created by Erwin Mazariegos on 12/15/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

extension UICollectionReusableView {

	static var identifier: String {
		return "\(self)"
	}

}

