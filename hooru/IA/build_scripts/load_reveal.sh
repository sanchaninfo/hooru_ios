# example code to put in xcode build script: "$SRCROOT/hooru/IA/build_scripts/load_reveal.sh" (with the quotes)

REVEAL_APP_PATH=$(mdfind kMDItemCFBundleIdentifier="com.ittybittyapps.Reveal2" | head -n 1)
BUILD_SCRIPT_PATH="${REVEAL_APP_PATH}/Contents/SharedSupport/Scripts/reveal_server_build_phase.sh"
if [ "${REVEAL_APP_PATH}" -a -e "${BUILD_SCRIPT_PATH}" ]; then
	echo ${BUILD_SCRIPT_PATH}
	"${BUILD_SCRIPT_PATH}"
else
	echo "Reveal Server not loaded: Cannot find a compatible Reveal app."
fi
