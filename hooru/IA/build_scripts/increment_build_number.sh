# example build script content: "$SRCROOT/hooru/IA/build_scripts/increment_build_number.sh" (with the quotes)

# check "Run script only when installing" to avoid bumping build when not archiving

echo \"Bumping build number...\"

plist=${INFOPLIST_FILE}
buildnum=$(/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" "${plist}")
echo \"Current build number: ${buildnum}\"

if [[ \"${buildnum}\" == \"\" ]]; then
	echo \"No build number in $plist\"
	exit 2
fi
buildnum=$(expr $buildnum + 1)
/usr/libexec/Plistbuddy -c "Set :CFBundleVersion $buildnum" "${plist}"
echo \"Bumped build number to $buildnum\"

