//
//  NSData+.swift
//  InspiringApps
//
//  Created by Brad Weber on 6/17/16.
//  Copyright © 2016 InspiringApps. All rights reserved.
//

import Foundation

extension Data {
	// http://stackoverflow.com/questions/24123518/how-to-use-cc-md5-method-in-swift-language
	// Swift 3: http://stackoverflow.com/questions/39400495/md5-of-data-in-swift-3
	func md5Hash() -> String {
		let length = Int(CC_MD5_DIGEST_LENGTH)
		var digest = [UInt8](repeating: 0, count: length)

		// CC_MD5(bytes, CC_LONG(count), &digest)
		let _ = withUnsafeBytes { (bytes) in
			CC_MD5(bytes, CC_LONG(count), &digest)
		}

		var digestHex = ""
		for i in 0 ..< length {
			digestHex += String(format: "%02x", digest[i])
		}

		return digestHex
	}

}
