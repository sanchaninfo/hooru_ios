//
//  UIColor+.swift
//  InspiringApps
//
//  Created by Brad Weber on 6/28/16.
//  Copyright © 2016 InspiringApps. All rights reserved.
//

import UIKit

extension UIColor {
	convenience init(r: Int, g: Int, b: Int, alpha: CGFloat = 1.0) {
		// Do we need to check for a valid range?
		// let validColorRange = (0 ... 255)

		self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
	}

	convenience init(hex: Int) {
		self.init(r: (hex >> 16) & 0xff, g:(hex >> 8) & 0xff, b: hex & 0xff)
	}

	convenience init(hexString: String) {
		var hexString = hexString
		if hexString.starts(with: "#") {
			let secondIndex = hexString.index(after: hexString.startIndex)
			hexString = String(hexString[secondIndex...])
		}

		guard [3, 6].contains(hexString.count) else {
			print("Unexpected character count in UIColor hexString: \(hexString.count)")
			self.init(hex: 0x000000)
			return
		}

		let charactersPerColor = hexString.count == 3 ? 1 : 2
		var colorComponents = [Int]()

		for i in (0 ..< 3) {
			let startIndex = hexString.index(hexString.startIndex, offsetBy: i * charactersPerColor)
			let endIndex = hexString.index(startIndex, offsetBy: charactersPerColor)
			var colorCharacters = hexString[startIndex..<endIndex]

			if charactersPerColor == 1 {
				// Duplicate it so that F3A becomes FF33AA
				colorCharacters += colorCharacters
			}

			if let colorComponent = Int(colorCharacters, radix: 16) {
				colorComponents.append(colorComponent)
			} else {
				colorComponents.append(0)
			}
		}

		self.init(r: colorComponents[0], g: colorComponents[1], b: colorComponents[2])
	}

	func toHexString() -> String {
		var r: CGFloat = 0
		var g: CGFloat = 0
		var b: CGFloat = 0
		var a: CGFloat = 0 // Ignored

		getRed(&r, green: &g, blue: &b, alpha: &a)

		let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

		return String(format:"#%06x", rgb)
	}
}
