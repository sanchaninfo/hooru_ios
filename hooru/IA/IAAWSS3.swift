//
//  IAAWSS3.swift
//  InspiringApps
//
//  Created by Brad Weber on 12/23/16.
//  Copyright © 2016 InspiringApps. All rights reserved.
//

import Foundation

struct IAAWSS3 {

	static var environmentName: String {
		let keyName = "S3_ENVIRONMENT_NAME"

		if let envName = Bundle.main.infoDictionary![keyName] as? String {
			return envName
		} else {
			IAAWS.printMessage("IAAWSS3: No user-defined build setting for S3_ENVIRONMENT_NAME. Using 'production'.")
		}

		return "production"
	}

	static func urlForFilePath(_ serverFilePath: String, inBucket bucketName: String) -> URL {
		return URL(string: "https://\(bucketName).s3.amazonaws.com/\(serverFilePath)")!
	}

	static func retrieveFileFromBucket(aws: IAAWS, bucketName: String, serverFilePath: String, toLocalURL localUrl: URL, callback: @escaping (_ success: Bool) -> ()) {

		// If there are no keys for the aws object, attempt to get them
		// from the server and update the defaults. The re-initiate
		// this call with a revised aws object.
		guard aws.accessKey != nil && aws.secretKey != nil else {
			callback(false)
			return
		}

		var serverFilePath = serverFilePath
		if serverFilePath.substring(fromIndex: 0, length: 1) == "/" {
			serverFilePath = serverFilePath.substring(fromIndex: 1, toIndex: NSString(string: serverFilePath).length - 1)
		}

		IAAWS.printMessage("Getting S3 object \(bucketName)/\(serverFilePath)")

		let url = urlForFilePath(serverFilePath, inBucket: bucketName)
		let now = Date()
		let payloadHash = "".sha256Hex() ?? ""
		let httpMethod = "GET"

		let headers = aws.signedHeaders(url: url, httpMethod: httpMethod, date: now, payloadHash: payloadHash)

		var request = URLRequest(url: url)
		request.httpMethod = httpMethod

		// Add signed headers
		for header in headers {
			request.addValue(header.value, forHTTPHeaderField: header.key)
		}

		let session = URLSession.shared
		let task = session.downloadTask(with: request) { (tempLocationURL, httpResponse, responseError) in
			var success = false

			if let error = responseError {
				IAAWS.printMessage("IAAWSS3: Error downloading \(bucketName)/\(serverFilePath): \(error.localizedDescription)")
			} else {
				if let response = httpResponse as? HTTPURLResponse {

					if response.statusCode == 404 {
						IAAWS.printMessage("IAAWSS3: Could not find file at \(url)")
					} else {
						if let url = tempLocationURL {
							if FileManager.default.fileExists(atPath: localUrl.path) {
								do {
									try FileManager.default.removeItem(at: localUrl)
								} catch {
									IAAWS.printMessage("IAAWSS3: Could not remove file at \(localUrl.path)")
								}
							}

							do {
								try FileManager.default.moveItem(at: url, to: localUrl)
								success = true
							} catch {
								IAAWS.printMessage("IAAWSS3: Could not move downloaded file to \(localUrl.path)")

								if FileManager.default.fileExists(atPath: localUrl.path) {
									IAAWS.printMessage("    File already exists")
									success = true
								}
							}
						}
					}
				}
			}
			callback(success)
		}

		task.resume()
	}

	static func deliverFileToBucket(aws: IAAWS, bucketName: String, serverFilePath: String, fromLocalURL localUrl: URL, callback: @escaping (_ success: Bool) -> ()) {

		// If there are no keys for the aws object, attempt to get them
		// from the server and update the defaults. The re-initiate
		// this call with a revised aws object.
		guard aws.accessKey != nil && aws.secretKey != nil else {
			callback(false)
			return
		}

		var serverFilePath = serverFilePath
		if serverFilePath.substring(fromIndex: 0, length: 1) == "/" {
			serverFilePath = serverFilePath.substring(fromIndex: 1, toIndex: NSString(string: serverFilePath).length - 1)
		}

		IAAWS.printMessage("Delivering S3 object \(bucketName)/\(serverFilePath)")

		let url = urlForFilePath(serverFilePath, inBucket: bucketName)
		let now = Date()

		var bodyData: Data?
		do {
			try bodyData = Data(contentsOf: localUrl)
		} catch {
			callback(false)
			return
		}

		guard bodyData != nil else {
			callback(false)
			return
		}

		let payloadHash = bodyData!.sha256Hex() ?? ""
		let httpMethod = "PUT"

		let headers = aws.signedHeaders(url: url, httpMethod: httpMethod, date: now, payloadHash: payloadHash)

		var request = URLRequest(url: url)
		request.httpMethod = httpMethod
		request.httpBody = bodyData

		// Add signed headers
		for header in headers {
			request.addValue(header.value, forHTTPHeaderField: header.key)
		}

		let session = URLSession.shared
		let task = session.dataTask(with: request) { (data, response, error) in
			var success = false

			if error != nil {
				IAAWS.printMessage("IAAWSS3: Error delivering \(bucketName)/\(serverFilePath): \(error!.localizedDescription)")
			} else {
				success = true
			}

			callback(success)
		}
		
		task.resume()
	}

}
