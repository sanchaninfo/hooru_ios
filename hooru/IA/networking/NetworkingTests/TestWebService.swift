//
//  TestWebService.swift
//  TestNetworker
//
//  Created by Will Helling on 3/6/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation
import Networking

class TestWebService {
	private static var webService: WebService!
	
	static var service: WebService = {
		webService = WebService(session: GlobalProvider.urlSession)

		webService
			.setServerPath("http://localhost/~will/test/v1/")
			.setReauthenticationHandler(CustomReauthenticationHandler())
		
		return webService
	}()
	
	class CustomReauthenticationHandler: ReauthenticationHandler {
		func reauthenticate() {
			NSLog("Something")
		}
	}
}
