//
//  GlobalProvider.swift
//  Networking
//
//  Created by Will Helling on 3/21/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation
import Networking

class GlobalProvider {
	static let urlSession = FakeURLSession()
}
