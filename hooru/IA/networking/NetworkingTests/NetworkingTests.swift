//
//  TestNetworkerTests.swift
//  TestNetworkerTests
//
//  Created by Will Helling on 3/6/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import XCTest
@testable import Networking

class TestNetworkerTests: XCTestCase {
	
	override func setUp() {
		super.setUp()
		// Put setup code here. This method is called before the invocation of each test method in the class.
	}
	
	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
		super.tearDown()
	}
	
	func testTestWebAPI() {
		GlobalProvider.urlSession.responseHandler = { request -> Data? in
			if request.url!.absoluteString == "http://localhost/~will/test/v1/response.txt" {
				return "{\"string_field\": \"I am a string\"}".data(using: .utf8)
			}
			return nil
		}
		
		let api = TestWebAPI()
		
		let apiExpectation = expectation(description: "hi")
		let response = api.fetchResponse { string, error in
			XCTAssertNotNil(string)
			XCTAssertNil(error)
			if let string = string {
				XCTAssertTrue(string.contains("string"))
			}
			apiExpectation.fulfill()
		}
		
		XCTAssertNotNil(response)
		
		waitForExpectations(timeout: 2, handler: nil)
	}
	
	func testPerformanceExample() {
		// This is an example of a performance test case.
		self.measure {
			// Put the code you want to measure the time of here.
		}
	}
	
}
