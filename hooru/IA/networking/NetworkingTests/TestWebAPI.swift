//
//  TestWebAPI.swift
//  TestNetworker
//
//  Created by Will Helling on 3/6/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation
import Networking

class TestWebAPI {
	
	@discardableResult
	func fetchResponse(completion: @escaping (_ string: String?, _ error: Error?) -> Void) -> Response {
		let webCall = WebCall(service: TestWebService.service)
			.setEndpoint("response.txt")
		
		let responseHandler = JSONResponseHandler(completion: { (jsonObject, error) in
			if error != nil {
				completion(nil, error)
			}
			else if let string = jsonObject?["string_field"] as? String {
				completion(string, nil)
			}
			else {
				completion(nil, nil)
			}
		})
		
		webCall.setResponseHandler(responseHandler)
		let response = webCall.call()
		
		return response
	}
	
}
