//
//  Response.swift
//  Networking
//
//  Created by Will Helling on 3/6/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public enum ResponseError: Error {
	case unauthorized(description: String)
	case restricted(description: String)
	case clientSide(description: String)
	case serverSide(description: String)
	case connectivity(description: String)
	case other(description: String)
}

/// Represents an asynchronous task to a web service. It works essentially the same as a future or promise, in other language lingo.
public final class Response {
	
	/// An enum of possible states a Response object can currently be set to.
	public enum Status {
		/// This Response object is still waiting for the network call to return.
		case InFlight
		/// This Response object has completed.
		case Complete
	}
	
	// MARK: - Variables

	/// Set to true by using IgnoreReauthenticationHandler call flag
	public var ignoreReauthenticationHandler: Bool = false

	/// The raw data returned from the URLRequest
	public private(set) var data: Data?
	
	/// Any associated localURL for this Response (for example, in the case of a .UseDownloadTask call).
	public private(set) var localURL: URL?
	
	/// Any error that might have come from the network handling of this Response.
	public private(set) var error: ResponseError?
	
	/// Either InFlight or Complete.
	public private(set) var status: Status?
	
	// The raw HTTPURLResponse returned from the URLRequest
	public private(set) var httpResponse: HTTPURLResponse?
	
	// The number of bytes currently uploaded to the server.
	public private(set) var bytesUploaded: Double = 0.0
	
	// The total number of bytes the Response is expecting to upload.
	public private(set) var totalBytesToUpload: Double = 0.0
	
	// The number of bytes currently downloaded from the server.
	public private(set) var bytesDownloaded: Double = 0.0
	
	// The total number of bytes the Response is expecting to download.
	public private(set) var totalBytesToDownload: Double = 0.0
	
	private var chainedUpdateHandlers = [UpdateHandler]()
	private var chainedResponseHandlers = [ResponseHandler]()
	private var isKilled = false
	
	
	// MARK: - Public API
	
	/// This variable can be used to pass data down the ResponseHandler chain. It is otherwise not used by Networking framework.
	public var userInfo = [String : Any]()
	
	/// UpdateHandlers provide feedback on both the upload and download status of the network call. If an update handler is added after the Response has completed,
	/// it will never be called.
	///
	/// - Parameter updateHandler: A class adhering to the UpdateHandler protocol that should receive information about the network status of the call.
	public func addUpdateHandler(_ updateHandler: UpdateHandler) {
		guard status == .InFlight else {
			return
		}
		chainedUpdateHandlers.append(updateHandler)
	}
	
	/// ResponseHandlers are called, in order of addition, after the network activity of the web call has completed. If a response handler is added after the Response
	/// has completed, it will be handled immediately.
	///
	/// - Parameter responseHandler: The response handler object that should be notified when the web call completes.
	public func addResponseHandler(_ responseHandler: ResponseHandler) {
		synchronized(self) {
			if status == .Complete {
				responseHandler.handleResponse(response: self)
				return
			}
			
			chainedResponseHandlers.append(responseHandler)
		}
	}
	
	/// Overwrites any error that might currently be held by the response with the one you provide. Useful for ResponseHandlers to provide custom errors that can be
	/// propagated through the ResponseHandler chain.
	///
	/// - Parameter error: The error with which to overwrite the Response's internal error field.
	public func overwriteError(error: ResponseError) {
		self.error = error
	}
	
	/// Immediately kills the response, thereby halting the response chain. Useful for forcefully ending the ResponseHandler chain in the event of an unrecoverable error.
	public func kill() {
		isKilled = true
	}
	
	
	// MARK: - Internal Methods
	
	func appendData(_ data: Data) {
		if self.data == nil {
			self.data = data
		} else {
			self.data?.append(data)
		}
	}
	
	func updateDownload(bytes: Int64, totalBytes: Int64) {
		bytesDownloaded = Double(bytes)
		totalBytesToDownload = Double(totalBytes)
		for chainedUpdateHandler in chainedUpdateHandlers {
			chainedUpdateHandler.handleDownloadUpdate(response: self)
		}
	}
	
	func updateUpload(bytes: Int64, totalBytes: Int64) {
		bytesUploaded = Double(bytes)
		totalBytesToUpload = Double(totalBytes)
		for chainedUpdateHandler in chainedUpdateHandlers {
			chainedUpdateHandler.handleUploadUpdate(response: self)
		}
	}
	
	func distribute(data: Data?, url: URL?, httpResponse: HTTPURLResponse?, error: ResponseError?) {
		synchronized(self) {
			self.data = data
			self.localURL = url
			self.httpResponse = httpResponse
			self.error = error
			self.status = .Complete
			
			for chainedResponseHandler in chainedResponseHandlers {
				if isKilled {
					break
				}
				chainedResponseHandler.handleResponse(response: self)
			}
			
			chainedResponseHandlers.removeAll()
		}
	}
	
	func synchronized(_ lock: AnyObject, _ body: () -> Void) {
		objc_sync_enter(lock)
		defer { objc_sync_exit(lock) }
		return body()
	}
}
