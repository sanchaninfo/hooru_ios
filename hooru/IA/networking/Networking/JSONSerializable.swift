//
//  JSONSerializable.swift
//  Networking
//
//  Created by Will Helling on 3/22/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public protocol JSONSerializable {
	init(json: [String : Any])
	var json: [String : Any] { get }
}
