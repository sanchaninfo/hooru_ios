//
//  FakeURLSession.swift
//  Networking
//
//  Created by Will Helling on 3/6/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public final class FakeURLSession: URLSession {
	
	public typealias FakeResponseHandler = (_ request: URLRequest) -> Data?
	
	public var responseBundle: Bundle?
	public var responseHandler: FakeResponseHandler?
	public var responseMapFile: URL?
	public var responseRootDirectory: URL?
	
	private var storedDelegate: URLSessionDelegate?
	override public var delegate: URLSessionDelegate? {
		set {
			storedDelegate = delegate
		}
		get {
			return storedDelegate
		}
	}
	
	public override init() {
		self.responseBundle = nil
		self.responseHandler = nil
	}
	
	public init(bundleName: String) {
		if let bundleURL = Bundle.main.url(forResource: bundleName, withExtension: "bundle") {
			if let bundle = Bundle(url:bundleURL) {
				self.responseBundle = bundle
				self.responseHandler = nil
				return
			}
		}
		
		fatalError("Bundle does not exist")
	}
	
	public init(fakeDirectory: String, mapFileName: String, delegate: URLSessionDelegate?) {
		self.responseBundle = nil
		self.responseHandler = nil
		
		let cleanMapFileName = mapFileName.replacingOccurrences(of: ".json", with: "")
		self.responseMapFile = Bundle.main.url(forResource: "\(fakeDirectory)/\(cleanMapFileName)", withExtension: "json")
		self.responseRootDirectory = Bundle.main.resourceURL!.appendingPathComponent(fakeDirectory)
		
		self.storedDelegate = delegate
	}
	
	public init(responseHandler: @escaping FakeResponseHandler) {
		self.responseBundle = nil
		self.responseHandler = responseHandler
	}
	
	public override func dataTask(with request: URLRequest) -> URLSessionDataTask {
		let dataTask = FakeURLSessionDataTask(request: request, completion: nil)
		
		dataTask.completion = { data, response, error in
			if let taskDelegate = self.delegate as? URLSessionDataDelegate {
				if error != nil {
					taskDelegate.urlSession?(self, task: dataTask, didCompleteWithError: error)
				} else if data != nil {
					taskDelegate.urlSession?(self, dataTask: dataTask, didReceive: data!)
					taskDelegate.urlSession?(self, task: dataTask, didCompleteWithError: nil)
				}
			}
		}
		
		dataTask.responseBundle = responseBundle
		dataTask.responseHandler = responseHandler
		dataTask.responseMapFile = responseMapFile
		dataTask.responseRootDirectory = responseRootDirectory
		return dataTask
	}
	
	public override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
		let dataTask = FakeURLSessionDataTask(request: request, completion: completionHandler)
		dataTask.responseBundle = responseBundle
		dataTask.responseHandler = responseHandler
		dataTask.responseMapFile = responseMapFile
		dataTask.responseRootDirectory = responseRootDirectory
		return dataTask
	}
	
	class FakeURLSessionDataTask: URLSessionDataTask {
		private static var identifier = 0
		private var request: URLRequest
		private var httpURLResponse: HTTPURLResponse?
		private var currentState: URLSessionTask.State = URLSessionTask.State.suspended
		
		fileprivate var completion: ((Data?, URLResponse?, Error?) -> Void)?
		
		fileprivate var responseBundle: Bundle?
		fileprivate var responseHandler: FakeResponseHandler?
		fileprivate var responseMapFile: URL? {
			didSet {
				map = nil
				guard let responseMapFile = responseMapFile else {
					return
				}
				do {
					let data = try Data(contentsOf: responseMapFile)
					map = try JSONSerialization.jsonObject(with: data, options: []) as? [String : String]
				} catch {
					return
				}
			}
		}
		fileprivate var responseRootDirectory: URL?
		
		var map: [String:String]?
		
		init(request: URLRequest, completion: ((Data?, URLResponse?, Error?) -> Void)?) {
			self.request = request
			self.completion = completion
		}
		
		override var taskIdentifier: Int {
			FakeURLSessionDataTask.identifier += 1
			return FakeURLSessionDataTask.identifier
		}
		
		override var originalRequest: URLRequest? {
			return request
		}
		
		override var currentRequest: URLRequest? {
			return request
		}
		
		private var storedCountOfBytesReceived: Int64 = 0
		override var countOfBytesReceived: Int64 {
			get {
				return storedCountOfBytesReceived
			}
		}
		
		private var storedCountOfBytesExpectedToReceive: Int64 = 0
		override var countOfBytesExpectedToReceive: Int64 {
			get {
				return storedCountOfBytesExpectedToReceive
			}
		}
		
		//@NSCopying
		override var response: URLResponse? {
			return httpURLResponse
		}
		
		override func cancel() {
			currentState = URLSessionTask.State.canceling
		}
		
		override var state: URLSessionTask.State {
			return currentState
		}
		
		override var error: Error? {
			return nil
		}
		
		override func suspend() {
			currentState = URLSessionTask.State.suspended
		}
		
		override func resume() {
			currentState = URLSessionTask.State.running
			
			DispatchQueue.global(qos: .background).async {
				guard let url = self.request.url?.absoluteString, let method = self.request.httpMethod else {
					return
				}
				
				var data: Data?
				var response: HTTPURLResponse?
				var error: Error?
				
				if self.responseHandler != nil {
					(data, response, error) = self.fulfillRequestWithHandler(method: method, url: url)
				}
				
				if self.responseBundle != nil {
					(data, response, error) = self.fulfillRequestWithBundle(method: method, url: url)
				}
				
				if self.responseMapFile != nil && self.responseRootDirectory != nil {
					(data, response, error) = self.fulfillRequestWithMapFile(method: method, url: url)
				}
				
				if response != nil {
					self.storedCountOfBytesReceived = Int64(data?.count ?? 0)
					self.storedCountOfBytesExpectedToReceive = Int64(data?.count ?? 0)
					self.completion?(data, response, error)
				} else {
					self.httpURLResponse = HTTPURLResponse(url: self.request.url!, statusCode: 404, httpVersion: nil, headerFields: nil)
					self.currentState = URLSessionTask.State.completed
					self.completion?(nil, self.httpURLResponse, nil)
				}
			}
		}
		
		private func fulfillRequestWithHandler(method: String, url: String) -> (Data?, HTTPURLResponse?, Error?) {
			guard let responseHandler = responseHandler else {
				return (nil, nil, nil)
			}
			
			if let data = responseHandler(request) {
				httpURLResponse = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)
				currentState = URLSessionTask.State.completed
				return (data, httpURLResponse, nil)
			}
			
			return (nil, nil, nil)
		}
		
		private func fulfillRequestWithBundle(method: String, url: String) -> (Data?, HTTPURLResponse?, Error?) {
			guard let bundle = responseBundle else {
				return (nil, nil, nil)
			}
			
			guard let callMap = bundle.infoDictionary as? [String : String] else {
				return (nil, nil, nil)
			}
			
			var bundleURL: URL?
			if let mappedCall = callMap[url] {
				bundleURL = bundle.url(forResource: mappedCall, withExtension: nil)
			}
			
			//Thread.sleep(forTimeInterval: 1.5)
			
			if bundleURL != nil {
				if let data = try? Data(contentsOf: bundleURL!) {
					// Successful Request
					httpURLResponse = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)
					currentState = URLSessionTask.State.completed
					return (data, httpURLResponse, nil)
				} else {
					// Data loading error
					httpURLResponse = HTTPURLResponse(url: request.url!, statusCode: 500, httpVersion: nil, headerFields: nil)
					currentState = URLSessionTask.State.completed
					return (nil, httpURLResponse, nil)
				}
			}
			
			return (nil, nil, nil)
		}
		
		private func fulfillRequestWithMapFile(method: String, url: String) -> (Data?, HTTPURLResponse?, Error?) {
			guard let map = map, let responseRootDirectory = responseRootDirectory else {
				return (nil, nil, nil)
			}
			
			var key = "\(method)\(url)"
			//key.replacingOccurrences(of: "x/", with: "")
			
			for mapKey in map.keys {
				if key.contains(mapKey) {
					key = mapKey
					break
				}
			}
			
			guard let path = map[key] else {
				return (nil, nil, nil)
			}
			
			let responseURL = responseRootDirectory.appendingPathComponent(path)
			
			if let data = try? Data(contentsOf: responseURL) {
				// Successful Request
				httpURLResponse = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)
				currentState = URLSessionTask.State.completed
				return (data, httpURLResponse, nil)
			} else {
				// Data loading error
				httpURLResponse = HTTPURLResponse(url: request.url!, statusCode: 500, httpVersion: nil, headerFields: nil)
				currentState = URLSessionTask.State.completed
				return (nil, httpURLResponse, nil)
			}
		}
	}
}
