//
//  URLRequest+Serialization.swift
//  Networking
//
//  Created by Will Helling on 3/21/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public extension URLRequest {
	public mutating func applyFormInfo(_ formInfo: [String : String]) {
		let boundary = "Boundary-\(UUID().uuidString)"
		
		self.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
		
		let httpBody = NSMutableString()
		
		for (key, value) in formInfo {
			httpBody.append("--\(boundary)\r\n")
			httpBody.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
			httpBody.append("\(value)\r\n")
		}
		
		self.httpBody = httpBody.data(using: String.Encoding.utf8.rawValue)
	}
	
	public mutating func applyQueryInfo(_ queryInfo: [String: String]) {
		let queryString = NSMutableString(string: "?")
		
		for (key, value) in queryInfo {
			queryString.append("\(key)=\(value)&")
		}
		
		let urlPath = "\(self.url!.absoluteString)\(queryString)"
		self.url = URL(string: urlPath)
	}
	
	public mutating func applyJSONInfo(_ jsonInfo: Any) {
		self.setValue("application/json", forHTTPHeaderField: "Content-Type")
		
		do {
			self.httpBody = try JSONSerialization.data(withJSONObject: jsonInfo, options: [])
		} catch {
			WebCall.showError("Error serializing json info for request \(self)")
		}
	}
	
	public mutating func applyPathParameter(_ path: String) {
		let urlPath = "\(self.url!.absoluteString)\(path)"
		self.url = URL(string: urlPath)
	}
}
