//
//  WebCall
//  Networking
//
//  Created by Will Helling on 3/6/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public final class WebCall {
	
	private let _service: WebService
	private var _url: URL?
	private var _timeout: TimeInterval = 60.0
	private var _priority: Float = URLSessionTask.defaultPriority
	private var _callFlags: [CallFlag] = []
	private var _responseHandlers = [ResponseHandler]()
	private var _requestModifiers = [RequestModifier]()
	private var _updateHandlers = [UpdateHandler]()
	
	// MARK: - Init
	
	/// Required initializer for the WebCall.
	///
	/// - Parameter service: The service that should be used to handle this WebCall.
	public required init(service: WebService) {
		_service = service
	}
	
	
	// MARK: - Setters and Builder Methods
	
	/// The endpoint that should be attached to the service's serverPath field when building the URL for the call.
	///
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func setEndpoint(_ endpoint: String) -> WebCall {
		guard let url = URL(string: _service.serverPath.appending("\(endpoint)")) else {
			WebCall.showError("A valid URL could not be constructed from the webservice path: \"\(_service.serverPath)\" and endpoint: \"\(endpoint)\"")
			return self
		}
		_url = url
		return self
	}
	
	/// If this call hits something that doesn't match the serverPath + endpoint paradigm, you can set the full path manually here.
	///
	/// - Parameter url: The full URL object for the call.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func setCustomURL(_ url: URL) -> WebCall {
		_url = url
		return self
	}
	
	/// If this call should have a priority other than the default defined by the service, you can set it here.
	///
	/// - Parameter priority: A float between 0.0 (low) and 1.0 (high) indicating the relative priority for the call.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func setPriority(_ priority: Float) -> WebCall {
		_priority = priority
		return self
	}
	
	/// Some tasks require specific configuration at generation. CallFlags provide a mechanism to inform the service how to
	/// properly create the task that is used by the networking code.
	///
	/// - Parameter callFlag: A CallFlag object indicating that the WebCall should be generated in a specific way.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func addCallFlag(_ callFlag: CallFlag) -> WebCall {
		_callFlags.append(callFlag)
		return self
	}
	
	/// RequestModifiers are used to alter the URLRequest object prior to being sent.
	/// For example: adding POST data or GET parameters using concrete classes PostRequestModifier or GetRequestModifier, respectively
	///
	/// - Parameter requestModifier: The RequestModifier object that should alter the URLRequest object before it is sent.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func addRequestModifier(_ requestModifier: RequestModifier) -> WebCall {
		_requestModifiers.append(requestModifier)
		return self
	}
	
	/// RequestModifiers are used to alter the URLRequest object prior to being sent.
	/// For example: adding POST data or GET parameters using concrete classes PostRequestModifier or GetRequestModifier, respectively
	///
	/// - Parameter requestModifiers: An array of RequestModifier objects that should alter the URLRequest object befoire it is sent.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func addRequestModifiers(_ requestModifiers: [RequestModifier]) -> WebCall {
		_requestModifiers.append(contentsOf: requestModifiers)
		return self
	}
	
	/// UpdateHandlers are given information about the upload and download state of a network call while it is in flight. This is
	/// a convenience method to pass the handler to the Response object once it has been created (where it is actually handled).
	///
	/// - Parameter updateHandler: The UpdateHandler object that should receive network information about the network call.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func addUpdateHandler(_ updateHandler: UpdateHandler) -> WebCall {
		_updateHandlers.append(updateHandler)
		return self
	}
	
	/// UpdateHandlers are given information about the upload and download state of a network call while it is in flight. This is
	/// a convenience method to pass the handler to the Response object once it has been created (where it is actually handled).
	///
	/// - Parameter updateHandlers: An array of UpdateHandler objects that should receive network information about the network call.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func addUpdateHandlers(_ updateHandlers: [UpdateHandler]) -> WebCall {
		_updateHandlers.append(contentsOf: updateHandlers)
		return self
	}
	
	/// ResponseHandlers are used to interact with the data that is returned from the server. This is a convenience method to pass
	/// the handler to the Response object once it has been created (where it is actually handled).
	///
	/// - Parameter responseHandler: The ResponseHandler object that should be called once the network call has returned.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func addResponseHandler(_ responseHandler: ResponseHandler) -> WebCall {
		_responseHandlers.append(responseHandler)
		return self
	}
	
	/// ResponseHandlers are used to interact with the data that is returned from the server. This is a convenience method to pass
	/// the handler to the Response object once it has been created (where it is actually handled).
	///
	/// - Parameter responseHandlers: An array of ResponseHandler objects that should be called once the network call has returned.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func addResponseHandlers(_ responseHandlers: [ResponseHandler]) -> WebCall {
		_responseHandlers.append(contentsOf: responseHandlers)
		return self
	}
	
	/// If this network call should have a timeout other than 60 seconds, set it here.
	///
	/// - Parameter timeout: The timeout length in seconds.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func setTimeout(_ timeout: TimeInterval) -> WebCall {
		_timeout = timeout
		return self
	}

	static func showError(_ message: String) {
		#if DEBUG
			print("\n *****  IA Networking: \n\n    Error: \(message)\n\n *****")
		#endif
	}

	// MARK: - Call
	
	/// Builds the URLRequest, runs all the RequestModifiers (in order of addition) and sends the call on its way.
	///
	/// - Returns: The Response object that maintains the state information for this network call.
	@discardableResult
	public func call() -> Response {
		guard _url != nil else {
			fatalError("The URL has not been properly built for this web api call")
		}
		
		var request = URLRequest(url: _url!,
		                         cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
		                         timeoutInterval: _timeout)
		
		for requestModifier in _requestModifiers {
			requestModifier.modifyRequest(request: &request)
		}
		
		let response = _service.call(request: &request, priority: _priority, callFlags: _callFlags,
		                             updateHandlers: _updateHandlers,
		                             responseHandlers: _responseHandlers)
		
		return response
	}

}
