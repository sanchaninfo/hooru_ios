//
//  KeychainItem.swift
//  Networking
//
//  Created by Will Helling on 2/9/2016.
//  Copyright © 2016 inspiringapps. All rights reserved.
//

import Foundation

public class KeychainItem {
	var lastResultCode: OSStatus = noErr
	
	var accessGroup: String? // 
	var service: String? // A context bucket for the keychain, for instance, the name of the app
	var account: String? // For instance, the key of the value being saved
	var data: Data?
	
	convenience public init(accessGroup: String? = nil, service: String? = nil, account: String? = nil, data: Data? = nil) {
		self.init(accessGroup: accessGroup, service: service, account: account, value: data)
	}
	
	public init(accessGroup: String? = nil, service: String? = nil, account: String? = nil, value: Any? = nil) {
		self.accessGroup = accessGroup
		self.service = service
		self.account = account
		
		if let data = value as? Data {
			self.data = data
		}
		else if let string = value as? String {
			self.data = string.data(using: .utf16)
		}
		else if var int = value as? Int {
			self.data = Data(buffer: UnsafeBufferPointer(start: &int, count: 1))
		}
		else if var double = value as? Double {
			self.data = Data(buffer: UnsafeBufferPointer(start: &double, count: 1))
		}
	}
	
	fileprivate var deleteQuery: [String: AnyObject] {
		var queryInfo = [String: AnyObject]()
		
		queryInfo[kSecClass as String] = kSecClassGenericPassword
		
		if accessGroup != nil {
			queryInfo[kSecAttrAccessGroup as String] = accessGroup! as AnyObject?
		}
		
		if service != nil {
			queryInfo[kSecAttrService as String] = service! as AnyObject?
		}
		
		if account != nil {
			queryInfo[kSecAttrAccount as String] = account! as AnyObject?
		}
		
		return queryInfo
	}
	
	fileprivate var getQuery: [String: AnyObject] {
		var queryInfo = [String: AnyObject]()
		
		queryInfo[kSecClass as String] = kSecClassGenericPassword
		queryInfo[kSecReturnData as String] = kCFBooleanTrue
		queryInfo[kSecMatchLimit as String] = kSecMatchLimitOne
		
		if accessGroup != nil {
			queryInfo[kSecAttrAccessGroup as String] = accessGroup! as AnyObject?
		}
		
		if service != nil {
			queryInfo[kSecAttrService as String] = service! as AnyObject?
		}
		
		if account != nil {
			queryInfo[kSecAttrAccount as String] = account! as AnyObject?
		}
		
		return queryInfo
	}
	
	fileprivate var setQuery: [String: AnyObject] {
		var queryInfo = [String: AnyObject]()
		
		queryInfo[kSecClass as String] = kSecClassGenericPassword
		
		if accessGroup != nil {
			queryInfo[kSecAttrAccessGroup as String] = accessGroup! as AnyObject?
		}
		
		if service != nil {
			queryInfo[kSecAttrService as String] = service! as AnyObject?
		}
		
		if account != nil {
			queryInfo[kSecAttrAccount as String] = account! as AnyObject?
		}
		
		if data != nil {
			queryInfo[kSecValueData as String] = data! as AnyObject?
		}
		
		return queryInfo
	}
	
	@discardableResult
	public func delete() -> Bool {
		lastResultCode = SecItemDelete(deleteQuery as CFDictionary)
		return lastResultCode == noErr
	}
	
	@discardableResult
	public func save() -> Bool {
		delete()
		lastResultCode = SecItemAdd(setQuery as CFDictionary, nil)
		return lastResultCode == noErr
	}
	
	public func getData() -> Data? {
		var result: AnyObject?
		lastResultCode = withUnsafeMutablePointer(to: &result) { SecItemCopyMatching(getQuery as CFDictionary, UnsafeMutablePointer($0)) }
		
		if lastResultCode == noErr && result != nil {
			return result as? Data
		}
		
		return nil
	}
	
	public func getText() -> String? {
		if let data = getData() {
			if let text = String(data: data, encoding: .utf16) {
				return text
			}
			else if let text = String(data: data, encoding: .utf8) {
				return text
			}
			
			lastResultCode = -67853 // errSecInvalidEncoding
		}
		
		return nil
	}
	
	public func getInt() -> Int? {
		if let data = getData() {
			let int: Int = data.withUnsafeBytes { $0.pointee }
			return int
		}
		return nil
	}
	
	public func getDouble() -> Double? {
		if let data = getData() {
			let double: Double = data.withUnsafeBytes { $0.pointee }
			return double
		}
		return nil
	}
}
