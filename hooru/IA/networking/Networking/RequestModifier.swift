//
//  RequestModifier.swift
//  Networking
//
//  Created by Will Helling on 3/21/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

// Use concrete classes PostRequestModifier or GetRequestModifier for implementations

public protocol RequestModifier {
	func modifyRequest(request: inout URLRequest)
}
