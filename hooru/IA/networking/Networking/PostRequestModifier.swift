//
//  PostRequestModifier.swift
//  Networking
//
//  Created by Will Helling on 3/22/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public class PostRequestModifier: RequestModifier {
	
	private var headers = [String : String]()
	private var body: Data?
	private var useStream: Bool = false
	
	public init() {}
	
	public init(body: Data? = nil, contentType: String? = nil) {
		headers["Content-Type"] = contentType
		self.body = body
	}
	
	public init(json: Any) {
		do {
			body = try JSONSerialization.data(withJSONObject: json, options: [])
			headers["Content-Type"] = "application/json"
		} catch {
			print("Error serializing json for request.")
		}
	}
	
	public init<T: Encodable>(encodableJSON: T) {
		do {
			body = try JSONEncoder().encode(encodableJSON)
			headers["Content-Type"] = "application/json"
		} catch {
			print("Error serializing json for request.")
		}
	}
	
	/// Create a new PostRequestModifier object using multipart/form-data for the body.
	///
	/// - Parameters:
	///   - form: An array of FormElement objects that should be converted into the form data. See the FormElement object for more information.
	///   - useStream: If you need to send objects directly from disk as a part of this form, you must set this to true. Further, you must pass the callFlag
	/// .UseStreamedUploadTask into the WebCall before its call() method
	public init(form: [FormElement], useStream: Bool = false) {
		let boundary = "Boundary-\(UUID().uuidString)"
		
		headers["Content-Type"] = "multipart/form-data; boundary=\(boundary)"
		
		body = Data()
		
		for formElement in form {
			
			// Boundary
			body!.append("--\(boundary)\r\n".data(using: .utf8)!)
			
			// Disposition
			body!.append("Content-Disposition: form-data; name=\"\(formElement.name)\"".data(using: .utf8)!)
			if let filename = formElement.filename {
				body!.append("; filename=\"\(filename)\"".data(using: .utf8)!)
			}
			body!.append("\r\n".data(using: .utf8)!)
				
			// Type
			if let type = formElement.type {
				body!.append("Content-Type: \(type)\r\n\r\n".data(using: .utf8)!)
			} else {
				body!.append("Content-Type: text/plain\r\n\r\n".data(using: .utf8)!)
			}
			
			if let stringValue = formElement.value as? String {
				body!.append("\(stringValue)".data(using: .utf8)!)
			}
			
			else if let dataValue = formElement.value as? Data {
				body!.append(dataValue)
			}
			
			body!.append("\r\n".data(using: .utf8)!)
		}
		
		body!.append("--\(boundary)\r\n".data(using: .utf8)!)
		
		headers["Content-Length"] = "\(body!.count)"
		
		// Debug Check
		if let string = NSString(data: body!, encoding: String.Encoding.utf8.rawValue) {
			print("\(string)")
		}
		
		self.useStream = useStream
	}
	
	@discardableResult
	public func addHeader(key: String, value: String) -> PostRequestModifier {
		headers[key] = value
		return self
	}
	
	public func modifyRequest(request: inout URLRequest) {
		request.httpMethod = "POST"
		
		if let body = body {
			if useStream {
				let linkedFileInputStream = LinkedFileInputStream(data: body)
				request.httpBodyStream = linkedFileInputStream
				headers["Content-Length"] = "\(linkedFileInputStream.totalDataSize)"
			} else {
				request.httpBody = body
			}
		}
		
		for (headerKey, headerValue) in headers {
			request.addValue(headerValue, forHTTPHeaderField: headerKey)
		}
	}
	
	public struct FormElement {
		public var disposition: String = "form-data"
		public var name: String
		public var filename: String?
		public var type: String?
		public var value: Any
		
		public init(name: String, value: Any) {
			self.name = name
			self.value = value
		}
		
		public init(name: String, filename: String, type: String, value: Any) {
			self.name = name
			self.filename = filename
			self.type = type
			self.value = value
		}
	}
}
