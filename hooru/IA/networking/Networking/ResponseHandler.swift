//
//  ResponseHandler.swift
//  Networking
//
//  Created by Will Helling on 3/7/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

/// ResponseHandlers are the driving force behind the Response object.
public protocol ResponseHandler {
	func handleResponse(response: Response)
}
