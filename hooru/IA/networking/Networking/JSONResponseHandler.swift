//
//  JSONResponseHandler.swift
//  Networking
//
//  Created by Will Helling on 3/6/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

/// If you know your data is going to be returned in the form of a JSON array, you may use this response handler to automatically
/// convert it to a JSONArray object ([[String: AnyObject]])
public final class JSONArrayResponseHandler: JSONResponseHandler {
	public init(completion: @escaping JSONArrayCompletionHandler) {
		super.init()
		arrayCompletion = completion
	}
}

/// If you know your data is going to be returned in the form of a JSON object, you may use this response handler to automatically
/// convert it to a JSONObject object ([String : AnyObject]).
public final class JSONObjectResponseHandler: JSONResponseHandler {
	public init(completion: @escaping JSONObjectCompletionHandler) {
		super.init()
		objectCompletion = completion
	}
}

/// Use this for Swift 4 Codable Decoding
/// Returns the raw json data
public final class JSONDataResponseHandler: JSONResponseHandler {
	public init(completion: @escaping JSONDataCompletionHandler) {
		super.init()
		dataCompletion = completion
	}
}

public class JSONResponseHandler: ResponseHandler {
	
	/// List of possible errors coming out of the conversion process from the network data to the specific JSON objects.
	public enum Errors : Error {
		case ReturnedDataWasNotJSON
		case ReturnedDataWasUnparsableJSON
		case ReturnedDataWasObjectNotArray
		case ReturnedDataWasArrayNotObject
	}
	
	public typealias JSONObject = [String : AnyObject]
	public typealias JSONArray = [JSONObject]
	public typealias JSONArrayCompletionHandler = (_ jsonArray: JSONArray, _ error: Error?) -> Void
	public typealias JSONObjectCompletionHandler = (_ jsonObject: JSONObject, _ error: Error?) -> Void
	public typealias JSONDataCompletionHandler = (_ jsonData: Data, _ error: Error?) -> Void

	fileprivate var arrayCompletion: JSONArrayCompletionHandler?
	fileprivate var objectCompletion: JSONObjectCompletionHandler?
	fileprivate var dataCompletion: JSONDataCompletionHandler?

	public func handleResponse(response: Response) {
		guard let data = response.data else {
			arrayCompletion?([], response.error)
			objectCompletion?([:], response.error)
			dataCompletion?(Data(), response.error)
			return
		}

		if let dataHandler = dataCompletion {
			dataHandler(data, response.error)
			return
		}
		
		guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
			arrayCompletion?([], Errors.ReturnedDataWasNotJSON)
			objectCompletion?([:], Errors.ReturnedDataWasNotJSON)
			return
		}
		
		if let jsonObject = json as? JSONResponseHandler.JSONObject {
			arrayCompletion?([], Errors.ReturnedDataWasObjectNotArray)
			objectCompletion?(jsonObject, response.error)
		}
		else if let jsonArray = json as? JSONResponseHandler.JSONArray {
			arrayCompletion?(jsonArray, response.error)
			objectCompletion?([:], Errors.ReturnedDataWasArrayNotObject)
		}
		else {
			arrayCompletion?([], Errors.ReturnedDataWasUnparsableJSON)
			objectCompletion?([:], Errors.ReturnedDataWasUnparsableJSON)
		}
	}
}
