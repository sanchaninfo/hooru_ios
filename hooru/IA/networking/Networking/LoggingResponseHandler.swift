//
//  LoggingResponseHandler.swift
//  Networking
//
//  Created by Will Helling on 3/21/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public final class LoggingResponseHandler: ResponseHandler {
	
	private let tag: String?
	private let message: String?
	
	public init(tag: String? = nil, message: String? = nil) {
		self.tag = tag
		self.message = message
	}
	
	public func handleResponse(response: Response) {
		if let data = response.data {
			if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
				print("[\(tag ?? "")][\(message ?? "")] Response Data: \(string)")
			} else {
				print("[\(tag ?? "")][\(message ?? "")] Response Data not a string.")
			}
		}
		else {
			print("[\(tag ?? "")][\(message ?? "")] Response Data: nil")
		}
	}
}
