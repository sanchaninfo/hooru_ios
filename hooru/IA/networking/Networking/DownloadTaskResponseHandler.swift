//
//  DownloadTaskResponseHandler.swift
//  Networking
//
//  Created by Will Helling on 3/27/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

/// When using the .UseDownloadTask CallFlag, you may use this file in conjunction in order to automatically move the file from
/// its temporary local location to the location you designate.
public final class DownloadTaskResponseHandler: ResponseHandler {
	
	public typealias DownloadClosure = (_ response: Response) -> Void
	
	let localURL: URL
	let closure: DownloadClosure
	
	public init(localURL: URL, closure: @escaping DownloadClosure) {
		self.localURL = localURL
		self.closure = closure
	}
	
	public func handleResponse(response: Response) {
		if let url = response.localURL {
			if FileManager.default.fileExists(atPath: localURL.path) {
				do {
					try FileManager.default.removeItem(at: localURL)
				} catch {
					WebCall.showError("Could not remove file at \(localURL.path)")
				}
			}
			
			do {
				try FileManager.default.createDirectory(at: url.deletingLastPathComponent(), withIntermediateDirectories: true, attributes: nil)
				try FileManager.default.moveItem(at: url, to: localURL)
			} catch {
				WebCall.showError("Could not move downloaded file to \(localURL.path)")
				
				if FileManager.default.fileExists(atPath: localURL.path) {
					print("    File already exists")
				}
			}
			
			closure(response)
		}
	}
}
