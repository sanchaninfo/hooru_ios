//
//  ClosureUpdateHandler.swift
//  Networking
//
//  Created by Will Helling on 5/1/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public final class ClosureUpdateHandler: UpdateHandler {
	
	public typealias UpdateHandlerClosure = (_ response: Response) -> Void
	
	let uploadClosure: UpdateHandlerClosure?
	let downloadClosure: UpdateHandlerClosure?
	
	public init(uploadClosure: UpdateHandlerClosure? = nil, downloadClosure: UpdateHandlerClosure? = nil) {
		self.uploadClosure = uploadClosure
		self.downloadClosure = downloadClosure
	}
	
	public func handleUploadUpdate(response: Response) {
		uploadClosure?(response)
	}
	
	public func handleDownloadUpdate(response: Response) {
		downloadClosure?(response)
	}
}
