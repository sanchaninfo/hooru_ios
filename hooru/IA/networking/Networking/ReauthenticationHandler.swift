//
//  ReauthenticationHandler.swift
//  Networking
//
//  Created by Will Helling on 3/6/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public protocol ReauthenticationHandler {
	func reauthenticate()
}
