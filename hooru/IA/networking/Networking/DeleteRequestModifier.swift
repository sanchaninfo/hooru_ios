//
//  DeleteRequestModifier.swift
//  Networking
//
//  Created by Will Helling on 4/12/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public class DeleteRequestModifier: RequestModifier {
	private var headers = [String : String]()
	
	public init() {}
	
	@discardableResult
	public func addHeader(key: String, value: String) -> DeleteRequestModifier {
		headers[key] = value
		return self
	}
	
	public func modifyRequest(request: inout URLRequest) {
		request.httpMethod = "DELETE"
		
		for (headerKey, headerValue) in headers {
			request.addValue(headerValue, forHTTPHeaderField: headerKey)
		}
	}
}

