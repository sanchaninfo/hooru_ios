//
//  AmazonDownloadRequestModifier.swift
//  Networking
//
//  Created by Will Helling on 3/27/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public final class AmazonDownloadRequestModifier: RequestModifier {
	
	let serverFilePath: String
	let bucketName: String
	let awsSigner: AWSSigner
	
	public init(serverFilePath: String, bucketName: String, accessKey: String, secretKey: String, regionName: String, serviceName: String) {
		self.serverFilePath = serverFilePath
		self.bucketName = bucketName
		awsSigner = AWSSigner(accessKey: accessKey, secretKey: secretKey, regionName: regionName, serviceName: serviceName)
	}
	
	public func modifyRequest(request: inout URLRequest) {
		let cleanServerFilePath = serverFilePath
		
		//		if cleanServerFilePath.substring(fromIndex: 0, length: 1) == "/" {
		//			cleanServerFilePath = cleanServerFilePath.substring(fromIndex: 1, length: cleanServerFilePath.characters.count - 1)
		//		}
		
		let url = URL(string: "https://\(bucketName).s3.amazonaws.com/\(cleanServerFilePath)")!
		let now = Date()
		let payloadHash = "".sha256Hex() ?? ""
		let httpMethod = "GET"
		
		let headers = awsSigner.signedHeaders(url: url, httpMethod: httpMethod, date: now, payloadHash: payloadHash)
		
		request.url = url
		request.httpMethod = httpMethod
		
		// Add signed headers
		for header in headers {
			request.addValue(header.value, forHTTPHeaderField: header.key)
		}
	}
}
