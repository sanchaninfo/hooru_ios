//
//  LoggingRequestModifier.swift
//  Networking
//
//  Created by Will Helling on 3/22/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public final class LoggingRequestModifier: RequestModifier {
	private let tag: String?
	private let message: String?
	
	public init(tag: String? = nil, message: String? = nil) {
		self.tag = tag
		self.message = message
	}
	
	public func modifyRequest(request: inout URLRequest) {
		print("[\(tag ?? "")][\(message ?? "")] Request URL: \(request.url?.absoluteString ?? "")")
		print("[\(tag ?? "")][\(message ?? "")] Request Method: \(request.httpMethod ?? "")")
		print("[\(tag ?? "")][\(message ?? "")] Request Headers: \(request.allHTTPHeaderFields ?? [:])")
		
		if let data = request.httpBody {
			if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
				print("[\(tag ?? "")][\(message ?? "")] Request Body: \(string)")
			}
		}
		else {
			print("[\(tag ?? "")][\(message ?? "")] Request Body: nil")
		}
	}
}
