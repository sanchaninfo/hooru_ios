//
//  UpdateHandler.swift
//  Networking
//
//  Created by Will Helling on 4/28/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

/// UpdateHandler allows for you to monitor the response object as it uploads or downloads data.
public protocol UpdateHandler {
	func handleUploadUpdate(response: Response)
	func handleDownloadUpdate(response: Response)
}
