//
//  PatchRequestModifier.swift
//  Networking
//
//  Created by Will Helling on 3/15/18.
//  Copyright © 2018 inspiringapps. All rights reserved.
//

import Foundation

public class PatchRequestModifier: PostRequestModifier {
	override public func modifyRequest(request: inout URLRequest) {
		super.modifyRequest(request: &request)
		request.httpMethod = "PATCH"
	}
}
