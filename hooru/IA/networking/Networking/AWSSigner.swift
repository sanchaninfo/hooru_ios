//
//  AWSSigner.swift
//  Networking
//
//  Created by Will Helling on 3/27/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

final class AWSSigner {
	private var accessKey: String
	private var secretKey: String
	private var regionName: String
	private var serviceName: String
	
	init(accessKey: String, secretKey: String, regionName: String, serviceName: String) {
		self.accessKey = accessKey
		self.secretKey = secretKey
		self.regionName = regionName
		self.serviceName = serviceName
	}
	
	func signedHeaders(url: URL, httpMethod: String, date: Date, payloadHash: String) -> [String: String] {
		var headers = [
			"x-amz-content-sha256": payloadHash,
			"x-amz-date": date.databaseDateTimeISOString(),
			// "x-amz-acl" : "public-read",
			"Host": url.host!,
			]
		headers["Authorization"] = authorizationHeader(url: url, headers: headers, date: date, httpMethod: httpMethod, payloadHash: payloadHash)
		return headers
	}

	// MARK: - Private
	private func pathForURL(_ url: URL) -> String {
		let path = url.path.isEmpty ? "/" : url.path
		return path
	}
	
	private func scope(date: Date) -> String {
		let scope = [date.databaseDateCompactString(), regionName, serviceName, "aws4_request"].joined(separator: "/")
		return scope
	}
	
	private func signedHeaders(_ headers: [String:String]) -> String {
		var signed = headers.keys.map { (key) in
			key.lowercased()
			}.sorted()
		
		if let index = signed.index(of: "authorization") {
			signed.remove(at: index)
		}
		
		let s = signed.joined(separator: ";")
		return s
	}
	
	private func cononicalHeaders(_ headers: [String: String]) -> String {
		var cononical = [String]()
		let keys = headers.keys.sorted() { (key1, key2) -> Bool in
			return key1.localizedCompare(key2) == ComparisonResult.orderedAscending
		}
		
		for key in keys {
			if key.caseInsensitiveCompare("authorization") != ComparisonResult.orderedSame {
				// Note: This does not strip whitespace, but the spec says it should
				cononical.append("\(key.lowercased()):\(headers[key]!)")
			}
		}
		
		let c = cononical.joined(separator: "\n")
		return c
	}
	
	private func cononicalRequest(httpMethod: String, url: URL, queryString: String, headers: [String: String], hashedPayload: String) -> String {
		let request = [httpMethod,
		               pathForURL(url),
		               queryString,
		               "\(cononicalHeaders(headers))\n",
			signedHeaders(headers),
			hashedPayload].joined(separator: "\n")
		return request
	}
	
	
	private func signatureKey(date: Date) -> Data {
		// http://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html#signature-v4-examples-jscript
		
		guard let dateKey = date.databaseDateCompactString().hmac256(key: ("AWS4" + secretKey).data(using: String.Encoding.utf8)!) else {
			return Data()
		}
		
		guard let dateRegionKey = regionName.hmac256(key: dateKey) else {
			return Data()
		}
		
		guard let dateRegionServiceKey = serviceName.hmac256(key: dateRegionKey) else {
			return Data()
		}
		
		guard let signingKey = "aws4_request".hmac256(key: dateRegionServiceKey) else {
			return Data()
		}
		
		let s = signingKey
		return s
	}
	
	private func stringToSign(date: Date, cononicalRequest: String) -> String {
		let signedRequest = cononicalRequest.sha256Hex() ?? ""
		
		let string = ["AWS4-HMAC-SHA256", // algorithm
			date.databaseDateTimeISOString(),
			scope(date: date),
			signedRequest].joined(separator: "\n")

		return string
	}
	
	
	private func authorizationHeader(url: URL, headers: [String: String], date: Date, httpMethod: String, payloadHash: String) -> String {
		let credentials: String = [accessKey, scope(date: date)].joined(separator: "/")
		
		let cononicalRequest = self.cononicalRequest(httpMethod: httpMethod, url: url, queryString: "", headers: headers, hashedPayload: payloadHash)
		
		let stringToSign = self.stringToSign(date: date, cononicalRequest: cononicalRequest)
		let signatureKey = self.signatureKey(date: date)
		
		let signature = stringToSign.hmac256(key: signatureKey)?.toHexString() ?? ""
		
		let h = [
			"AWS4-HMAC-SHA256 Credential=\(credentials)",
			"SignedHeaders=\(signedHeaders(headers))",
			"Signature=\(signature)",
			].joined(separator: ", ")
		
		return h
	}
}

extension Date {
	private static let sharedDBDateTimeFormatter = DateFormatter()
	
	func databaseDateCompactString() -> String {
		Date.sharedDBDateTimeFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		Date.sharedDBDateTimeFormatter.dateFormat = "yyyyMMdd"
		
		return Date.sharedDBDateTimeFormatter.string(from: self)
	}
	
	func databaseDateTimeISOString() -> String {
		Date.sharedDBDateTimeFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		Date.sharedDBDateTimeFormatter.locale = Locale(identifier: "en_US_POSIX")
		Date.sharedDBDateTimeFormatter.dateFormat = "yyyyMMdd'T'HHmmss'Z'"
		
		return Date.sharedDBDateTimeFormatter.string(from: self)
	}
}

extension Data {
	func sha256Hex() -> String? {
		var hash = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
		self.withUnsafeBytes {
			_ = CC_SHA256($0, CC_LONG(self.count), &hash)
		}
		
		return Data(bytes: hash).toHexString()
	}
	
	func toHexString() -> String {
		return map { String(format: "%02x", $0) }.joined()
	}
}

extension String {
	func hmac256(key: Data) -> Data? {
		// Adapted from...
		// http://www.codeography.com/2016/03/18/calling-cchmac-from-swift.html
		var hmacData: Data?
		
		key.withUnsafeBytes { (keyPointer: UnsafePointer<UInt8>) in
			let data = self.cString(using: String.Encoding.utf8)
			let dataLen = Int(self.lengthOfBytes(using: String.Encoding.utf8))
			let digestLen = Int(CC_SHA256_DIGEST_LENGTH)
			let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
			
			CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), keyPointer, key.count, data, dataLen, result)
			
			hmacData = Data(bytes: result, count: digestLen)
		}
		
		return hmacData
	}
	
	
	func sha256Hex() -> String? {
		if let data = self.data(using: String.Encoding.utf8) {
			var hash = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
			var result = Data()
			
			data.withUnsafeBytes() { (dataPointer: UnsafePointer<UInt8>) in
				CC_SHA256(dataPointer, CC_LONG(data.count), &hash)
				result = Data(bytes: hash, count: Int(CC_SHA256_DIGEST_LENGTH))
			}
			
			return result.toHexString()
		}
		
		return nil
	}
}
