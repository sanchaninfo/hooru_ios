//
//  CallFlag.swift
//  Networking
//
//  Created by Will Helling on 3/27/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public enum CallFlag: Int {
	
	/// ??
	case Authenticated
	
	/// Tells the web service to call this task with URLSessionDownloadTask. The local URL the file is downloaded to is passed
	/// into the Response's localURL field.
	case UseDownloadTask
	
	/// ??
	case UseUploadTask
	
	/// Tells the web service to use the URLSessionStream
	case UseStreamedUploadTask

	/// Useful when calling API to log in the user. In case of failure, we shouldn't instantiate a new log in controller
	case IgnoreReauthenticationHandler

}
