//
//  LinkedFileInputStream.swift
//  Networking
//
//  Created by Will Helling on 5/3/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

/// Streams data as normal, but treats anything between <> tags as a local URL and streams that file from disk directly when referenced.
public class LinkedFileInputStream: InputStream, StreamDelegate {
	
	private static let LinkRegexPattern = "(<.*?>)"
	
	private var postDataBuffer: UnsafeMutableBufferPointer<UInt8>
	private var postDataSize = 0
	private(set) var totalDataSize: Int64 = 0
	private var postIndex = 0
	private var totalReadBytes: Int64 = 0
	
	private var parentStream: InputStream
	private var fileStream: InputStream?
	
	var filePaths = [Int : String]()
	
	private var _streamStatus: Stream.Status = .notOpen
	override public var streamStatus: Stream.Status { return _streamStatus }
	
	private var _streamError: Error? = nil
	override public var streamError: Error? { return _streamError }
	
	private var _delegate: StreamDelegate?
	override public var delegate: StreamDelegate? {
		set {
			if newValue == nil {
				_delegate = self
			} else {
				self._delegate = newValue
			}
		}
		get {
			return _delegate
		}
	}
	
	override public init(data: Data) {
		let unsafeMutablePointer = UnsafeMutablePointer<UInt8>.allocate(capacity: data.count)
		postDataBuffer = UnsafeMutableBufferPointer(start: unsafeMutablePointer, count: data.count)
		postDataSize = data.copyBytes(to: postDataBuffer)
		totalDataSize = Int64(postDataSize)
		
		parentStream = InputStream(data: data)
		
		super.init(data: data)
		
		let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) ?? ""
		
		let regex = try? NSRegularExpression(pattern: LinkedFileInputStream.LinkRegexPattern, options: [])
		
		regex?.enumerateMatches(in: string as String, options: [], range: NSMakeRange(0, string.length))
		{ (textCheckingResult, flags, finished) in
			guard let result = textCheckingResult else {
				return
			}
			
			let range = NSMakeRange(result.range.location + 1, result.range.length - 2)
			let path = string.substring(with: range)
			filePaths[result.range.location] = path
			
			do {
				let url = URL(string: path)!
				let attributes = try FileManager.default.attributesOfItem(atPath: url.relativePath)
				if let fileSize = attributes[FileAttributeKey.size] as? NSNumber {
					totalDataSize += fileSize.int64Value
					totalDataSize -= Int64(result.range.length)
				}
			} catch {
				WebCall.showError("\(error)")
			}
			
		}
	}
	
	override public func open() {
		_streamStatus = .open
	}
	
	override public func close() {
		_streamStatus = .closed
	}
	
	override public func schedule(in aRunLoop: RunLoop, forMode mode: RunLoopMode) {
		parentStream.schedule(in: aRunLoop, forMode: mode)
	}
	
	override public func remove(from aRunLoop: RunLoop, forMode mode: RunLoopMode) {
		parentStream.remove(from: aRunLoop, forMode: mode)
	}
	
	override public func setProperty(_ property: Any?, forKey key: Stream.PropertyKey) -> Bool {
		return parentStream.setProperty(property, forKey: key)
	}
	
	override public func property(forKey key: Stream.PropertyKey) -> Any? {
		return parentStream.property(forKey: key)
	}
	
	override public func read(_ buffer: UnsafeMutablePointer<UInt8>, maxLength len: Int) -> Int {
		_streamStatus = .reading
		
		var readBytes = 0
		
		if let fileStream = fileStream {
			readBytes = fileStream.read(buffer, maxLength: len)
			
			if readBytes > 0 {
				totalReadBytes += Int64(readBytes)
			}
			
			if !fileStream.hasBytesAvailable {
				fileStream.close()
				self.fileStream = nil
			}
		}
		
		while (readBytes < len && postIndex < postDataSize) {
			if let filePath = filePaths[postIndex] {
				postIndex += filePath.count + 2
				
				if let url = URL(string: filePath) {
					if let fileStream = InputStream(url: url) {
						self.fileStream = fileStream
						
						fileStream.open()
						assert(fileStream.streamError == nil, "Stream Error: \(fileStream.streamError!)")
						readBytes += read(buffer.advanced(by: readBytes), maxLength: len - readBytes)
					} else {
						WebCall.showError("LinkedFileInputStream Warning: URL \"\(filePath)\" is unsupported")
					}
				} else {
					WebCall.showError("LinkedFileInputStream Warning: Path to file \"\(filePath)\" is an invalid url")
				}
			} else {
				buffer[readBytes] = postDataBuffer[postIndex]
				readBytes += 1
				postIndex += 1
			}
		}
		
		if postIndex == postDataSize {
			_streamStatus = .atEnd
		} else {
			_streamStatus = .open
		}
		
		return readBytes
	}
	
	override public func getBuffer(_ buffer: UnsafeMutablePointer<UnsafeMutablePointer<UInt8>?>, length len: UnsafeMutablePointer<Int>) -> Bool {
		return false
	}
	
	override public var hasBytesAvailable: Bool {
		if let fileStream = fileStream {
			return fileStream.hasBytesAvailable
		}
		
		return postIndex < postDataSize
	}
	
	// MARK: - StreamDelegate
	public func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
		// not necessarily an error, just using the conditional print facility
		WebCall.showError( "eventcode: \(eventCode)")
	}
}
