//
//  GetRequestModifier.swift
//  Networking
//
//  Created by Will Helling on 3/22/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public class GetRequestModifier: RequestModifier {
	private var headers = [String : String]()
	
	public init() {}
	
	@discardableResult
	public func addHeader(key: String, value: String) -> GetRequestModifier {
		headers[key] = value
		return self
	}
	
	public func modifyRequest(request: inout URLRequest) {
		request.httpMethod = "GET"
		
		for (headerKey, headerValue) in headers {
			request.addValue(headerValue, forHTTPHeaderField: headerKey)
		}
	}
}
