//
//  WebService.swift
//  Networking
//
//  Created by Will Helling on 3/6/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

/// The WebService object handles the state of the overall network stack. It is responsible for building, sending and tracking network calls.
public final class WebService: NSObject, URLSessionDataDelegate, URLSessionDownloadDelegate {
	
	public static var DelegateQueueName = "WebService Delegate Queue"

	private var _session: URLSession! = nil
	private var _delegateQueue: OperationQueue!
	private var _serverPath: String?
	private var _reauthenticationHandler: ReauthenticationHandler?
	private var _requestModifiers = [RequestModifier]()
	private var _responseHandlers = [ResponseHandler]()
	
	private var _requestToTaskMap = [URLRequest : URLSessionTask]()
	private var _taskToRequestMap = [URLSessionTask : URLRequest]()
	private var _taskToResponsesMap = [URLSessionTask : [Response]]()
	private var _taskToFileMap = [URLSessionTask : URL]()
	
	private let _lockQueue = DispatchQueue(label: "LockQueue")
	
	// MARK: - Initializers
	
	/// The default initializer for the WebService object. Automatically generates everything it needs to operate correctly.
	///
	/// - Parameter queueName: A specific OperationQueue name that can be used to track network callbacks. Defaults to "WebService Delegate Queue"
	/// - Parameter sessionConfig: A specific URLSessionConfiguration object with which to initialize the URLSession object. Defaults to .default
	public init(
		queueName: String = WebService.DelegateQueueName,
		sessionConfig: URLSessionConfiguration = .default) {
		
		_delegateQueue = OperationQueue()
		_delegateQueue.name = queueName
		
		super.init()
		
		_session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue:_delegateQueue)
	}
	
	/// If you would like to provide your own URLSession for the service to use,
	/// you may use this initializer and take responsibility for implemting delegate methods.
	///
	/// - Parameter session: The URLSession object that should handle the network calls. Ensure the session has your delegate set
	public init(session: URLSession) {
		_delegateQueue = OperationQueue()
		_delegateQueue.name = WebService.DelegateQueueName
		_session = session

		super.init()
	}

	// MARK: - Setters and Builder Methods
	
	public func setSession(_ session: URLSession) {
		_session = session
	}
	
	/// Provides a ReauthenticationHandler object that will be called if the service is refused connectivity based on an authentication restriction, specifically a 401 error.
	///
	/// - Parameter handler: The ReauthenticationHandler object to receive the reauthentication request.
	/// - Returns: The WebService object for passthrough.
	@discardableResult
	public func setReauthenticationHandler(_ handler: ReauthenticationHandler?) -> WebService {
		_reauthenticationHandler = handler
		return self
	}
	
	/// Sets the root server path for the service.
	///
	/// - Parameter path: The full path for the root server url.
	/// - Returns: The WebService object for passthrough.
	@discardableResult
	public func setServerPath(_ path: String) -> WebService {
		_serverPath = path
		return self
	}
	
	/// RequestModifiers are used to alter the URLRequest object prior to being sent. When added to the WebService object they are called before every WebCall that goes through the service.
	/// WebService level RequestModifiers are run AFTER WebCall level RequestModifiers.
	///
	/// - Parameter requestModifier: The RequestModifier object that should alter the URLRequest object before it is sent.
	/// - Returns: The WebService object for passthrough.
	@discardableResult
	public func addRequestModifier(_ requestModifier: RequestModifier) -> WebService {
		_requestModifiers.append(requestModifier)
		return self
	}
	
	/// ResponseHandlers are used to interact with the data that is returned from the server. When added to the WebService object they are called once the network call completes.
	/// WebService level ResponseHandlers are run AFTER WebCall level ResponseHandlers.
	///
	/// - Parameter responseHandler: The ResponseHandler object that should be called once the network call has returned.
	/// - Returns: The WebCall object for passthrough.
	@discardableResult
	public func addResponseHandler(_ responseHandler: ResponseHandler) -> WebService {
		_responseHandlers.append(responseHandler)
		return self
	}
	

	// MARK: - Internal API
	
	var serverPath: String {
		guard _serverPath != nil else {
			fatalError("Server Path has not been set for the web service.")
		}
		return _serverPath!
	}
	
	private func findExistingTask(_ newRequest: URLRequest) -> URLSessionTask? {
		guard newRequest.httpMethod == "GET" else {
			return nil
		}
		
		guard newRequest.httpBodyStream == nil else {
			return nil
		}
		
		for (request, task) in _requestToTaskMap {
			if request == newRequest, request.httpBody == request.httpBody {
				return task
			}
		}
		
		return nil
	}
	
	@discardableResult
	func call(request: inout URLRequest, priority: Float, callFlags: [CallFlag],
	          updateHandlers: [UpdateHandler] = [UpdateHandler](),
	          responseHandlers: [ResponseHandler] = [ResponseHandler]()) -> Response {
		
		// Iterate through the service level request modifiers
		for requestModifier in _requestModifiers {
			requestModifier.modifyRequest(request: &request)
		}
		
		// Automatically add any update and response handlers passed into this method
		let response = Response()
		
		for updateHandler in updateHandlers {
			response.addUpdateHandler(updateHandler)
		}
		
		for responseHandler in responseHandlers {
			response.addResponseHandler(responseHandler)
		}

		if callFlags.contains(.IgnoreReauthenticationHandler) {
			response.ignoreReauthenticationHandler = true
		}
		
		// If a task for this request already exists and is in flight, just attach our response to the response map.
		if let existingTask = findExistingTask(request) {
			var responses = _taskToResponsesMap[existingTask] ?? []
			responses.append(response)
			_lockQueue.sync {
				_taskToResponsesMap[existingTask] = responses
			}
			return response
		}
		
		// Create the session task and send it off for handling
		var task: URLSessionTask
		
		if callFlags.contains(.UseDownloadTask) {
			task = _session.downloadTask(with: request)
		}
		else if callFlags.contains(.UseUploadTask) {
			task = _session.uploadTask(with: request, from: request.httpBody!)
		}
		else if callFlags.contains(.UseStreamedUploadTask) {
			task = _session.uploadTask(withStreamedRequest: request)
		}
		else {
			task = _session.dataTask(with: request)
		}
		
		_lockQueue.sync {
			_requestToTaskMap[request] = task
			_taskToRequestMap[task] = request
			_taskToResponsesMap[task] = [response]
		}
		
		task.priority = priority
		task.resume()
		
		return response
	}
	
	
	// MARK: - URLSessionTaskDelegate, URLSessionDataDelegate
	public func urlSession(_ session: URLSession, task: URLSessionTask,
	                       didSendBodyData bytesSent: Int64,
	                       totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
		guard let responses = _taskToResponsesMap[task] else {
			return
		}
		
		for response in responses {
			response.updateUpload(bytes: totalBytesSent, totalBytes: totalBytesExpectedToSend)
		}
	}
	
	public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
		guard let responses = _taskToResponsesMap[dataTask] else {
			return
		}
		
		for response in responses {
			response.appendData(data)
			response.updateDownload(bytes: dataTask.countOfBytesReceived, totalBytes: dataTask.countOfBytesExpectedToReceive)
		}
	}
	
	public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
		do {
			let cachesDirectory = try FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
			let cacheFile = cachesDirectory.appendingPathComponent(UUID().uuidString)
			try FileManager.default.moveItem(at: location, to: cacheFile)
			_lockQueue.sync {
				_taskToFileMap[downloadTask] = cacheFile
			}
		} catch {
			WebCall.showError("\(error)")
		}
	}
	
	public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
		guard let responses = _taskToResponsesMap[task], let firstResponse = responses.first else {
			return
		}
		
		// If a single response has requested we not ignore the reauthentication handler, we will not ignore the reauthentication handler.
		var ignoreReauthenticationHandler = true
		for response in responses {
			if response.ignoreReauthenticationHandler == false {
				ignoreReauthenticationHandler = false
			}
		}

		var responseError: ResponseError?

		if let httpResponse = task.response as? HTTPURLResponse {

			switch httpResponse.statusCode {
			case 401:
				if let reauthHandler = _reauthenticationHandler, !ignoreReauthenticationHandler {
					reauthHandler.reauthenticate()
				} else {
					responseError = ResponseError.unauthorized(description: "Received a 401 (Unauthorized) status and no reauthenticationHandler is set")
				}
			case 403:
				responseError = ResponseError.clientSide(description: "Received a 403 (FORBIDDEN) status. Check auth tokens in http header fields")
			case 404:
				responseError = ResponseError.clientSide(description: "Received a 404 (Not Found) status. Check URL + endpoint")
			case 418:
				responseError = ResponseError.clientSide(description: "Congratulations! You have reached a teapot. If you wanted not-tea, please look elsewhere.")
			case 0..<200:
				responseError = ResponseError.other(description: "Received status code \(httpResponse.statusCode) (Non-OK informational something)")
			case 300..<400:
				responseError = ResponseError.other(description: "Received status code \(httpResponse.statusCode) (Redirection)")
			case 400..<500:
				if let data = firstResponse.data,
					let errorObject = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? Dictionary<String, String>,
					let errorMessage = errorObject["error"] {
					responseError = ResponseError.restricted(description: "Received status code \(httpResponse.statusCode): \(errorMessage)")
				} else {
					responseError = ResponseError.restricted(description: "Received status code \(httpResponse.statusCode) (Other client-side error)")
				}
			case 500..<600:
				responseError = ResponseError.serverSide(description: "Received status code \(httpResponse.statusCode) (Server error)")
			default:
				break
			}
		} else {
			if let error = error {
				responseError = ResponseError.connectivity(description: error.localizedDescription)
			}
		}

		if let error = responseError {
			if let request = task.currentRequest {
				WebCall.showError("\(error) \n     for request\n      \(request)")
			} else {
				WebCall.showError("\(error)")
			}
		}
		
		for response in responses {
			// Pass the call information into the WebCall's Response object
			response.distribute(data: response.data, url: _taskToFileMap[task], httpResponse: task.response as? HTTPURLResponse, error: responseError)
			
			// Iterate through the service level response handlers
			for responseHandler in _responseHandlers {
				responseHandler.handleResponse(response: response)
			}
		}
		
		_lockQueue.sync {
			_taskToResponsesMap[task] = nil
			_taskToFileMap[task] = nil
			if let request = _taskToRequestMap[task] {
				_requestToTaskMap[request] = nil
			}
			_taskToRequestMap[task] = nil
		}
	}
}
