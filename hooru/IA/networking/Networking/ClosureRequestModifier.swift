//
//  ClosureRequestModifier.swift
//  Networking
//
//  Created by Will Helling on 3/21/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

/// Simple RequestModifier that just passes the request through to the designated closure.
public class ClosureRequestModifier: RequestModifier {
	
	public typealias RequestModifierClosure = (_ request: inout URLRequest) -> Void
	
	let closure: RequestModifierClosure
	
	public init(closure: @escaping RequestModifierClosure) {
		self.closure = closure
	}
	
	public func modifyRequest(request: inout URLRequest) {
		closure(&request)
	}
}
