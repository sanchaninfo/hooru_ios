//
//  ClosureResponseHandler.swift
//  Networking
//
//  Created by Will Helling on 3/21/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import Foundation

public final class ClosureResponseHandler: ResponseHandler {
	
	public typealias ResponseHandlerClosure = (_ response: Response) -> Void
	
	let closure: ResponseHandlerClosure
	
	public init(closure: @escaping ResponseHandlerClosure) {
		self.closure = closure
	}
	
	public func handleResponse(response: Response) {
		closure(response)
	}
}
