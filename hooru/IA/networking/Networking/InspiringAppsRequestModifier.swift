//
//  InspiringAppsRequestModifier.swift
//  Networking
//
//  Created by Will Helling on 3/21/17.
//  Copyright © 2017 inspiringapps. All rights reserved.
//

import UIKit

/// RequestModifier that installs the correct IA headers to interface with our web API
public class InspiringAppsRequestModifier: RequestModifier {
	class Headers {
		static let IA_AUTH_USER = "ia-auth-user"
		static let IA_AUTH_USER_NAME = "ia-auth-user-name"
		static let IA_AUTH_TOKEN_USER = "ia-auth-token-user"
		static let IA_AUTH_TOKEN_APP = "ia-auth-token-app"
		static let IA_APP_VERSION = "ia-app-version"
		static let IA_DEVICE_ARCHITECTURE = "ia-device-architecture"
		static let IA_DEVICE_ID = "ia-device-id"
		static let IA_DEVICE_LOCALE = "ia-device-locale"
		static let IA_DEVICE_MANUFACTURER = "ia-device-manufacturer"
		static let IA_DEVICE_MODEL = "ia-device-model"
		static let IA_OS_NAME = "ia-os-name"
		static let IA_OS_VERSION = "ia-os-version"
	}
	
	public var user: String = ""
	public var username: String = ""
	public var userToken: String = ""
	public var appToken: String = ""
	public var appVersion: String = ""
	
	public init() {}
	
	public func modifyRequest(request: inout URLRequest) {
		request.setValue(user, forHTTPHeaderField: Headers.IA_AUTH_USER)
		request.setValue(username, forHTTPHeaderField: Headers.IA_AUTH_USER_NAME)
		request.setValue(userToken, forHTTPHeaderField: Headers.IA_AUTH_TOKEN_USER)
		request.setValue(appToken, forHTTPHeaderField: Headers.IA_AUTH_TOKEN_APP)
		request.setValue(appVersion, forHTTPHeaderField: Headers.IA_APP_VERSION)
		
		request.setValue(deviceArchitecture, forHTTPHeaderField: Headers.IA_DEVICE_ARCHITECTURE)
		request.setValue(deviceIdentifier, forHTTPHeaderField: Headers.IA_DEVICE_ID)
		request.setValue(deviceLocale, forHTTPHeaderField: Headers.IA_DEVICE_LOCALE)
		request.setValue(deviceManufacturer, forHTTPHeaderField: Headers.IA_DEVICE_MANUFACTURER)
		request.setValue(deviceModel, forHTTPHeaderField: Headers.IA_DEVICE_MODEL)
		request.setValue(osName, forHTTPHeaderField: Headers.IA_OS_NAME)
		request.setValue(osVersion, forHTTPHeaderField: Headers.IA_OS_VERSION)
	}
	
	private var deviceArchitecture: String {
		var size = 0
		sysctlbyname("hw.machine", nil, &size, nil, 0)
		var machine = [CChar](repeating: 0,  count: Int(size))
		sysctlbyname("hw.machine", &machine, &size, nil, 0)
		return String(cString: machine)
	}
	
	private var deviceIdentifier: String {
		return UIDevice.current.identifierForVendor?.uuidString ?? ""
	}
	
	private var deviceLocale: String {
		return "Locale \(Locale.current.identifier); \(NSTimeZone.local.identifier)"
	}
	
	private var deviceManufacturer: String {
		return "Apple"
	}
	
	private var deviceModel: String {
		var systemInfo = utsname()
		uname(&systemInfo)
		let machineMirror = Mirror(reflecting: systemInfo.machine)
		let identifier = machineMirror.children.reduce("") { identifier, element in
			guard let value = element.value as? Int8, value != 0 else { return identifier }
			return identifier + String(UnicodeScalar(UInt8(value)))
		}
		
		if identifier == "x86_64" {
			return "Simulator"
		}
		
		return identifier
	}
	
	private var osName: String {
		return UIDevice.current.systemName
	}
	
	private var osVersion: String {
		return UIDevice.current.systemVersion
	}
}
