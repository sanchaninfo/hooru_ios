//
//  PhotoManager.swift
//  InspiringApps
//
//  Created by Brad Weber on 10/28/17.
//  Copyright © 2017 InspiringApps. All rights reserved.
//

import Foundation
import Photos

struct PhotoManager {
	static let jpegCompressionQuality: CGFloat = 0.7

	static func photoAccessIsAuthorized(_ callback: @escaping (_ authorized: Bool) -> ()) {
		switch PHPhotoLibrary.authorizationStatus() {
		case .authorized:
			callback(true)

		case .denied, .restricted:
			callback(false)

		case .notDetermined:
			PHPhotoLibrary.requestAuthorization({ (status) in
				switch status {
				case .authorized:
					callback(true)

				default:
					callback(false)
				}
			})
		}
	}
}
