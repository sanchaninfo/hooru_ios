//  hooru
//  Created by Erwin Mazariegos on 11/28/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

extension UIViewController {

	/// Instantiate a view controller from a storyboard
	func getController(ofClass controllerClass: UIViewController.Type, inStoryboardFile file: String = "") -> UIViewController {
				let storyboard = getStoryboard(file: file)
		return storyboard.instantiateViewController(withIdentifier: "\(controllerClass)")
	}

	/// Instantiate the initial view controller from a storyboard
	func getInitialController(inStoryboardFile file: String) -> UIViewController {
				let storyboard = getStoryboard(file: file)
		if let controller = storyboard.instantiateInitialViewController() {
			return controller
		} else {
			fatalError("Could not instantiate initial controller in storyboard \(file)")
		}
	}

	/// Instantiate and present a controller modally
	func presentController(ofClass controllerClass: UIViewController.Type, inStoryboardFile file: String = "", completion: (() -> Void)? = nil) {
				let controller = getController(ofClass: controllerClass, inStoryboardFile: file)
		present(controller: controller, completion: completion)
	}

	/// Present a controller modally
	func present(controller: UIViewController, completion: (() -> Void)? = nil) {
				self.present(controller, animated: true) {
			completion?()
		}
	}

	/// Instantiate and present modally the initial controller in the storyboard
	func presentInitialController(inStoryboardFile file: String) {
				let controller = getInitialController(inStoryboardFile: file)
		present(controller: controller)
	}

	/// Instantiate and push the initial controller in a storyboard to the navigation stack
	func showInitialController(inStoryboardFile file: String = "") {
				let controller = getInitialController(inStoryboardFile: file)
		show(controller: controller)
	}

	/// Instantiate and push a controller to the navigation stack
	func showController(ofClass controllerClass: UIViewController.Type, inStoryboardFile file: String = "") {
				let controller = getController(ofClass: controllerClass, inStoryboardFile: file)
		show(controller: controller)
	}

	/// Push a controller to the navigation stack
	func show(controller: UIViewController) {
				self.show(controller, sender: self)
	}

	func dismiss() {
				if let navController = self.navigationController {
			navController.popViewController(animated: true)
		} else {
			dismiss(animated: true, completion: nil)
		}
	}
    
    // Tap to hide keyboard
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }


	// MARK: - Private methods

	private func getStoryboard(file: String = "") -> UIStoryboard {
		
		let storyboard: UIStoryboard

		if file.isEmpty {
			if let board = self.storyboard {
				storyboard = board
			} else {
				fatalError("Cannot determine storyboard to use: No storyboard file specified, and calling VC has no storyboard")
			}
		} else {
			storyboard = UIStoryboard(name: file, bundle: nil)
		}

		return storyboard
	}


}
