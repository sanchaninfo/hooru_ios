//
//  Avatar.swift
//  InspiringApps
//
//  Created by Brad Weber on 10/26/17.
//  Copyright © 2017 InspiringApps. All rights reserved.
//

import Foundation
import UIKit

struct Avatar {
	private static let s3RegionName = "us-west-2"
	private static let s3BucketName = "ia-flyingspoke"
	private static let avatarDirectoryPath = Bundle.main.infoDictionary!["AVATAR_DirectoryPath"] as? String
	private static let awsAccessKey = Bundle.main.infoDictionary!["AWS_AccessKey"] as? String ?? "empty"
	private static let awsSecretKey = Bundle.main.infoDictionary!["AWS_SecretKey"] as? String ?? "empty"

	static func filenameForData(_ data: Data) -> String {
		let md5Hash = data.md5Hash().lowercased()
		return "\(md5Hash).jpg"
	}

	static func uploadAvatarData(_ data: Data, withFileName fileName: String, retry: Bool = true) {
				guard avatarDirectoryPath != nil else {
			IAAWS.printMessage("No AVATAR_directoryPath configured")
			return
		}

		let tempDirectoryURL = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
		let fileURL = tempDirectoryURL.appendingPathComponent(fileName)

		do {
			try data.write(to: fileURL, options: .atomic)

			let aws = IAAWS(accessKey: awsAccessKey, secretKey: awsSecretKey, regionName: s3RegionName, serviceName: "s3")
			IAAWSS3.deliverFileToBucket(aws: aws, bucketName: s3BucketName, serverFilePath: "\(avatarDirectoryPath!)\(fileName)", fromLocalURL: fileURL) { (success) in
				if !success {
					if retry {
						IAAWS.printMessage(" **** Error uploading avatar with filename: \(fileName), trying again")
						uploadAvatarData(data, withFileName: fileName, retry: false)
					} else {
						IAAWS.printMessage(" **** Error uploading avatar with filename: \(fileName), giving up")
					}
				}
				try? FileManager.default.removeItem(at: fileURL)
			}
		} catch {
			IAAWS.printMessage(" **** Error uploading avatar with filename: \(fileName)")
		}
	}

	static func cacheAvatarData(_ data: Data, withFileName fileName: String, oldFileName: String? = nil) {
//		
		guard let avatarDirectoryURL = avatarDirectory() else {
			return
		}

		if let oldFile = oldFileName {
			let fileURL = avatarDirectoryURL.appendingPathComponent(oldFile)
			if FileManager.default.fileExists(atPath: fileURL.relativePath) {
				try? FileManager.default.removeItem(at: fileURL)
			}
		}

		let fileURL = avatarDirectoryURL.appendingPathComponent(fileName)
		do {
			try data.write(to: fileURL, options: .atomic)
		} catch {
			IAAWS.printMessage(" **** Error saving avatar with filename: \(fileName)")
		}
	}

	static func avatarDataForFileName(_ fileName: String, completion: @escaping (_ data: Data?) -> ()) {
		guard let avatarDirectoryURL = avatarDirectory() else {
			completion(nil)
			return
		}

		guard avatarDirectoryPath != nil else {
			IAAWS.printMessage("No AVATAR_directoryPath configured")
			completion(nil)
			return
		}

		let fileURL = avatarDirectoryURL.appendingPathComponent(fileName)

		if FileManager.default.fileExists(atPath: fileURL.relativePath) {
			if let data = try? Data(contentsOf: fileURL) {
				completion(data)
			} else {
				completion(nil)
			}
			return
		}

		// Couldn't find the file locally. Retrieve it.
		let aws = IAAWS(accessKey: awsAccessKey, secretKey: awsSecretKey, regionName: s3RegionName, serviceName: "s3")
		IAAWSS3.retrieveFileFromBucket(aws: aws, bucketName: s3BucketName, serverFilePath: "\(avatarDirectoryPath!)\(fileName)", toLocalURL: fileURL) { (success) in
			guard success else {
				completion(nil)
				return
			}

			if let data = try? Data(contentsOf: fileURL) {
				completion(data)
				return
			}

			completion(nil)
		}
	}

	static func avatarImageForFileName(_ fileName: String, completion: @escaping (_ image: UIImage?) -> ()) {
//		
		avatarDataForFileName(fileName, completion: { (data) in
			if let imageData = data, let image = UIImage(data: imageData) {
				completion(image)
			} else {
				completion(nil)
			}
		})
	}

	private static func avatarDirectory() -> URL? {
		let fileManager = FileManager.default

		let documentDirectories = fileManager.urls(for: .documentDirectory, in: .userDomainMask)

		guard 0 < documentDirectories.count else {
			return nil
		}

		let documentsDirectory = documentDirectories.first!
		let avatarDirectory = documentsDirectory.appendingPathComponent("Avatar")
		let avatarDirectoryPath = avatarDirectory.path

		// Does the directory exist?
		if fileManager.fileExists(atPath: avatarDirectoryPath) {
			return URL(fileURLWithPath: avatarDirectoryPath, isDirectory: true)
		}

		// Directory doesn't yet exist. Create it.
		do {
			try fileManager.createDirectory(atPath: avatarDirectoryPath, withIntermediateDirectories: false, attributes: nil)
			return URL(fileURLWithPath: avatarDirectoryPath, isDirectory: true)

		} catch {
			IAAWS.printMessage("***** ERROR: avatarDirectory creation: \(error.localizedDescription)" )
		}

		return nil
	}

}
