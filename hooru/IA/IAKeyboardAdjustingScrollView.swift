//
//  IAKeyboardAdjustingScrollView.swift
//  InspiringApps
//
//  Created by Brad Weber on 10/19/17.
//  Copyright © 2017 InspiringApps. All rights reserved.
//

import UIKit

class IAKeyboardAdjustingScrollView: UIScrollView {

	var adjustableBottomContraint = NSLayoutConstraint()
	var activeField: UITextField?

	override func willMove(toSuperview newSuperview: UIView?) {
		super.willMove(toSuperview: newSuperview)

		if newSuperview == nil {
			deregisterForKeyboardNotifications()
		} else {
			registerForKeyboardNotifications()
		}
	}

	// MARK: - Keyboard notifications
	fileprivate func registerForKeyboardNotifications() {
		NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillChangeFrame(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
	}

	fileprivate func deregisterForKeyboardNotifications() {
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
	}

	@objc func handleKeyboardWillChangeFrame(_ notification: Notification) {

		guard let info = notification.userInfo else {
			return
		}

		guard let keyboardRect = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
			return
		}

		let keyboardTop = keyboardRect.origin.y
		let windowHeight = window?.bounds.height ?? keyboardTop
		var offset = contentOffset.y

//		let originalConstant = adjustableBottomContraint.constant

		if windowHeight <= keyboardTop {
			adjustableBottomContraint.constant = 0
		} else {
			let inset = windowHeight - keyboardTop
			if inset > 0 {
				adjustableBottomContraint.constant = inset
				if let field = activeField {
					let fieldBottom = field.frame.maxY + frame.origin.y + 10
					if fieldBottom > keyboardTop {
						offset = fieldBottom - keyboardTop
					}
				}
			}
		}

//		print("offset: \(contentOffset.y), constant: \(originalConstant), fieldbottom: \((activeField?.frame.maxY ?? 0) + frame.origin.y + 10), keyboard: \(keyboardTop), |||| new offset: \(offset), new constant: \(adjustableBottomContraint.constant)")

		let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.25

		let animationCurveRawNSN = info[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
		let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
		let animationCurve = UIViewAnimationOptions(rawValue: animationCurveRaw)

		UIView.animate(withDuration: duration, delay: 0, options: animationCurve, animations: {
			self.layoutIfNeeded()
			self.contentOffset = CGPoint(x: 0, y: offset)
		})

	}

	deinit {
		deregisterForKeyboardNotifications()
	}
}
