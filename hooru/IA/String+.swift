//
//  String+.swift
//  InspiringApps
//
//  Created by Brad Weber on 6/9/16.
//  Copyright © 2016 InspiringApps. All rights reserved.
//

import Foundation

extension String {
	func asCleanPhoneNumber() -> String? {
		// Clean phone numbers must have no punctuation or spaces
		// and be exactly 10 digits. We're assuming US phone numbers
		// only for now.
		let validPhoneLength = 10

		let pattern = "\\D"
		let options: NSRegularExpression.Options = []
		let matchingOptions: NSRegularExpression.MatchingOptions = []
		let range = NSRange(location: 0, length: NSString(string: self).length)

		if let regEx = try? NSRegularExpression(pattern: pattern, options: options) {
			let noPunctuation = regEx.stringByReplacingMatches(in: self, options: matchingOptions, range: range, withTemplate: "")

			let noPunctuationLen = NSString(string: noPunctuation).length
			switch true {
			case noPunctuationLen == validPhoneLength:
				return noPunctuation // Proper length, no punctuation. We're done.

			case noPunctuationLen == validPhoneLength + 1 && noPunctuation.substring(fromIndex: 0, length: 1) == "1":
				return self.substring(fromIndex: 1, length: NSString(string: self).length - 1)

			default:
				print("Invalid phone format/length: \(self)")
			}
		}

		return nil
	}

	func splitIntoNameComponents() -> (firstName: String?, lastName: String?) {
		var firstName: String? = nil
		var lastName: String? = nil

		let nameComponents = self.components(separatedBy: " ")

		if 0 < nameComponents.count {
			firstName = nameComponents[0]
		}

		if 1 < nameComponents.count {
			for i in (1 ..< nameComponents.count) {
				if lastName == nil {
					lastName = nameComponents[i]
				} else {
					lastName! += " \(nameComponents[i])"
				}
			}
		}

		return (firstName: firstName, lastName: lastName)
	}

	func substring(fromIndex: Int, toIndex: Int) -> String {
		let start = self.index(self.startIndex, offsetBy: fromIndex)
		let end = self.index(self.startIndex, offsetBy: toIndex)

		return String(self[start ..< end])
	}

	func substring(fromIndex: Int, length: Int) -> String {
		return substring(fromIndex: fromIndex, toIndex: fromIndex + length)
	}
    
    func sanitized() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
}
