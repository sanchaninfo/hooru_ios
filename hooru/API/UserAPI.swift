//  hooru
//  Created by Brad Weber on 3/21/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

struct UserAPI {
	static func registerUser(_ user: Participant, completion: @escaping (_ error: HooruError?) -> Void) -> Response {
		guard user.email != nil && user.password != nil else {
			completion(HooruError(message: "Missing user email or password in UserAPI.registerUser."))
			return Response()
		}

		var json = ["email": user.email!, "password": user.password!]
		if user.firstName != nil {
			json["firstName"] = user.firstName!
		}

		if user.lastName != nil {
			json["lastName"] = user.lastName!
		}

		if user.avatarFileName != nil {
			json["avatarFilename"] = user.avatarFileName!
		}

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/user/register/")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(HooruError(error: error!))
					return
				}

				user.update(json: jsonObject)
				completion(nil)
			})

		return webCall.call()
	}

	static func login(email: String, password: String, completion: @escaping (_ error: HooruError?) -> Void) -> Response {
		let json = ["email": email, "password": password]

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/user/login/")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(HooruError(error: error!))
					return
				}

				if let userID = jsonObject["id"] as? Int, let webToken = jsonObject["ia-auth-token-user"] as? String {
					let user = Participant(id: userID, webToken: webToken)
					AccountManager.currentLoggedInUser = user

					_ = getUser(completion: completion)
				} else {
					completion(nil)
				}
			})

		return webCall.call()
	}

	static func resendVerification(email: String, completion: @escaping (_ error: HooruError?) -> Void) -> Response {
		let json = ["email": email]

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/user/resendValidationCode/")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(HooruError(error: error!))
					return
				}

				if jsonObject["id"] as? Int != nil {
					completion(nil)
				} else {
					// TODO
				}
			})

		return webCall.call()
	}

	static func resetPassword(email: String, completion: @escaping (_ error: HooruError?) -> Void) -> Response {
		let json = ["email": email]

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/user/resetPassword/")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(HooruError(error: error!))
					return
				}

				if jsonObject["success"] as? String != nil {
					completion(nil)
				} else {
					// TODO
				}
			})

		return webCall.call()
	}


	static func getUser(completion: @escaping (_ error: HooruError?) -> Void) -> Response {
		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/user/")
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(HooruError(error: error!))
					return
				}

				AccountManager.currentLoggedInUser?.update(json: jsonObject)

				completion(nil)
			})

		return webCall.call()
	}

	static func updateUser(_ user: Participant, completion: @escaping (_ error: HooruError?) -> Void) -> Response {
		var json = [String: Any]()
		if user.email != nil {
			json["email"] = user.email!
		}

		if user.password != nil {
			json["password"] = user.password!
		}

		if user.firstName != nil {
			json["firstName"] = user.firstName!
		}

		if user.lastName != nil {
			json["lastName"] = user.lastName!
		}

		if user.avatarFileName != nil {
			json["avatarFilename"] = user.avatarFileName!
		}

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/user/")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(HooruError(error: error!))
					return
				}

				user.update(json: jsonObject)

				completion(nil)
			})

		return webCall.call()
	}

	static func getUserAvatarsForEmailAddresses(_ emailAddresses: [String], completion: @escaping (_ userAvatars: [UserAvatar], _ error: HooruError?) -> Void) -> Response {
		let json = ["emails": emailAddresses]

		let webCall = WebCall(service: HooruWebService.service)
		.setEndpoint("/getAvatars/")
		.addRequestModifier(PostRequestModifier(json: json))
		.addResponseHandler(HooruErrorResponseHandler())
		.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
			var avatars = [UserAvatar]()

			guard error == nil else {
				completion(avatars, HooruError(error: error!))
				return
			}

			if let avatarObjects = jsonObject["avatars"] as? [[String: Any]] {
				for avatarObject in avatarObjects {
					let userAvatar = UserAvatar(json: avatarObject)
					avatars.append(userAvatar)
				}
			}

			completion(avatars, nil)
		})

		return webCall.call()
	}
}
