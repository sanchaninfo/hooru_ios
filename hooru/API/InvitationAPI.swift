//  hooru
//  Created by Brad Weber on 3/21/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

struct InvitationAPI {
	static func sendInvitation(_ invitation: Invitation, completion: @escaping (_ success: Bool, _ error: HooruError?) -> Void) -> Response {
		var json = [String: Any?]()
		var recipients = [[String: Any?]]()
		var questionIDs = [Int]()

		for recipient in invitation.recipients {
			guard recipient.email != nil else {
				continue
			}

			var recipientDictionary: [String: Any?] = ["email": recipient.email!]
			if recipient.firstName != nil {
				recipientDictionary["firstName"] = recipient.firstName!
			}

			if recipient.lastName != nil {
				recipientDictionary["lastName"] = recipient.lastName!
			}

			recipients.append(recipientDictionary)
		}

		for recipientEmail in invitation.nonContactRecipients {
			recipients.append(["email": recipientEmail])
		}

		for question in invitation.questions {
			if let id = question.id {
				questionIDs.append(id)
			}
		}

		json["recipients"] = recipients
		json["questionIds"] = questionIDs

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/requestGroup/")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				var success = false

				guard error == nil else {
					completion(success, HooruError(error: error!))
					return
				}

				if let _ = jsonObject["requests"] as? [[String: Any]] {
					success = true
				}

				completion(success, nil)
			})

		return webCall.call()
	}
}
