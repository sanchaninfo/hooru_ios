//  hooru
//  Created by Brad Weber on 3/20/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

struct QuestionAPI {
	static func getQuestions(completion: @escaping (_ questions: [Question], _ error: HooruError?) -> Void) -> Response {
		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/questions/")
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				var questions = [Question]()

				guard error == nil else {
					completion(questions, HooruError(error: error!))
					return
				}

				if let questionObjects = jsonObject["questions"] as? [[String: Any]] {
					for questionObject in questionObjects {
						let question = Question(json: questionObject)
						questions.append(question)
					}
				}

				completion(questions, nil)
			})

		return webCall.call()
	}
}
