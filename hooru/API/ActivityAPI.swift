//  hooru
//  Created by Brad Weber on 4/3/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

struct ActivityAPI {
	static func getActivities(completion: @escaping (_ activities: [Activity], _ error: HooruError?) -> Void) -> Response {
		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/getActivity/")
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONArrayResponseHandler() { (jsonObjects, error) in
				var activities = [Activity]()

				guard error == nil else {
					completion(activities, HooruError(error: error!))
					return
				}

				for activityObject in jsonObjects {
					let activity = Activity(json: activityObject)
					activities.append(activity)
				}

				completion(activities, nil)
			})

		return webCall.call()
	}
}
