//  hooru
//  Created by Brad Weber on 4/3/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

struct AlbumAPI {
	static func getAlbumsForUser(_ user: Participant, completion: @escaping (_ albums: [Album], _ error: HooruError?) -> Void) -> Response {
		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/getLibraries/")
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONArrayResponseHandler() { (jsonObjects, error) in
				var albums = [Album]()

				guard error == nil else {
					completion(albums, HooruError(error: error!))
					return
				}

				for albumObject in jsonObjects {
					let album = Album(json: albumObject)
					albums.append(album)
				}

				let sortedAlbums = albums.sorted(by: { (album1, album2) -> Bool in
					return album1.name ?? "" < album2.name ?? ""
				})

				completion(sortedAlbums, nil)
			})

		return webCall.call()
	}

	static func createAlbum(_ name: String, withPrompts prompts: [Prompt], completion: @escaping (_ album: Album?, _ error: HooruError?) -> Void) -> Response {
		var promptIDs = [Int]()
		for prompt in prompts {
			guard prompt.id != nil else {
				continue
			}

			promptIDs.append(prompt.id!)
		}

		let json: [String: Any?] = ["name": name, "items": promptIDs]

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/addLibrary/")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(nil, HooruError(error: error!))
					return
				}

				let album = Album(json: jsonObject)
				completion(album, nil)
			})

		return webCall.call()
	}

	static func deleteAlbum(_ album: Album, completion: @escaping (_ success: Bool, _ error: HooruError?) -> Void) -> Response {
		let albumID = album.id ?? -1

		let json: [String: Any?] = ["isDeleted": true]

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/updateLibrary/\(albumID)")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(false, HooruError(error: error!))
					return
				}

				completion(true, nil)
			})

		return webCall.call()
	}

	static func updateName(_ name: String, forAlbum album: Album, completion: @escaping (_ album: Album?, _ error: HooruError?) -> Void) -> Response {
		let albumID = album.id ?? -1

		let json: [String: Any?] = ["name": name]

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/updateLibrary/\(albumID)")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(nil, HooruError(error: error!))
					return
				}

				let album = Album(json: jsonObject)
				completion(album, nil)
			})

		return webCall.call()
	}

	static func addPrompt(_ prompt: Prompt, toAlbum album: Album, completion: @escaping (_ album: Album?, _ error: HooruError?) -> Void) -> Response {
		let promptID = prompt.id ?? -1
		let albumID = album.id ?? -1

		let json: [String: Any?] = ["addItems": [promptID]]

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/updateLibrary/\(albumID)")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(nil, HooruError(error: error!))
					return
				}

				let album = Album(json: jsonObject)
				completion(album, nil)
			})

		return webCall.call()
	}

	static func removePrompt(_ prompt: Prompt, fromAlbum album: Album, completion: @escaping (_ album: Album?, _ error: HooruError?) -> Void) -> Response {
		let promptID = prompt.id ?? -1
		let albumID = album.id ?? -1

		let json: [String: Any?] = ["removeItems": [promptID]]

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/updateLibrary/\(albumID)")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				guard error == nil else {
					completion(nil, HooruError(error: error!))
					return
				}

				let album = Album(json: jsonObject)
				completion(album, nil)
			})

		return webCall.call()
	}
}
