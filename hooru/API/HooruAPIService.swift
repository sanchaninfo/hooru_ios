//  hooru
//  Created by Brad Weber on 3/20/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation
import UIKit

final class HooruWebService {
	private static var webService: WebService!

	static var service: WebService = {

		webService = WebService()

		let iaRequestModifier = InspiringAppsRequestModifier()
		iaRequestModifier.appToken = Bundle.main.infoDictionary!["API_KEY"] as? String ?? ""
		iaRequestModifier.appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""

		webService
			.setServerPath(Bundle.main.infoDictionary!["API_BASE_URL"] as? String ?? "")
			.setReauthenticationHandler(CustomReauthenticationHandler())
			.addRequestModifier(ClosureRequestModifier { request in
				if let token = AccountManager.currentLoggedInUser?.webToken {
					request.setValue(token, forHTTPHeaderField: "Authorization")
				}
			})
			.addRequestModifier(iaRequestModifier)

		return webService
	}()

	class CustomReauthenticationHandler: ReauthenticationHandler {
		func reauthenticate() {
			DispatchQueue.main.async {
				// TODO: BDW
				// NotificationCenter.default.post(name: .onShouldAuthenticateUser, object: self, userInfo: nil)
			}
		}
	}
}

public struct HooruError: Error {
	let error: Error?
	let message: String

	init(error: Error) {
		if let hooruError = error as? HooruError {
			self.error = hooruError.error
			self.message = hooruError.message
		} else {
			self.error = error
			self.message = error.localizedDescription
		}
	}

	init(message: String) {
		self.error = nil
		self.message = message
	}

	init(error: Error, message: String) {
		self.error = error
		self.message = message
	}

	init(json: [String: Any]) {
		error = nil

		guard let errorTypeString = json["error"] as? String else {
			message = ""
			return
		}

		guard let type = ErrorType(rawValue: errorTypeString) else {
			message = ""
			return
		}

		message = type.userDisplayMessage()
	}

	enum ErrorType: String {
		case userExists = "USER_EXISTS"

		func userDisplayMessage() -> String {
			switch self {
			case .userExists:
				return "You already have a user account. Please log in."
			}
		}
	}
}
