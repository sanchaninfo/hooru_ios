//  hooru
//  Created by Brad Weber on 4/3/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

struct PromptAPI {
	static func getIncomingYouAskedPromptGroups(completion: @escaping (_ promptGroups: [PromptGroup], _ error: HooruError?) -> Void) -> Response {
		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/requestGroup/")
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				var promptGroups = [PromptGroup]()

				guard error == nil else {
					completion(promptGroups, HooruError(error: error!))
					return
				}

				if let promptGroupObjects = jsonObject["requests"] as? [[String: Any?]] {
					for promptGroupObject in promptGroupObjects {
						let promptGroup = PromptGroup(json: promptGroupObject)

						// Don't include pending prompt groups in results
						if !promptGroup.isPending {
							promptGroups.append(promptGroup)
						}
					}
				}

				completion(promptGroups, nil)
			})

		return webCall.call()
	}
	
	static func getIncomingTheyAskedPromptGroups(completion: @escaping (_ promptGroups: [PromptGroup], _ error: HooruError?) -> Void) -> Response {
		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/inbox/")
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				var promptGroups = [PromptGroup]()

				guard error == nil else {
					completion(promptGroups, HooruError(error: error!))
					return
				}

				if let promptGroupObjects = jsonObject["requests"] as? [[String: Any?]] {
					for promptGroupObject in promptGroupObjects {
						let promptGroup = PromptGroup(json: promptGroupObject)

						promptGroups.append(promptGroup)
					}
				}

				completion(promptGroups, nil)
			})

		return webCall.call()
	}


	static func getMyVideos(completion: @escaping (_ promptGroups: [PromptGroup], _ error: HooruError?) -> Void) -> Response {
		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/outbox/")
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				var promptGroups = [PromptGroup]()

				guard error == nil else {
					completion(promptGroups, HooruError(error: error!))
					return
				}

				if let promptGroupObjects = jsonObject["requests"] as? [[String: Any?]] {
					for promptGroupObject in promptGroupObjects {
						let promptGroup = PromptGroup(json: promptGroupObject)

						promptGroups.append(promptGroup)
					}
				}

				completion(promptGroups, nil)
			})

		return webCall.call()
	}


	// Mark Request Group as Read
	static func markPromptGroupAsRead(_ promptGroup: PromptGroup, completion: @escaping (_ success: Bool, _ error: HooruError?) -> Void) -> Response {
		let promptGroupID = promptGroup.id ?? -1

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/requestGroup/read/\(promptGroupID)")
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				var success = false

				guard error == nil else {
					completion(success, HooruError(error: error!))
					return
				}

				if let successString = jsonObject["success"] as? String {
					success = successString == "OK"
				}

				completion(success, nil)
			})

		return webCall.call()
	}

	// Mark Request Group as Sent
	static func markPromptGroupAsSent(_ promptGroup: PromptGroup, completion: @escaping (_ success: Bool, _ error: HooruError?) -> Void) -> Response {
		let promptGroupID = promptGroup.id ?? -1

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/requestGroup/sent/\(promptGroupID)")
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				var success = false

				guard error == nil else {
					completion(success, HooruError(error: error!))
					return
				}

				if let successString = jsonObject["success"] as? String {
					success = successString == "OK"
				}

				completion(success, nil)
			})

		return webCall.call()
	}

	// Update video response for prompt
	static func updateVideoResponseForPrompt(_ prompt: Prompt, completion: @escaping (_ success: Bool, _ error: HooruError?) -> Void) -> Response {
		let promptID = prompt.id ?? -1
		let fileName = prompt.responseLocalFileURL?.lastPathComponent ?? ""

		let json: [String: Any?] = ["responseFilename": fileName]

		let webCall = WebCall(service: HooruWebService.service)
			.setEndpoint("/requestGroup/update/\(promptID)")
			.addRequestModifier(PostRequestModifier(json: json))
			.addResponseHandler(HooruErrorResponseHandler())
			.addResponseHandler(JSONObjectResponseHandler() { (jsonObject, error) in
				var success = false

				guard error == nil else {
					completion(success, HooruError(error: error!))
					return
				}

				if let responseObject = jsonObject["response"] as? [String: Any?] {
					success = responseObject["id"] as? Int == promptID
				}

				completion(success, nil)
			})

		return webCall.call()
	}
}
