//  hooru
//  Created by Brad Weber on 3/20/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

/// Parses out the data response to check for a common "errors" object in typical responses.
/// If one exists, it sets the error of the response to an app-specific error with the passed in message.
class HooruErrorResponseHandler: ResponseHandler {

	public enum Errors: Error {
		case HooruServerError
	}

	public typealias CompletionHandler = (_ response: Response, _ hooruError: HooruError) -> Void

	let completion: CompletionHandler?

	init(completion: CompletionHandler? = nil) {
		self.completion = completion
	}

	func handleResponse(response: Response) {
		guard let data = response.data else {
			return
		}

		guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
			return
		}

		guard let jsonObject = json as? JSONResponseHandler.JSONObject else {
			return
		}

		if let jsonError = jsonObject["errors"] as? String {
			let hooruError = HooruError(error: Errors.HooruServerError, message: jsonError)
			// response.overwriteError(error: hooruError) TODO: BDW
			completion?(response, hooruError)
		}
	}
}
