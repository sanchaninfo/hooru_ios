//  hooru
//  Created by Erwin Mazariegos on 12/12/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import Foundation

class Prompt {
	var id: Int?
	var groupID: Int?
	var question: Question?

	var responseRemoteURL: URL?

	var responseLocalFileURL: URL? {
		willSet {
			VideoService.deleteLocalFileAt(responseLocalFileURL)
		}
	}

	var hasResponse: Bool {
		return responseRemoteURL != nil
	}

	var order: Int?


	init() {

	}

	init(json: [String: Any?]) {
		id = json["id"] as? Int

		// Will pull question and questionID from json
		question = Question(json: json)

		if let fileName = json["responseFilename"] as? String {
			responseRemoteURL = VideoService.remoteURLForFilename(fileName)
		}
	}

/*
	static let notSetId = -1

	var serverId: Int = Prompt.notSetId
	let text: String
	let uuid = UUID()

	var promptOrder: Int?		// used for ordering, but will be an arbitrary integer (row id of sent cue in server db)
	var answerDate: Date?
	var answerRemoteUrl: URL?

	var answerLocalFileUrl: URL? {
		willSet {
			VideoService.deleteLocalFileAt(answerLocalFileUrl)
		}
	}

	var hasAnswer: Bool {
		return answerRemoteUrl != nil
	}

	init(question: String) {
		text = question
	}

	enum CodingKeys: String, CodingKey {
		case serverId = "questionID"
		case text = "question"
		case promptOrder = "id"
		case answerCreatedDate = "created"
		case answerModifiedDate = "modified"
		case responseFilename
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)

		try container.encode(serverId, forKey: .serverId)
		try container.encode(text, forKey: .text)
		try container.encode(promptOrder, forKey: .promptOrder)

		if let remoteUrl = answerRemoteUrl {
			let remoteFileName = remoteUrl.lastPathComponent
			try container.encode(remoteFileName, forKey: .responseFilename)
		}
	}

	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)

		guard let questionText = try container.decodeIfPresent(String.self, forKey: .text) else {
			throw APIService.APIError.question(underlyingError: .noText)
		}
		self.text = questionText

		if let id = try container.decodeIfPresent(Int.self, forKey: .serverId) {
			self.serverId = id
		} else {
			self.serverId = Prompt.notSetId
		}

		self.promptOrder = try container.decodeIfPresent(Int.self, forKey: .promptOrder)

		if let fileName = try container.decodeIfPresent(String.self, forKey: .responseFilename) {
			self.answerRemoteUrl = VideoService.remoteURLForFilename(fileName)
		}

		if let modified = try container.decodeIfPresent(Date.self, forKey: .answerModifiedDate) {
			self.answerDate = modified
		} else {
			self.answerDate = try container.decodeIfPresent(Date.self, forKey: .answerCreatedDate)
		}
	}
	*/
}

extension Prompt: Equatable, Hashable {
	var hashValue: Int {
		return id ?? 0
	}

	static func ==(lhs: Prompt, rhs: Prompt) -> Bool {
		return lhs.id == rhs.id
	}
}

/*
struct Prompts: Codable {
	let prompts: [Prompt]

	enum CodingKeys: String, CodingKey {
		case prompts = "questions"
	}

	func encode(to encoder: Encoder) throws {

	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.prompts = try container.decode([Prompt].self, forKey: .prompts)
	}
}
*/
