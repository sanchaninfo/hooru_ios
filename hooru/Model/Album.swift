//  hooru
//  Created by Brad Weber on 3/18/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

class Album {
	var id: Int?
	var name: String?
	var prompts = [Prompt]()
	var participants = [Participant]()

	init (json: [String: Any?]) {
		id = json["id"] as? Int
		name = json["name"] as? String

		if let responseObjects = json["responses"] as? [[String: Any?]] {
			for responseObject in responseObjects {
				// Album data isn't split into component parts, so we'll create those here
				let promptObject: [String: Any?] = ["id": responseObject["promptID"] as? Int,
													"question": responseObject["question"] as? String,
													"questionID": responseObject["questionID"] as? Int,
													"responseFilename": responseObject["responseFilename"] as? String]

				let prompt = Prompt(json: promptObject)
				prompts.append(prompt)


				let participantObject: [String: Any?] = ["id": responseObject["userID"] as? Int,
													   "email": responseObject["email"] as? String,
													   "firstName": responseObject["firstName"] as? String,
													   "lastName": responseObject["lastName"] as? String,
													   "avatarFilename": responseObject["avatarFilename"] as? String]

				let participant = Participant(json: participantObject)
				participants.append(participant)
			}
		}
	}
}
