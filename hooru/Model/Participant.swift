//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class Participant {
	var id: Int?
	var created: Date?
	var modified: Date?
	var firstName: String?
	var lastName: String?
	var email: String?
	var password: String?
	var avatarFileName: String?
	var isVerified = false
	var hasPendingInvitations = false
	var hasAnsweredAtLeastOnce = false
	var webToken: String?


	private var retrievedAvatarImage: UIImage?
	@objc dynamic var avatarImageName: String? = nil {
		didSet {
			if avatarFileName != avatarImageName {
				avatarFileName = avatarImageName
			}

			retrievedAvatarImage = nil
		}
	}

	func avatarImage(callback: @escaping (UIImage) -> ()) {
		if let image = retrievedAvatarImage {
			callback(image)
			return
		}

		if let fileName = avatarImageName ?? avatarFileName {
			Avatar.avatarImageForFileName(fileName, completion: { (avatarImage) in
				if let image = avatarImage {
					self.retrievedAvatarImage = image
					callback(image)

				} else {
					callback(Design.Elements.genericAvatar)
				}
			})
			
		} else {
			callback(Design.Elements.genericAvatar)
		}
	}

	var debugVerificationCode: String?

	init(id: Int, webToken: String) {
		self.id = id
		self.webToken = webToken
	}

	init(first: String? = nil, last: String? = nil, emailString: String? = nil) {
		firstName = first
		lastName = last
		email = emailString
	}

	init(json: [String: Any?]) {
		id = json["id"] as? Int

		update(json: json)
	}

	var fullName: String {
		var nameParts = [String]()

		if firstName != nil {
			nameParts.append(firstName!)
		}

		if lastName != nil {
			nameParts.append(lastName!)
		}

		return nameParts.joined(separator: " ")
	}
    
	var indexCharacter: String {
		if let firstChar = sortTerm.first {
			return String(firstChar).uppercased()
		}
		return "#"
	}

	func update(json: [String: Any?]) {
		created = Date.dateFromDatabaseDateTime(json["created"] as? String) ?? created
		modified = Date.dateFromDatabaseDateTime(json["modified"] as? String) ?? modified

		firstName = json["firstName"] as? String ?? firstName
		lastName = json["lastName"] as? String ?? lastName
		avatarFileName = json["avatarFilename"] as? String ?? avatarFileName
		email = json["email"] as? String ?? email

		if let isVerifiedNum = json["isVerified"] as? Int {
			isVerified = isVerifiedNum == 1
		}

		if let hasPendingInvitationsNum = json["hasPendingInvitations"] as? Int {
			hasPendingInvitations = hasPendingInvitationsNum == 1
		}

		if let hasAnsweredAtLeastOnceNum = json["hasAnsweredAtLeastOnce"] as? Int {
			hasAnsweredAtLeastOnce = hasAnsweredAtLeastOnceNum == 1
		}
	}


	func updateFrom(_ updatedUser: Participant) {
		
		created = updatedUser.created ?? created
		modified = updatedUser.modified ?? modified
		firstName = updatedUser.firstName ?? firstName
		lastName = updatedUser.lastName ?? lastName
		email = updatedUser.email ?? email
		password = updatedUser.password ?? password
		avatarFileName = updatedUser.avatarFileName ?? avatarFileName

		debugVerificationCode = updatedUser.debugVerificationCode ?? debugVerificationCode

		isVerified = 	updatedUser.isVerified
		hasPendingInvitations = updatedUser.hasPendingInvitations
		hasAnsweredAtLeastOnce = updatedUser.hasAnsweredAtLeastOnce

		webToken = updatedUser.webToken ?? webToken
	}

	private var sortTerm: String {
		let nonNullData = [firstName, lastName, email].compactMap({ $0 })
		return nonNullData.joined().lowercased()
	}
}

extension Participant: Comparable {
	static func == (lhs: Participant, rhs: Participant) -> Bool {

		switch (lhs.email, rhs.email) {
		case (.none, .some):
			return false
		case (.some, .none):
			return false
		case (.none, .none):
			return lhs.firstName?.lowercased() == rhs.firstName?.lowercased()
				&& lhs.lastName?.lowercased() == rhs.lastName?.lowercased()
		case (.some, .some):
			if let email1 = lhs.email, let email2 = rhs.email {
				return email1.lowercased() == email2.lowercased()
			}
			return false
		}
	}

	static func < (lhs: Participant, rhs: Participant) -> Bool {
		return lhs.sortTerm < rhs.sortTerm
	}

	static func > (lhs: Participant, rhs: Participant) -> Bool {
		return lhs.sortTerm > rhs.sortTerm
	}
}
