//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import Foundation

class PromptGroup {
	var id: Int?
	var creationDate: Date?
	var sender: Participant? // Question sender
	var recipient: Participant? // Question responder (video recorder)
	var isComplete: Bool = false
	var isRead: Bool = true
	var status: String?
	var isPending: Bool {
		return status == "Pending"
	}

	var prompts = [Prompt]()

	var responseProgress: Float {
		guard 0 < prompts.count else {
			return 0
		}

		let responseCount: Float = prompts.reduce(0, { sum, prompt in
			sum + (prompt.hasResponse ? 1 : 0)
		})

		return responseCount / Float(prompts.count)
	}


	init () {

	}

	init(json: [String: Any?]) {
		id = json["id"] as? Int
		creationDate = Date.dateFromDatabaseDateTime(json["created"] as? String)

		if let senderObject = json["author"] as? [String: Any?] {
			sender = Participant(json: senderObject)
		}

		if let recipientObject = json["recipient"] as? [String: Any?] {
			recipient = Participant(json: recipientObject)
		}

		if let isReadNum = json["isRead"] as? Int {
			isRead = isReadNum == 1
		}

		status = json["status"] as? String

		if let promptObjects = json["questions"] as? [[String: Any?]] {
			for promptObject in promptObjects {
				let prompt = Prompt(json: promptObject)
				prompt.groupID = id
				prompts.append(prompt)
			}
		}
	}

/*
	static let notSetId = -1

	var serverId: Int = PromptGroup.notSetId

	var creationDate: Date
	var sendDate: Date?
	var hasBeenViewed = false
	var isPending = true

	var completionProgress: Float {
		guard 0 < prompts.count else {
			return 0
		}

		if !isPending {
			return 1
		}

		let answeredCount: Float = prompts.reduce(0, { sum, prompt in
			sum + (prompt.hasAnswer ? 1 : 0)
		})

		return answeredCount / Float(prompts.count)
	}

	init() {
		prompts = [Prompt]()
		sender = Participant()
		recipient = Participant()

		creationDate = Date()
		sendDate = nil
	}

	func discardAnswers() {
				prompts.forEach { (prompt) in
			prompt.answerLocalFileUrl = nil
		}
	}

	enum CodingKeys: String, CodingKey {
		case serverId = "id"
		case sender = "author"
		case prompts = "questions"
		case created, modified, recipient, status, isRead
	}

	func encode(to encoder: Encoder) throws {

	}

	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)

		if let id = try container.decodeIfPresent(Int.self, forKey: .serverId) {
			self.serverId = id
		} else {
			self.serverId = PromptGroup.notSetId
		}

		do {
			self.prompts = try container.decode([Prompt].self, forKey: .prompts)
		} catch {
			if case APIService.APIError.question(let reason) = error, case .noText = reason {
				self.prompts = [Prompt]()
			} else {
				throw error
			}
		}

		self.sender = try container.decode(Participant.self, forKey: .sender)
		self.recipient = try container.decode(Participant.self, forKey: .recipient)
		self.isPending = try container.decode(String.self, forKey: .status).lowercased() == "pending"

		if let isRead = try container.decodeIfPresent(Int.self, forKey: .isRead) {
			self.hasBeenViewed = isRead != 0
		}

		self.creationDate = try container.decode(Date.self, forKey: .created)
		if let updated = try container.decodeIfPresent(Date.self, forKey: .modified) {
			self.sendDate = updated
		} else {
			self.sendDate = self.creationDate
		}
	}
	*/
}

/*
struct PromptGroups: Codable {
	let promptGroups: [PromptGroup]

	enum CodingKeys: String, CodingKey {
		case promptGroups
	}

	func encode(to encoder: Encoder) throws {

	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.promptGroups = try container.decode([PromptGroup].self, forKey: .promptGroups)
	}
}
*/
