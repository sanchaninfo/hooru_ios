//  hooru
//  Created by Erwin Mazariegos on 1/11/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

class Invitation {
	var questions = [Question]()
	var sender = Participant()
	var recipients = [Participant]()
	var nonContactRecipients = [String]()
	var creationDate = Date()
	var sendDate: Date?
}
