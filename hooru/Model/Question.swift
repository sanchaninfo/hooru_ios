//  hooru
//  Created by Brad Weber on 3/20/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

class Question {
	var id: Int?
	var text: String?

	init(id: Int, text: String) {
		self.id = id
		self.text = text
	}

	init(json: [String: Any?]) {
		id = json["questionID"] as? Int
		text = json["question"] as? String
	}
}

extension Question: Equatable, Hashable {
	var hashValue: Int {
		return (text ?? "").hashValue
	}

	static func ==(lhs: Question, rhs: Question) -> Bool {
		return lhs.text == rhs.text
	}
}
