//  hooru
//  Created by Brad Weber on 3/21/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

class UserAvatar {
	var id: Int?
	var email: String?
	var avatarFileName: String?

	init(json: [String: Any]) {
		id = json["id"] as? Int
		email = json["email"] as? String
		avatarFileName = json["avatarFilename"] as? String
	}
}
