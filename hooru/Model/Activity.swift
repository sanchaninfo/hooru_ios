//  hooru
//  Created by Brad Weber on 3/15/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

class Activity {
	var created: Date?
	var modified: Date?
	var prompt: Prompt?
	var videoAuthor: Participant? // video author?
	var questionAsker: Participant? // question asker?

	init (json: [String: Any?]) {
		created = Date.dateFromDatabaseDateTime(json["created"] as? String)
		modified = Date.dateFromDatabaseDateTime(json["modified"] as? String)

		let promptObject: [String: Any?] = ["id": json["promptID"] as? Int,
											"questionID": json["questionID"] as? Int,
											"question": json["question"] as? String]

		prompt = Prompt(json: promptObject)

		if let videoAuthorObject = json["author"] as? [String: Any?] {
			videoAuthor = Participant(json: videoAuthorObject)
		}

		if let questionAskerObject = json["recipient"] as? [String: Any?] {
			questionAsker = Participant(json: questionAskerObject)
		}
	}
}
