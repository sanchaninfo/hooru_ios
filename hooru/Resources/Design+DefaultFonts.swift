//  hooru
//  Created by Erwin Mazariegos on 12/19/17 using Swift 4.0.
//  Copyright © 2017 Flying Spoke Media. All rights reserved.

import UIKit

extension Design {

	/// Apply app-wide default font, color, kerning, etc; using size set in the label.
	static func setDefaultFont(label: UILabel, weight: FontWeights) {
		let fontSize = Int(label.font.pointSize)
		setDefaultFont(label: label, weight: weight, size: fontSize)
	}

	/// Apply app-wide default font, color, kerning, etc; using specified size
	static func setDefaultFont(label: UILabel, weight: FontWeights, size: Int) {
		guard let text = label.text else {
			print("\n  *****    Warning: attempting to apply a style on nonexistent UILabel text.\n           Please set the label text first.\n")
			return
		}

		let defaultAttributes = defaultFontAttibutes(weight: weight, size: size)
		label.attributedText = NSAttributedString(string: text, attributes: defaultAttributes)
	}
    
    // Apply app-wide error font, color, kerning, etc; using specifed size
    static func setErrorFont(label: UILabel, weight: FontWeights, size: Int) {
        guard let text = label.text else {
            print("\n  *****    Warning: attempting to apply error style on nonexistent UILabel text.\n           Please set the label text first.\n")
            return
        }
        
        let errorAttributes = errorFontAttributes(weight: weight, size: size)
        label.attributedText = NSAttributedString(string: text, attributes: errorAttributes)
    }
    
	/// Apply app-wide default font, color, kerning, etc; using size set in the button
	/// In some cases, storyboard font size might be different than the actual size at run time.
	/// In that situation, be sure to specify the size
	static func setDefaultFont(button: UIButton, weight: FontWeights) {
		guard let buttonLabel = button.titleLabel else {
			fatalError("Button has no title, can't apply default font")
		}

		let fontSize = Int(buttonLabel.font.pointSize)
		setDefaultFont(button: button, weight: weight, size: fontSize)
	}

	/// Apply app-wide default font, color, kerning, etc; using specified size
	static func setDefaultFont(button: UIButton, weight: FontWeights, size: Int) {
		guard let text = button.titleLabel?.text else {
			print("\n  *****    Warning: attempting to apply a style on nonexistent UIButton text.\n           Please set the button title first.\n")
			return
		}

		let defaultAttributes = defaultFontAttibutes(weight: weight, size: size)
		let attributedText = NSAttributedString(string: text, attributes: defaultAttributes)
		button.setAttributedTitle(attributedText, for: .normal)
	}

	/// Apply app-wide default font, color, kerning, etc; using size set in the textview.
	static func setDefaultFont(textView: UITextView, weight: FontWeights) {
		guard let textviewFont = textView.font else {
			print("*** Error: cannot determine textView font")
			return
		}

		let fontSize = Int(textviewFont.pointSize)
		setDefaultFont(textView: textView, weight: weight, size: fontSize)
	}

	/// Apply app-wide default font, color, kerning, etc; using the specifed size for the textfield
	static func setDefaultFont(textView: UITextView, weight: FontWeights, size: Int) {
		guard let text = textView.text else {
			print("\n  *****    Warning: attempting to apply a style on nonexistent UITextView text.\n           Please set the textview text first.\n")
			return
		}

		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.alignment = textView.textAlignment

		var defaultAttributes = defaultFontAttibutes(weight: weight, size: size)
		defaultAttributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle

		textView.attributedText = NSAttributedString(string: text, attributes: defaultAttributes)
	}

}

