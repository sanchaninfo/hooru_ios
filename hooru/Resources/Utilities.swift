//  Utilities.swift
//  Created by Erwin Mazariegos on 9/29/15.

import Foundation
import SystemConfiguration
import UIKit

extension Array {
	func randomItem() -> Element? {
		let count = self.count
		guard count > 0 else {
			return nil
		}
		let n = Int(arc4random_uniform(UInt32(count)))
		return self[n]
	}
}

func printFonts() {
	let families = UIFont.familyNames.sorted()

	for familyName:String in families {
		print("Family: \(familyName)")
		for fontName:String in UIFont.fontNames(forFamilyName: familyName) {
			print("   Name: \(fontName)")
		}
		print("\n")
	}
}

func isConnectedToNetwork() -> Bool {
	var zeroAddress = sockaddr_in()
	zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
	zeroAddress.sin_family = sa_family_t(AF_INET)
	
	guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
		$0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
			SCNetworkReachabilityCreateWithAddress(nil, $0)
		}
	}) else {
		return false
	}
	
	var flags: SCNetworkReachabilityFlags = []
	if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
		return false
	}
	
	let isReachable = flags.contains(.reachable)
	let needsConnection = flags.contains(.connectionRequired)
	
	return (isReachable && !needsConnection)
}

struct Utilities {
	static var logOutput = false
}

private func threadNumberForThread(_ thread: Thread) -> String {
	let array1 = thread.description.components(separatedBy: ">")
	if array1.count > 1 {
		let array2 = array1[1].trimmingCharacters(in: CharacterSet(charactersIn: "{}")).components(separatedBy: ",")
		for pair in array2 {
			let array3 = pair.components(separatedBy: "=")
			if array3.count > 1 {
				if array3[0].contains("number") {
					return array3[1].trimmingCharacters(in: CharacterSet.whitespaces)
				}
			}
		}
	}
	return "(unknown)"
}

func PrintErrorType(_ error: Error, file: String = #file, function: String = #function, line: Int = #line) {
	let msg = "*** \(function) ERROR [\(line)]: \(error)"
	log(msg)
}

func PrintErrorString(_ error: String, file: String = #file, function: String = #function, line: Int = #line) {
	let msg = "*** \(function)[\(line)]: \(error)"
	log(msg)
}

func Print(_ msg: String) {
	log(msg)
}

private func log(_ text: String) {
	#if DEBUG
		print(text)
	#endif
}
