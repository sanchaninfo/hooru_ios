//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.


import UIKit


struct Design {

	struct Elements {
		static let textColor = Colors.text.uiColor()
		static let accentLineColor = Colors.accent.uiColor()
		static let giantButtonBorderColor = Colors.accent.uiColor()
        static let errorColor = Colors.error.uiColor()
		static let newLabelBackground = Colors.brightHighlight.uiColor()

		static let genericAvatar = BackgroundImages.profile.uiImage()
		static let avatarBorderColor = Colors.avatarHighlight.uiColor()
		static let avatarBorderWidth: CGFloat = 2
	}

	struct Overlays {
		static let backgroundColor = Colors.obscuring.uiColor()
	}

	struct Cells {
		static var verticalCueListTextAttributes: [NSAttributedStringKey : Any] {

			var attributes = Design.defaultFontAttibutes(weight: .light, size: 32)

			let paragraphStyle = NSMutableParagraphStyle()
			paragraphStyle.alignment = .center
			paragraphStyle.lineBreakMode = .byWordWrapping

			attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
			return attributes
		}

		static func heightForVerticalQuestionText(_ text: String, inCellWithWidth cellWidth: CGFloat) -> CGFloat {
//						// if we don't adjust linespacing to calculate the neccessary text hight, we get an answer that is technically correct (correct number of linex * single line height)
			// but iOS nevertheless refuses to render the last line in certain cases, as if it didn't fit.
			var attributes = verticalCueListTextAttributes
			if let paragraphStyle = attributes[NSAttributedStringKey.paragraphStyle] as? NSMutableParagraphStyle {
				paragraphStyle.lineSpacing += 5
				attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
			}

			let string = NSAttributedString(string: text, attributes: attributes)
			let labelWidth = cellWidth - (2 * 50) // verticalQuestionListTextPadding
			let textHeight = string.boundingRect(with: CGSize(width: labelWidth, height: 1000), options: .usesLineFragmentOrigin, context: nil).size.height
			return textHeight
		}
	}

	struct LoadingView {

	}

	struct ErrorView {
		static let textColor = Colors.error.uiColor()
	}

	struct DateTimeFormats {
		static let dateTime = "MMM dd, yyyy hh:mm a"
	}

	enum FontWeights: String {
		case regular, light, thin, bold, lightItalic, italic, black
	}
    
    static func defaultFontAttibutes(weight: FontWeights, size: Int) -> [NSAttributedStringKey : Any] {
        guard let fontEnum = Fonts(rawValue: "\(weight.rawValue)_\(size)") else {
            fatalError("Specified default font is not implemented. Weight: \(weight), size: \(size)")
        }
        
        return [
            .font: fontEnum.uiFont(),
            .foregroundColor: Colors.text.uiColor(),
            .kern: -0.3
        ]
    }
    
    static func errorFontAttributes(weight: FontWeights, size: Int) -> [NSAttributedStringKey : Any] {
        guard let fontEnum = Fonts(rawValue: "\(weight.rawValue)_\(size)") else {
            fatalError("Specified default font is not implemented. Weight: \(weight), size: \(size)")
        }
        
        return [
            .font: fontEnum.uiFont(),
            .foregroundColor: Colors.error.uiColor(),
            .kern: -0.3
        ]
    }

	static func setNavbarStyle(navigationController: UINavigationController?) {
				navigationController?.navigationBar.titleTextAttributes = Design.defaultFontAttibutes(weight: .light, size: 28)
		navigationController?.navigationBar.setTitleVerticalPositionAdjustment(5, for: .default)
	}

	// MARK: - Private Constants
    private enum Colors {
		case text, error, obscuring, accent, lightObscuring, brightHighlight, avatarHighlight

		func uiColor() -> UIColor {
			switch self {
			case .text:				return UIColor(hexString: "#FFFFFF")
			case .avatarHighlight:	return UIColor(hexString: "#FFFFFF")
			case .accent:			return UIColor(hexString: "#FFFFFF").withAlphaComponent(0.5)
			case .obscuring:		return UIColor(hexString: "#373737").withAlphaComponent(0.5)
            case .error:			return UIColor(hexString: "#FED0DB")
			case .brightHighlight:	return UIColor(hexString: "#FC466B")
			case .lightObscuring: 	return UIColor(hexString: "#353535").withAlphaComponent(0.11)
			}
		}
	}

	private enum BackgroundImages {
		case logo, squares, profile

		func uiImage() -> UIImage {
			switch self {
			case .logo:		if let image = UIImage(named: "imgLogo")		{ return image }
			case .squares:	if let image = UIImage(named: "bgFrames")		{ return image }
			case .profile:	if let image = UIImage(named: "imgProfilepic")	{ return image }
			}
			return UIImage()
		}
	}

	private enum Fonts: String {
		case light_48, light_32, light_28, light_24, light_22, light_20, light_18, light_14, light_13,
            thin_36, thin_26, thin_20, thin_18,
			lightItalic_18, lightItalic_14,
			italic_15, italic_14, italic_13,
			bold_20, bold_17, bold_16, bold_14, bold_10,
			black_16,
			regular_16, regular_15, regular_14, regular_13, regular_11

		func uiFont() -> UIFont {
			switch self {
			case .light_48: 	if let font = UIFont(name: "Lato-Light", 	size: 48.0) 	{ return font }
			case .light_32: 	if let font = UIFont(name: "Lato-Light", 	size: 32.0) 	{ return font }
			case .light_28: 	if let font = UIFont(name: "Lato-Light", 	size: 28.0) 	{ return font }
			case .light_24: 	if let font = UIFont(name: "Lato-Light", 	size: 24.0) 	{ return font }
			case .light_22: 	if let font = UIFont(name: "Lato-Light", 	size: 22.0) 	{ return font }
			case .light_20: 	if let font = UIFont(name: "Lato-Light", 	size: 20.0) 	{ return font }
			case .light_18: 	if let font = UIFont(name: "Lato-Light", 	size: 18.0) 	{ return font }
			case .light_14: 	if let font = UIFont(name: "Lato-Light", 	size: 14.0) 	{ return font }
			case .light_13: 	if let font = UIFont(name: "Lato-Light", 	size: 13.0) 	{ return font }
                
            case .thin_36:     if let font = UIFont(name: "Lato-Hairline",     size: 36.0)     { return font }
            case .thin_26:     if let font = UIFont(name: "Lato-Hairline",     size: 26.0)     { return font }
            case .thin_20:     if let font = UIFont(name: "Lato-Hairline",     size: 20.0)     { return font }
            case .thin_18:     if let font = UIFont(name: "Lato-Hairline",     size: 18.0)     { return font }
                
			case .lightItalic_18: if let font = UIFont(name: "Lato-LightItalic", size: 18.0) { return font }
			case .lightItalic_14: if let font = UIFont(name: "Lato-LightItalic", size: 14.0) { return font }

			case .italic_15: 	if let font = UIFont(name: "Lato-Italic", 	size: 15.0) 	{ return font }
			case .italic_14: 	if let font = UIFont(name: "Lato-Italic", 	size: 14.0) 	{ return font }
			case .italic_13: 	if let font = UIFont(name: "Lato-Italic", 	size: 13.0) 	{ return font }
                
            case .bold_20:      if let font = UIFont(name: "Lato-Bold",     size: 20.0)     { return font }
			case .bold_17: 		if let font = UIFont(name: "Lato-Bold", 	size: 17.0) 	{ return font }
			case .bold_16: 		if let font = UIFont(name: "Lato-Bold", 	size: 16.0) 	{ return font }
            case .bold_14:      if let font = UIFont(name: "Lato-Bold",     size: 14.0)     { return font }
			case .bold_10:      if let font = UIFont(name: "Lato-Bold",     size: 10.0)     { return font }

			case .black_16:		if let font = UIFont(name: "Lato-Black", 	size: 16.0) 	{ return font }

			case .regular_16: 	if let font = UIFont(name: "Lato-Regular", 	size: 16.0) 	{ return font }
			case .regular_15: 	if let font = UIFont(name: "Lato-Regular", 	size: 15.0) 	{ return font }
			case .regular_14:	if let font = UIFont(name: "Lato-Regular", 	size: 14.0) 	{ return font }
			case .regular_13: 	if let font = UIFont(name: "Lato-Regular", 	size: 13.0) 	{ return font }
			case .regular_11: 	if let font = UIFont(name: "Lato-Regular", 	size: 11.0) 	{ return font }
			}
			return .systemFont(ofSize: 14)
		}
	}
}

