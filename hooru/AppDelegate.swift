//  cue&a
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		AccountManager.getValdiatedUser { (validatedUser) in
			DispatchQueue.main.async {
				if let user = validatedUser {
					print("Saved user token: \n\n\(user.webToken!)\n")
					self.showViewController(nil, inStoryboard: "Main")
					
				} else {
					let key = "HasLaunchedApp"
					if UserDefaults.standard.bool(forKey: key) {
						self.showViewController("WelcomeViewController", inStoryboard: "Welcome")

					} else {
						UserDefaults.standard.set(true, forKey: key)
						self.showViewController("WelcomeOnboardingViewController", inStoryboard: "Welcome")
					}
				}
			}
		}

		return true
	}

	func showViewController(_ identifier: String?, inStoryboard storyboardName: String) {
		let storyboard = UIStoryboard(name: storyboardName, bundle: nil)

		let viewController: UIViewController?
		if identifier == nil {
			viewController = storyboard.instantiateInitialViewController()
		} else {
			viewController = storyboard.instantiateViewController(withIdentifier: identifier!)
		}

		if viewController != nil {
			let window = UIWindow()
			window.rootViewController = viewController
			window.makeKeyAndVisible()
			self.window = window
		}
	}

	func showInitialControllerInStoryboard(_ filename: String) {
				DispatchQueue.main.async {

		}
	}

	func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
		guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
			let url = userActivity.webpageURL,
			let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
			return false
		}

		if components.path == "" {
			// Could add special handling based on the path
			return true
		}

		// Open the main website in a browser
		let redirectURL = URL(string: "https://www.FlyingSpokeMedia.com/")!
		application.open(redirectURL, options: [:], completionHandler: nil)

		return false
	}

//	func applicationWillResignActive(_ application: UIApplication) 	{		}
//	func applicationDidEnterBackground(_ application: UIApplication) {		}
//	func applicationWillEnterForeground(_ application: UIApplication) {		}
//	func applicationDidBecomeActive(_ application: UIApplication) 	{		}
//	func applicationWillTerminate(_ application: UIApplication) 	{		}
}
