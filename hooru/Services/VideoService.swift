//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit
import AVKit

// https://www.raywenderlich.com/94404/play-record-merge-videos-ios-swift
// https://stackoverflow.com/questions/8291727/ios-extracting-video-frames-as-images#8291760

struct VideoService {

	static func remoteURLForFilename(_ filename: String) -> URL {
		let filePath = "\(videoDirectoryPath)\(filename)"
		if filePath.hasSuffix(".mp4") {
			return IAAWSS3.urlForFilePath(filePath, inBucket: s3BucketName)
		} else {
			return IAAWSS3.urlForFilePath(filePath.appending(".mp4"), inBucket: s3BucketName)
		}
	}

	static func tempLocalURL() -> URL {
		
		let documentDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		guard documentDirectories.count > 0 else {
			print("Error obtaining document directory")
			return URL(string: "")!
		}

		let documentsDirectory = documentDirectories.first!

		let filename = "tempVideo"
		var nameSuffix = 0
		var fullFilename = "\(filename)\(nameSuffix).mp4"
		var targetURL = documentsDirectory.appendingPathComponent(fullFilename, isDirectory: false)

		while FileManager.default.fileExists(atPath: targetURL.path) {
			nameSuffix += 1
			fullFilename = "\(filename)\(nameSuffix).mp4"
			targetURL = documentsDirectory.appendingPathComponent(fullFilename, isDirectory: false)
		}

		return targetURL
	}

	static func exportVideo(_ sourceURL: URL, forPrompt prompt: Prompt, autoUpload: Bool, autoSend: Bool, completion: @escaping (URL?) -> ()) {
		
		guard !inProgressVideoExportFiles.contains(sourceURL) else {
			print("export of file \(sourceURL.lastPathComponent) already in progress")
			return
		}

		inProgressVideoExportFiles.insert(sourceURL)

		let targetURL = tempLocalURL()

		let asset = AVURLAsset(url: sourceURL, options: nil)
		guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough) else {
			print("Error creating AVAssetExportSession")
			completion(nil)
			return
		}

		exportSession.outputURL = targetURL
		exportSession.outputFileType = .mp4
		exportSession.shouldOptimizeForNetworkUse = true
		let start = CMTimeMakeWithSeconds(0.0, 0)
		let range = CMTimeRangeMake(start, asset.duration)
		exportSession.timeRange = range

		exportSession.exportAsynchronously {
			switch exportSession.status {
			case .failed:
				print("Export failed: \(exportSession.error?.localizedDescription ?? "No error specified")")

			case .cancelled:
				print("Export canceled")

			case .completed:
				// Get the MD5 hash of the video contents, use it to create a unique filename
				do {
					if let videoData = try? Data(contentsOf: targetURL) {
						let videoHash = videoData.md5Hash().lowercased()
						let uniqueFileName = "\(videoHash).mp4"
						let uniqueFileNameURL = targetURL.deletingLastPathComponent().appendingPathComponent(uniqueFileName, isDirectory: false)

						do {
							try FileManager.default.moveItem(at: targetURL, to: uniqueFileNameURL)
							prompt.responseLocalFileURL = uniqueFileNameURL
							prompt.responseRemoteURL = VideoService.remoteURLForFilename(uniqueFileNameURL.lastPathComponent)
							self.inProgressVideoExportFiles.remove(sourceURL)

							if autoUpload {
								self.uploadVideoForPrompt(prompt, autoSend: autoSend, completion: { (success) in
								})
							}

							if autoSend {
								self.updateResponseForPrompt(prompt, completion: { (success) in
								})
							}

							self.sendQueuedResponseUpdates()
							completion(uniqueFileNameURL)

						} catch {
							print("File rename failed: \(error.localizedDescription)")
							completion(nil)
						}

					} else {
						completion(nil)
					}
				}
			default:
				break
			}
		}
	}

	static func uploadVideoForPrompt(_ prompt: Prompt, autoSend: Bool, retry: Bool = true, completion: @escaping (_ success: Bool) -> ()) {
		
		guard let fileURL = prompt.responseLocalFileURL else {
			print("Error uploading video: video file has not been exported to local storage")
			return
		}

		guard !inProgressUploadUrls.contains(fileURL) else {
			print("upload of file \(fileURL.lastPathComponent) already in progress")
			return
		}

		inProgressUploadUrls.insert(fileURL)

		let aws = IAAWS(accessKey: awsAccessKey, secretKey: awsSecretKey, regionName: s3RegionName, serviceName: "s3")
		let filePath = "\(videoDirectoryPath)\(fileURL.lastPathComponent)"
		IAAWSS3.deliverFileToBucket(aws: aws, bucketName: s3BucketName, serverFilePath: filePath, fromLocalURL: fileURL) { (success) in
			if success {
				inProgressUploadUrls.remove(fileURL)
				print("video uploaded to \(prompt.responseRemoteURL?.absoluteString ?? "")")
				self.sendQueuedResponseUpdates()
				try? FileManager.default.removeItem(at: fileURL)
				#if DEBUG
//					debugFileSize(url: cue.answerRemoteUrl!)
				#endif
				completion(true)
			} else {
				if retry {
					uploadVideoForPrompt(prompt, autoSend: autoSend, retry: false, completion: completion)
				} else {
					completion(false)
				}
			}
		}
	}

	static func debugFileSize(url: URL) {
		
		var request = URLRequest(url: url)
		request.httpMethod = "HEAD"

		let task = URLSession.shared.dataTask(with: request){ (data, urlResponse, error) -> Void in
			if let response = urlResponse {
				let size = response.expectedContentLength
				let mb = Double(size) / 1000000.0

				DispatchQueue.main.async {

					let alert = UIAlertController(title: "Video Upload", message: "Uploaded \(mb) MB", preferredStyle: .alert)
					alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
						alert.dismiss(animated: true)

					}))

					if let window =	UIApplication.shared.keyWindow, let rootController = window.rootViewController as? UINavigationController {
						rootController.visibleViewController?.present(alert, animated: true)
					}
				}
			}
		}
		task.resume()
	}

	static func updateResponseForPrompt(_ prompt: Prompt, completion: @escaping (_ success: Bool) -> ()) {
		
		let queueItem = ResponseUpdateQueueItem(prompt: prompt, completion: completion)

		responseUpdateQueue.insert(queueItem)

		guard let remoteUrl = prompt.responseRemoteURL else {
			print("Can't update response for Q '\(prompt.question?.text?.prefix(15) ?? "")...' yet: No answerRemotePath set")
			return
		}

		guard !updatedPromptResponses.contains(remoteUrl) else {
			print("Already updated response for: '\(prompt.question?.text?.prefix(15) ?? "")...', not re-sending")
			responseUpdateQueue.remove(queueItem)
			completion(true)
			return
		}

		guard !inProgressResponseUpdatePrompts.contains(prompt) else {
			print("Response update pending for: '\(prompt.question?.text?.prefix(15) ?? "")...', bailing out")
			return
		}

		inProgressResponseUpdatePrompts.insert(prompt)

		if let user = AccountManager.currentLoggedInUser {
			_ = PromptAPI.updateVideoResponseForPrompt(prompt, completion: { (success, error) in
				guard error == nil else {
					print("Error updating response: \(error!.localizedDescription)")
					completion(false)
					return
				}

				responseUpdateQueue.remove(queueItem)
				inProgressResponseUpdatePrompts.remove(prompt)
				updatedPromptResponses.insert(remoteUrl)
				user.hasAnsweredAtLeastOnce = true
				completion(true)
			})
		}
	}

	static func downloadFileAt(_ url: URL?, callback: @escaping (URL?) -> ()) {
		guard url != nil else {
			callback(nil)
			return
		}

		let sessionConfig = URLSessionConfiguration.default
		let session = URLSession(configuration: sessionConfig)
		let request = URLRequest(url: url!)
		let pathExtension = url!.pathExtension
		
		let task = session.downloadTask(with: request) { (localURL, response, error) in
			if localURL != nil && error == nil {
				// Success
				// Rename the temp file to have the same extension as the download
				let targetURL = localURL!.deletingPathExtension().appendingPathExtension(pathExtension)

				do {
					try FileManager.default.moveItem(at: localURL!, to: targetURL)
					callback(targetURL)
				} catch {
					callback(nil)
				}

			} else {
				print("Video download error: \(error!.localizedDescription)")
				callback(nil)
			}
		}

		task.resume()
	}

	static func deleteLocalFileAt(_ url: URL?) {
				guard let fileURL = url else {
			return
		}
		try? FileManager.default.removeItem(at: fileURL)
	}

	// MARK: - Private stuff
	private static var inProgressUploadUrls = Set<URL>()
	private static var inProgressVideoExportFiles = Set<URL>()
	private static var inProgressResponseUpdatePrompts = Set<Prompt>()
	private static var updatedPromptResponses = Set<URL>()
	private static var responseUpdateQueue = Set<ResponseUpdateQueueItem>()

	private static let s3RegionName = "us-west-2"
	private static let s3BucketName = "ia-flyingspoke"
	private static let awsAccessKey = Bundle.main.infoDictionary!["AWS_AccessKey"] as? String ?? "empty"
	private static let awsSecretKey = Bundle.main.infoDictionary!["AWS_SecretKey"] as? String ?? "empty"

	private static let videoURL = Bundle.main.infoDictionary!["VIDEO_DirectoryPath"] as? String
	private static var videoDirectoryPath: String {
		get {
			if let path = videoURL {
				return path
			} else {
				fatalError("VIDEO_DirectoryPath is not set (in Project->Build Settings) for this build configuration")
			}
		}
	}

	private static func sendQueuedResponseUpdates() {
				responseUpdateQueue.forEach({ updateResponseForPrompt($0.prompt, completion: $0.completion) })
	}

}

private struct ResponseUpdateQueueItem: Hashable {
	let prompt: Prompt
	let completion: (_ success: Bool) -> ()

	var hashValue: Int {
		return prompt.hashValue
	}

	static func ==(lhs: ResponseUpdateQueueItem, rhs: ResponseUpdateQueueItem) -> Bool {
		return lhs.prompt == rhs.prompt
	}
}
