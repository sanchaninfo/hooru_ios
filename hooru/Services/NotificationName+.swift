//  hooru
//  Created by Brad Weber on 4/6/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import Foundation

extension Notification.Name {
	static let onLoggedInUserDidChange = Notification.Name("onLoggedInUserDidChangeNotification")
	static let onUserAvatarDidChange = Notification.Name("onUserAvatarDidChangeNotification")
}
