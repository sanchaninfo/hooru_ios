//  hooru
//  Created by Erwin Mazariegos on 12/28/17 using Swift 4.0.
//  Copyright © 2017 Flying Spoke Media. All rights reserved.

/*
import Foundation
import UIKit

struct APIService {
	static func showAlertForError(_ error: APIError, withTitle title: String, fromController controller: UIViewController) {
				var message = "Something went wrong, somewhere, somehow. It's probably not you, it's us. Maybe try again?"

		switch error {
		case .users(let underlyingError):
			switch underlyingError {
			case .badLogin(let code):
				switch code {
				case .USER_EXISTS:
					message = "This email address is already in use. Did you mean to log in instead?"
				case .WRONG_PASSWORD, .USER_NOT_FOUND:
					message = "Username or password is incorrect. Please try again"
				case .USER_NOT_VALIDATED:
					message = "This account has not been validated.\n\nA new validation email has just been sent. Please click the link in the email to verify your email address."
				}
            case .passwordReset:
                message = "There was an error resetting your password"
			default:
				print("API Error: \(error)" )
			}
		default:
			print("API Error: \(error)" )
		}

		DispatchQueue.main.async {
			let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
			controller.present(alert, animated: true, completion: nil)
		}
	}

	enum APIError: Error {
		case question(underlyingError: Questions)
		case userResponse, requestGroup, album
		case users(underlyingError: Users)
		case inbox, outbox
		case responseError
		case badPOSTData(description: String)
		case other(description: String)

		enum Questions: Error {
			case noText, other(description: String)
		}

		enum Users: Error {
			case noEmail, noPassword, noToken, badLogin(code: UserErrorCodes), validate, resendValidationCode, register, passwordReset
		}

		enum UserErrorCodes: String {
			case WRONG_PASSWORD, USER_EXISTS, USER_NOT_VALIDATED, USER_NOT_FOUND
		}
	}

	// MARK: - Users
	static func loginUser(user: Participant, completion: @escaping (_ error: APIError?) -> Void) {
				guard user.email != nil else 	{	completion(.users(underlyingError: .noEmail)); 	return	}
		guard user.password != nil else {	completion(.users(underlyingError: .noPassword)); return }

		if let responseHandler = userUpdateResponseHandler(forUser: user, completion: completion),
			let requestModifier = postRequestModifierWithData(forObject: user) {
			let webCall = sharedPOSTCall().setEndpoint(EndPoints.user(subPath: .login).path()).addRequestModifier(requestModifier).addResponseHandler(responseHandler)
			webCall.addCallFlag(.IgnoreReauthenticationHandler)
			webCall.call()
		}
	}

	static func resendVerification(user: Participant, completion: @escaping (_ error: APIError?) -> Void) {
				guard user.email != nil else 	{	completion(.users(underlyingError: .noEmail)); 	return	}

		let responseHandler = JSONObjectResponseHandler { (jsonObject, responseError) in
			completion((responseError == nil) ? nil : .users(underlyingError :.resendValidationCode))
		}
		if let requestModifier = postRequestModifierWithData(forObject: user) {
			sharedPOSTCall().setEndpoint(EndPoints.user(subPath: .resendValidationCode).path()).addRequestModifier(requestModifier).addResponseHandler(responseHandler).call()
		}
	}

	static func resetPassword(user: Participant, completion: @escaping (_ error: APIError?) -> Void) {
				guard user.email != nil else 	{	completion(.users(underlyingError: .noEmail)); 	return	}

		let responseHandler = JSONObjectResponseHandler { (jsonObject, responseError) in
			completion((responseError == nil) ? nil : .users(underlyingError :.passwordReset))
		}
		if let requestModifier = postRequestModifierWithData(forObject: user) {
			sharedPOSTCall().setEndpoint(EndPoints.user(subPath: .resetPassword).path()).addRequestModifier(requestModifier).addResponseHandler(responseHandler).call()
		}
	}

	static func updatePassword(user: Participant, completion: @escaping (_ error: APIError?) -> Void) {
				guard user.email != nil else 	{	completion(.users(underlyingError: .noEmail)); 	return	}
		guard user.password != nil else {	completion(.users(underlyingError: .noPassword)); return }

		let responseHandler = JSONObjectResponseHandler { (jsonObject, responseError) in
			completion((responseError == nil) ? nil : .users(underlyingError :.passwordReset))
		}
		if let requestModifier = postRequestModifierWithData(forObject: user) {
			sharedPOSTCall().setEndpoint(EndPoints.user(subPath: .updatePassword).path()).addRequestModifier(requestModifier).addResponseHandler(responseHandler).call()
		}
	}

	static func validateUser(user: Participant, completion: @escaping (_ error: APIError?) -> Void) {
				guard user.email != nil else 				{	completion(.users(underlyingError: .noEmail)); 	return	}
		guard user.debugVerificationCode != nil else {	completion(.users(underlyingError: .noPassword)); return }

		let responseHandler = JSONObjectResponseHandler { (jsonObject, responseError) in
			completion((responseError == nil) ? nil : .users(underlyingError :.validate))
		}
		if let requestModifier = postRequestModifierWithData(forObject: user) {
			sharedPOSTCall().setEndpoint(EndPoints.user(subPath: .login).path()).addRequestModifier(requestModifier).addResponseHandler(responseHandler).call()
		}
	}

	/// passed-in user must have a webtoken to attempt to validate.
	/// if valid, passed-in user will be updated with server data, allowing access to their properties
	/// if not valid, reauthentication mechanism will be invoked to prompt user to log in
	static func getUser(user: Participant, completion: @escaping (_ error: APIError?) -> Void) {
				guard user.webToken != nil else { completion(.users(underlyingError: .noToken)); return}

		if let responseHandler = userUpdateResponseHandler(forUser: user, completion: completion) {
			sharedGETCall(forUser: user).setEndpoint(EndPoints.user(subPath: .none).path()).addResponseHandler(responseHandler).call()
		}
	}

	static func updateUser(user: Participant, completion: @escaping (_ error: APIError?) -> Void) {
				guard user.webToken != nil else { completion(.users(underlyingError: .noToken)); return	}

		if let responseHandler = userUpdateResponseHandler(forUser: user, completion: completion),
			let requestModifier = postRequestModifierWithData(forObject: user) {
			sharedPOSTCall(forUser: user).setEndpoint(EndPoints.user(subPath: .none).path()).addRequestModifier(requestModifier).addResponseHandler(responseHandler).call()
		}
	}


	// MARK: - Activity
	static func getActivities(forUser user: Participant, completion: @escaping (_ activities: [Activity]) -> Void) {
		let responseHandler = JSONDataResponseHandler { (jsonData, responseError) in
			let emptyResponse = [Activity]()
			guard responseError == nil else {
				completion(emptyResponse)
				return
			}

			do {
				let activities = try DateDecodingDecoder.decode([Activity].self, from: jsonData)
				completion(activities)
			} catch {
				print(error.localizedDescription)
				completion(emptyResponse)
			}
		}
		sharedGETCall(forUser: user).setEndpoint(EndPoints.activity.path()).addResponseHandler(responseHandler).call()
	}


	// MARK: - Questions
	/*
	static func getQuestions(completion: @escaping (_ prompts: [Prompt]) -> Void) {
		
		let responseHandler = JSONDataResponseHandler { (jsonData, responseError) in
			let emptyResponse = [Prompt]()
			guard responseError == nil else { completion(emptyResponse); return }

			do {
				let questions = try JSONDecoder().decode(Prompts.self, from: jsonData)
				completion(questions.prompts)
			} catch {
				print(error.localizedDescription)
				completion(emptyResponse)
			}
		}

		sharedGETCall().setEndpoint(EndPoints.questions.path()).addResponseHandler(responseHandler).call()
	}

	static func getQuestion(withId id: Int, forUser user: Participant, completion: @escaping (_ prompt: Prompt?) -> Void) {
				guard user.webToken != nil else { completion(nil); return	}

		let responseHandler = JSONDataResponseHandler { (jsonData, responseError) in
			guard responseError == nil else { completion(nil); return }
			completion(parseObjectOfType(Prompt.self, fromJsonData: jsonData))
		}
		sharedGETCall(forUser: user).setEndpoint("\(EndPoints.question.path())/\(id)").addResponseHandler(responseHandler).call()
	}

	static func getAvatars(forUser user: Participant, contacts: [Participant], completion: @escaping (_ userAvatars: [Participant]) -> Void) {
				guard user.webToken != nil else { completion([Participant]()); return }

		let responseHandler = JSONDataResponseHandler { (jsonData, responseError) in
			let emptyResponse = [Participant]()
			guard responseError == nil else { completion(emptyResponse); return }

			do {
				let avatars = try JSONDecoder().decode(Avatars.self, from: jsonData)
				completion(avatars.users)
			} catch {
				print(error.localizedDescription)
				completion(emptyResponse)
			}
		}

		let emailArray = contacts.compactMap { $0.email }

		// TODO: This is a temporary hack because the API can't handle too many emails in the request yet.
		let truncatedEmailArray: Array<String>
		if emailArray.count > 50 {
			truncatedEmailArray = Array(emailArray[0...50])
		} else {
			truncatedEmailArray = emailArray
		}

		let jsonObject = ["emails": truncatedEmailArray]

		if let requestModifier = postRequestModifierWithData(forObject: jsonObject) {
			sharedPOSTCall(forUser: user).setEndpoint(EndPoints.getAvatars.path()).addRequestModifier(requestModifier).addResponseHandler(responseHandler).call()
		}
	}

	// MARK: - PromptGroups
	static func getPromptGroups(forUser user: Participant, completion: @escaping (_ promptGroups: [PromptGroup]) -> Void) {
				getPromptGroups(forUser: user, atEndpoint: EndPoints.promptGroup(subPath: .none), completion: completion)
	}

	static func sendInvitation(_ invitation: Invitation, completion: @escaping (_ error: APIError?) -> Void) {
				guard invitation.sender.webToken != nil else { completion(.users(underlyingError: .noToken)); return	}

		
		let response = JSONObjectResponseHandler { (jsonObject, responseError) in
			completion((responseError == nil) ? nil : .requestGroup)
		}

		// Merge in the non-contacts
		var mergedRecipients = invitation.recipients
		for recipientEmail in invitation.nonContactRecipients {
			let participant = Participant(first: nil, last: nil, emailString: recipientEmail)
			mergedRecipients.insert(participant)
		}

		// TODO: ugh, don't encode just to decode again, to re-encode. Insread, create the proper RequestGroup encoding
		if let recipientsData = try? JSONEncoder().encode(mergedRecipients), let recipients = try? JSONSerialization.jsonObject(with: recipientsData, options: .allowFragments) {
			let questionIds = invitation.questions.compactMap({ $0.id })
			let jsonObject: [String: Any] = ["recipients": recipients, "questionIds": questionIds]
			if let jsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) {
				let requestModifier = PostRequestModifier(body: jsonData, contentType: jsonContentType)
				print("Sending json:\n\(String(data: jsonData, encoding: .utf8)!) \n\n to \(URL(string: EndPoints.promptGroup(subPath:.none).path())!.lastPathComponent)")
				sharedPOSTCall(forUser: invitation.sender).setEndpoint(EndPoints.promptGroup(subPath:.none).path()).addRequestModifier(requestModifier).addResponseHandler(response).call()
			}
		} else {
			print("Error encoding request.recipients")
		}
	}

	static func markPromptGroupAsRead(withId id: Int, forUser user: Participant) {
				guard user.webToken != nil else { return }

		let responseHandler = JSONDataResponseHandler { (jsonData, responseError) in
			guard responseError == nil else { return }	// ignore errors
		}

		sharedGETCall(forUser: user).setEndpoint(EndPoints.promptGroup(subPath:.markAsRead(promptGroupId:id)).path()).addResponseHandler(responseHandler).call()
	}

	static func markPromptGroupAsSent(withId id: Int, forUser user: Participant, completion: @escaping (_ success: Bool) -> Void) {
				guard user.webToken != nil else { return }

		let responseHandler = JSONDataResponseHandler { (jsonData, responseError) in
			guard responseError == nil else {
				completion(false)
				return
			}
			completion(true)
		}

		sharedGETCall(forUser: user).setEndpoint(EndPoints.promptGroup(subPath:.markAsSent(promptGroupId:id)).path()).addResponseHandler(responseHandler).call()
	}

	static func getPromptGroup(withId id: Int, forUser user: Participant, completion: @escaping (_ promptGroup: PromptGroup?) -> Void) {
				guard user.webToken != nil else { completion(nil); return	}

		let responseHandler = JSONDataResponseHandler { (jsonData, responseError) in
			guard responseError == nil else { completion(nil); return }
			completion(parseObjectOfType(PromptGroup.self, fromJsonData: jsonData))
		}

		sharedGETCall(forUser: user).setEndpoint(EndPoints.promptGroup(subPath:.single(promptGroupId:id)).path()).addResponseHandler(responseHandler).call()
	}

	static func getInbox(forUser user: Participant, completion: @escaping (_ promptGroups: [PromptGroup]) -> Void) {
		
		getPromptGroups(forUser: user, atEndpoint: EndPoints.inbox, completion: completion)
	}

	static func getOutbox(forUser user: Participant, completion: @escaping (_ promptGroups: [PromptGroup]) -> Void) {
		
		getPromptGroups(forUser: user, atEndpoint: EndPoints.outbox, completion: completion)
	}

	static func updateResponseForPrompt(_ prompt: Prompt, byUser user: Participant, completion: @escaping (_ error: APIError?) -> Void) {
				guard user.webToken != nil else { completion(.users(underlyingError: .noToken)); return	}
		guard let promptId = prompt.promptOrder else { completion(.badPOSTData(description: "missing id for Q within request")); return	}
		guard prompt.answerRemoteUrl != nil else { completion(.badPOSTData(description: "missing video file path for Q")); return	}

		let responseHandler = JSONObjectResponseHandler { (jsonObject, responseError) in
			completion((responseError == nil) ? nil : .requestGroup)
		}

		if let requestModifier = postRequestModifierWithData(forObject: prompt) {
			sharedPOSTCall(forUser: user).setEndpoint(EndPoints.promptGroup(subPath:.update(promptGroupId: promptId)).path()).addRequestModifier(requestModifier).addResponseHandler(responseHandler).call()
		}
	}
	*/

	//
	// MARK: - Private stuff
	private static let serverURL = Bundle.main.infoDictionary!["API_BASE_URL"] as? String
	private static var serverPath: String {
		get {
			if let path = serverURL {
				return path
			} else {
				fatalError("API_BASE_URL is not set (in Project->Build Settings) for this build configuration")
			}
		}
	}
	private static let apiKey = Bundle.main.infoDictionary!["API_KEY"] as? String
	private static var serverApiKey: String {
		get {
			if let key = apiKey {
				return key
			} else {
				fatalError("API_KEY is not set (in Project->Build Settings) for this build configuration")
			}
		}
	}

	private static let shared = WebService().setServerPath(serverPath).setReauthenticationHandler(LogInViewController())

	private static func sharedGETCall(forUser user: Participant? = nil) -> WebCall {
		let call = WebCall(service: shared).addRequestModifier(GetRequestModifier().addHeader(key: iaAppTokenHeader.key, value: iaAppTokenHeader.value))
		if let user = user, let token = user.webToken {
			call.addRequestModifier(GetRequestModifier().addHeader(key: "authorization", value: token))
		}
		return call
	}

	private static func sharedPOSTCall(forUser user: Participant? = nil) -> WebCall {
		let call = WebCall(service: shared).addRequestModifier(PostRequestModifier().addHeader(key: iaAppTokenHeader.key, value: iaAppTokenHeader.value))
		if let user = user, let token = user.webToken {
			call.addRequestModifier(PostRequestModifier().addHeader(key: "authorization", value: token))
		}
		return call
	}

	private static let jsonContentType = "application/json"
	private static let iaAppTokenHeader = (key: "ia-auth-token-app", value: serverApiKey)

	private enum EndPoints {
		case questions, question
		case user(subPath: Users)
		case activity
		case addAlbum
		case getAlbums
		case getAvatars
		case promptGroup(subPath: PromptGroups)
		case inbox, outbox

		enum Users: String {
			case none, register, login, resetPassword, updatePassword, resendValidationCode, validate
		}

		enum PromptGroups {
			case none
			case single(promptGroupId: Int)
			case update(promptGroupId: Int)
			case markAsRead(promptGroupId: Int)
			case markAsSent(promptGroupId: Int)
		}

		func path() -> String {
			switch self {
			case .user(let subPath):
				switch subPath {
				case .none:
					return "user"
				default:
					return "user/\(subPath)"
				}
			case .promptGroup(let subPath):
				switch subPath {
				case .none:
					return "requestGroup"
				case .single(let promptGroupId):
					return "requestGroup/\(promptGroupId)"
				case .update(let promptGroupId):
					return "requestGroup/update/\(promptGroupId)"
				case .markAsRead(let promptGroupId):
					return "requestGroup/read/\(promptGroupId)"
				case .markAsSent(let promptGroupId):
					return "requestGroup/sent/\(promptGroupId)"
				}
			case .questions, .question, .inbox, .outbox, .getAvatars:
				return "\(self)"
			case .activity:
				return "getActivity"
			case .getAlbums:
				return "getLibraries"
			case .addAlbum:
				return "addLibrary"
			}
		}
	}

	private static func postRequestModifierWithData<T: Encodable>(forObject object: T) -> PostRequestModifier? {
		do {
			let data = try JSONEncoder().encode(object)
			print("Sending json:\n\(String(data: data, encoding: .utf8)!)")
			return PostRequestModifier(body: data, contentType: jsonContentType)
		} catch {
			fatalError("*** Error encoding \(object): \(error)")
		}
	}

	private static func userUpdateResponseHandler(forUser user: Participant, completion: @escaping (_ error: APIError?) -> Void) -> JSONDataResponseHandler? {
				let responseHandler = JSONDataResponseHandler { (jsonData, responseError) in
			if let error = responseError {
				if let responseObject = try? JSONSerialization.jsonObject(with: jsonData, options: []),
					let responseDict = responseObject as? Dictionary<String, Any>,
					let code = responseDict["error"] as? String {
					if let errorCode = APIError.UserErrorCodes(rawValue: code) {
						completion(.users(underlyingError :.badLogin(code: errorCode)))
					} else {
						completion(.other(description: code))
					}
				} else {
					completion(.other(description: error.localizedDescription))
				}
				return
			}
			do {
				let returnedUser = try JSONDecoder().decode(Participant.self, from: jsonData)
				user.updateFrom(returnedUser)
				completion(nil)
			} catch {
				completion(.responseError)
			}
		}
		return responseHandler
	}

	/*
	static private func getPromptGroups(forUser user: Participant, atEndpoint endpoint: EndPoints, completion: @escaping (_ promptGroups: [PromptGroup]) -> Void) {
		guard user.webToken != nil else { completion([PromptGroup]()); return	}

		let responseHandler = JSONDataResponseHandler { (jsonData, responseError) in
			let emptyResponse = [PromptGroup]()
			guard responseError == nil else { completion(emptyResponse); return }

			do {
				let promptGroups = try DateDecodingDecoder.decode(PromptGroups.self, from: jsonData)
				completion(promptGroups.promptGroups)
			} catch {
				print(error.localizedDescription)
				completion(emptyResponse)
			}
		}

		sharedGETCall(forUser: user).setEndpoint(endpoint.path()).addResponseHandler(responseHandler).call()
	}
	*/

	private static func parseObjectOfType<T: Decodable>(_ type: T.Type, fromJsonData data: Data) -> T? {
		do {
			return try JSONDecoder().decode(type, from: data)
		} catch  {
			return nil
		}
	}

	/// use whenever the API will return dates
	private static var DateDecodingDecoder: JSONDecoder {
		let jsonDecoder = JSONDecoder()
		jsonDecoder.dateDecodingStrategy = .custom({ (decoder) -> Date in
			let container = try decoder.singleValueContainer()
			let dateString = try container.decode(String.self)

			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"		//	 "created": "2018-01-29 22:58:38"
			dateFormatter.timeZone = TimeZone(identifier: "UTC")
			if let date = dateFormatter.date(from: dateString) {
//				let localDate = Date(timeInterval: TimeInterval(TimeZone.current.secondsFromGMT()), since: date)
//				let x = Calendar.current.date(bySetting: Calendar.Component.timeZone, value: 6, of: localDate)
				return date
			}
			return Date()
		})
		return jsonDecoder
	}

}
*/




/*
Implemented:

statusCode: 400,errorCode: 'WRONG_PASSWORD',
reason: 'The provided password does not match the user\'s password'

statusCode: 400,errorCode: 'USER_EXISTS',
reason: 'Email address already exists in user table'

statusCode: 401,errorCode: 'USER_NOT_VALIDATED',
reason: 'User has not yet validated their account'

statusCode: 404,errorCode: 'USER_NOT_FOUND',
reason: 'The user was not found'


Not yet implemented:

(to implement, just add a case to enum UserErrorCodes and create new user-facing message)

statusCode:'400',errorCode: 'PASSWORD_VALIDATION',
reason: 'Password did not match validation rules'

statusCode: 400,errorCode: 'MOBILE_PHONE_VALIDATION',
reason: 'Mobile phone did not validate'

statusCode: 400,errorCode: 'EMAIL_ADDRESS_VALIDATION',
reason: 'Email address did not validate'

statusCode: 400,errorCode: 'NAME_VALIDATION',
reason: 'Name did not validate to a string'

statusCode: 404,errorCode: 'USER_NOT_FOUND_OR_INVALID_CODE',
reason: 'User was not found or code is invalid'


(to implement, create a new enum following pattern of UserErrorCodes)

statusCode: 400,errorCode: 'REQUEST_GROUP_ID_VALIDATION',
reason: 'The request must be sent with a single integer representing the requestGroupID'

statusCode: 401,errorCode: 'INVALID_API_KEY',
reason: 'Provided API Key is not valid'

statusCode: 404,errorCode: 'QUESTION_NOT_FOUND',
reason: 'The question was not found'

statusCode: 404,errorCode: 'RECIPIENTS_MISSING',
reason: 'The request must be sent with recipient emails'

statusCode: 404,errorCode: 'QUESTION_IDS_MISSING',
reason: 'The request must be sent with an array of question IDs'

statusCode: 404,errorCode: 'RESPONSES_MISSING',
reason: 'The request must be sent with an array of responses'

statusCode: 404,errorCode: 'REQUEST_GROUP_ID_MISSING',
reason: 'The request must be sent with a single requestGroupID'

statusCode: 500,errorCode: 'UNKNOWN_ERROR',
reason: 'An unknown error occurred'
*/
