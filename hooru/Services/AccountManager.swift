//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit


struct AccountManager {
	static var currentLoggedInUser: Participant? = nil {
		didSet {
			if let user = currentLoggedInUser {
				saveUser(user)
			}

			if (oldValue == nil && currentLoggedInUser != nil) ||
				(oldValue != nil && currentLoggedInUser == nil) ||
				(oldValue?.id != currentLoggedInUser?.id) {
				NotificationCenter.default.post(name: .onLoggedInUserDidChange, object: nil, userInfo: nil)
			}
		}
	}

	/*
	static func logIn(user: Participant, completion: @escaping (_ error: APIService.APIError?) -> Void) {
		
		APIService.loginUser(user: user) { (responseError) in
			if let token = user.webToken {
				AccountManager.currentLoggedInUser = user
				print("user token: \(token)")
			}
			completion(responseError)
		}
	}
	*/

	static func logOut() {
				UserDefaults.standard.removeObject(forKey: WEB_TOKEN_KEY)
//		UserDefaults.standard.removeObject(forKey: USER_EMAIL_KEY)
		AccountManager.currentLoggedInUser = nil
	}

	static func getValdiatedUser(completion: @escaping (_ user: Participant?) -> Void) {
		
		guard let token = UserDefaults.standard.string(forKey: WEB_TOKEN_KEY) else {
			completion(nil)
			return
		}

		let tempUser = Participant()
		tempUser.webToken = token

		AccountManager.currentLoggedInUser = tempUser

		_ = UserAPI.getUser(completion: { (error) in
			guard error == nil else {
				AccountManager.currentLoggedInUser = nil
				completion(nil)
				return
			}

			guard (AccountManager.currentLoggedInUser?.isVerified ?? false) == true else {
				AccountManager.currentLoggedInUser = nil
				completion(nil)
				return
			}

			completion(AccountManager.currentLoggedInUser)
		})
	}

	static func unvalidatedUser() -> Participant? {
		
		if let email = UserDefaults.standard.string(forKey: USER_EMAIL_KEY) {
			let tempUser = Participant()
			tempUser.email = email
			return tempUser
		}
		return nil
	}

	static private let WEB_TOKEN_KEY = "appUserWebToken"
	static private let USER_EMAIL_KEY = "appUaerEmail"

	private static func saveUser(_ user: Participant) {
		
		if let token = user.webToken {
			UserDefaults.standard.set(token, forKey: WEB_TOKEN_KEY)
		}

		if let email = user.email {
			UserDefaults.standard.set(email, forKey: USER_EMAIL_KEY)
		}
	}

	/*
	static func registerTempUser(completion: @escaping (_ error: APIService.APIError?) -> Void) {
		
		let newUUID = UUID().uuidString

		let firstName = "Temp"
		let lastName = "User"
		let password = String(newUUID.suffix(10))
		let email = "delete_me_\(String(newUUID.suffix(6)))"

		let user = Participant(first: firstName, last: lastName, emailString: "\(email)@inspiringapps.com")
		user.password = password

		/*
		APIService.registerUser(user: user) { (responseError) in
			if let error = responseError {
				completion(error)
				return
			}

			APIService.validateUser(user: user, completion: { (responseError) in
				if let error = responseError {
					completion(error)
					return
				}
				logIn(user: user, completion: { (responseError) in
					saveUser(user)
					completion(responseError)
				})
			})
		}
		*/
	}
	*/
    
    static func emailIsValid(_ emailString: String) -> Bool {
                let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,63}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailString)
    }
    
    static func passwordIsLongEnough(_ password: String) -> Bool {
                return password.count >= 6
    }
    
    static func passwordsAreValid(passwordText: String?, confirmPasswordText: String?) -> Bool {
        
        // Checks to make sure passwords are not nil
        guard let password = passwordText, let confirmPassword = confirmPasswordText else {
            return false
        }
        
        // Check password length
        if !passwordIsLongEnough(password.sanitized()) {
            return false
        }
        
        // Check to make sure there are no white spaces in password
        if password != password.sanitized() || confirmPassword != confirmPassword.sanitized() {
            return false
        }
        
        // Check to make sure they are the same
        if password.sanitized() == confirmPassword.sanitized() {
            return true
        }
        
        return false
    }
}

