//  hooru
//  Created by Erwin Mazariegos on 12/14/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class GiantButton: UIButton {

	override func willMove(toWindow newWindow: UIWindow?) {
		super.willMove(toWindow: newWindow)

		if let text = title(for: .normal) {
			let titleParts = text.components(separatedBy: .newlines)
			let string = NSMutableAttributedString(string: titleParts[0], attributes: Design.defaultFontAttibutes(weight: .light, size: 20))
			if titleParts.count > 1 {
				string.append(NSMutableAttributedString(string: "\n\(titleParts[1])", attributes: Design.defaultFontAttibutes(weight: .lightItalic, size: 14)))
			}
			self.setAttributedTitle(string, for: .normal)
		}
	}

	override func draw(_ rect: CGRect) {
		backgroundColor = .clear
		titleLabel?.numberOfLines = 0
		titleLabel?.textAlignment = .center

		layer.borderColor = Design.Elements.giantButtonBorderColor.cgColor
		layer.borderWidth = 1

		tintColor = Design.Elements.textColor
	}

}
