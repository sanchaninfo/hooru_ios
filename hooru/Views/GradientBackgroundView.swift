//  hooru
//  Created by Brad Weber on 3/12/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

@IBDesignable
class GradientBackgroundView: UIView {
	@IBInspectable var isDark: Bool = false {
		didSet {
			guard oldValue != isDark else {
				// No change
				return
			}

			updateLayers()
		}
	}

	private var backgroundLayer: CALayer?
	private var gradientLayer: CAGradientLayer?


	override func willMove(toSuperview newSuperview: UIView?) {
		super.willMove(toSuperview: newSuperview)

		updateLayers()
	}

	override func layoutSubviews() {
		backgroundLayer?.frame = bounds
		gradientLayer?.frame = bounds
	}


	private func updateLayers() {
		if backgroundLayer == nil {
			backgroundLayer = CALayer()
			backgroundLayer!.backgroundColor = UIColor.black.cgColor
			backgroundLayer!.frame = bounds

			layer.addSublayer(backgroundLayer!)
		}

		if gradientLayer == nil {
			let start = UIColor(hexString: "#FC466B")
			let end = UIColor(hexString: "#3F5EFB")

			gradientLayer = CAGradientLayer()
			gradientLayer!.colors = [start.cgColor, end.cgColor]
			gradientLayer!.frame = bounds

			layer.addSublayer(gradientLayer!)
		}

		gradientLayer?.opacity = isDark ? 0.4 : 1.0
	}
}
