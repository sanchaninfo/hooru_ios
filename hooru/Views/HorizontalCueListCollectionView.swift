//  hooru
//  Created by Erwin Mazariegos on 1/30/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

protocol HorizontalPromptListCollectionDelegate {
	func didScrollToItemIndex(_ index: Int)
}

class HorizontalPromptListCollectionView: UICollectionView, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
	var sectionDelegate: HorizontalPromptListCollectionDelegate?
	var allowBacktrack = true
	var requestor: Participant?

	private var cellWidth: CGFloat {
		return self.frame.size.width - 83
	}

	private var currentScrollOffset: CGFloat = 0
	private var currentTargetIndex = -1		// an index of -1 denotes the header view
	private var itemCount: Int {
		return numberOfItems(inSection: 0)
	}
	private var notifyDelegate = true

	private let cellSpacing: CGFloat = 20
	private let sectionInset: CGFloat = 44
	private let indexViewSize: CGFloat = 32

	private var indexViews = [UIView]()
	private var dashedLines = [CAShapeLayer]()


	override func didMoveToSuperview() {
				delegate = self
	}

	override func willMove(toWindow newWindow: UIWindow?) {
				super.willMove(toWindow: newWindow)

		if newWindow != nil {
			clipsToBounds = false
			indexViews.forEach({ $0.removeFromSuperview() })
			indexViews.removeAll()
			(1 ... itemCount).forEach({ addIndexLabel($0) })
			addIndexAvatar()
		}
	}

	override func layoutSubviews() {
		super.layoutSubviews()

		let width = cellWidth

		for (i, label) in indexViews.enumerated() {
			let cellLeft = CGFloat(i + 1) * width + cellSpacing
			label.frame.origin.x = cellLeft + width / 2 + cellSpacing * CGFloat(i)
		}

		if dashedLines.count != indexViews.count - 1 {
			dashedLines.forEach({ $0.removeFromSuperlayer() })
			dashedLines.removeAll()

			for (i, view) in indexViews.enumerated() {
				if i < indexViews.count - 1 {
					addDashedLineFromView(view, withLength: width + cellSpacing - indexViewSize)
				}
			}
		}
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		return CGSize(width: cellWidth, height: frame.size.height)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
		return CGSize(width: cellWidth, height: frame.size.height)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: cellWidth, height: frame.size.height)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		// affects cells only
		return UIEdgeInsets(top: 0, left: sectionInset, bottom: 0, right: sectionInset)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return cellSpacing
	}


	func scrollToPromptAtIndex(_ index: Int) {
		
		let idx = max(0, index)

		guard (0 ..< itemCount).contains(idx) else {
			return
		}
		
		DispatchQueue.main.async {
			self.scrollToItem(at: IndexPath(item: idx, section: 0), at: .centeredHorizontally, animated: true)
		}
	}

	func scrollToFooter() {
		
		let x = cellWidth * CGFloat(itemCount + 1) + cellSpacing * CGFloat(itemCount)
		let scrollingPosition = CGPoint(x: x, y: 0)

		DispatchQueue.main.async {
			self.setContentOffset(scrollingPosition, animated: true)
		}
	}

	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//		print("current offset: \(scrollView.contentOffset.x), old currentIndex: \(currentTargetIndex), new CurrentIndex: \(Int(floor((currentScrollOffset - cellWidth) / (cellWidth + cellSpacing))))")
		currentScrollOffset = scrollView.contentOffset.x

		currentTargetIndex = Int(floor((currentScrollOffset - cellWidth) / (cellWidth + cellSpacing)))
		notifyDelegate = true
	}

	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//		print("intended target: \(targetContentOffset.pointee.x)")

		let targetOffset = targetContentOffset.pointee.x

		guard abs(targetOffset - currentScrollOffset) > 50 else {
			targetContentOffset.pointee.x = currentScrollOffset
			notifyDelegate = false
			return
		}

		let isGoingForward = targetOffset > currentScrollOffset

		if !isGoingForward && !allowBacktrack {
			targetContentOffset.pointee.x = currentScrollOffset
			notifyDelegate = false
			return
		}

		if isGoingForward {
			currentTargetIndex = currentTargetIndex + 1
		} else {
			currentTargetIndex = currentTargetIndex - 1
		}

		targetContentOffset.pointee.x = cellWidth * CGFloat(currentTargetIndex + 1) + cellSpacing * CGFloat(currentTargetIndex)
		// targetContentOffset.pointee.x = cellWidth * CGFloat(currentTargetIndex) + cellSpacing * CGFloat(currentTargetIndex)

//		print("adjusted target: \(targetContentOffset.pointee.x)")
	}

	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
				if notifyDelegate {
			sectionDelegate?.didScrollToItemIndex(currentTargetIndex)
		}
	}

	// MARK: - Private stuff
	private func addIndexLabel(_ index: Int) {
		
		let label = UILabel(frame: CGRect(x: 0, y: frame.size.height + 18, width: indexViewSize, height: indexViewSize))
		label.textColor = Design.Elements.textColor
		label.textAlignment = .center
		label.text = "\(itemCount - index + 1)"
		Design.setDefaultFont(label: label, weight: .bold, size: 17)

		label.layer.cornerRadius = indexViewSize / 2
		label.layer.borderColor = Design.Elements.avatarBorderColor.cgColor
		label.layer.borderWidth = 2

		addSubview(label)
		indexViews.append(label)
	}

	private func addIndexAvatar() {
		
		guard let user = requestor else {
			return
		}

		let avatar = CAAvatarImageView(frame: CGRect(x: 0, y: frame.size.height + 18, width: indexViewSize, height: indexViewSize))
		avatar.user = user
		avatar.borderMode = .full

		addSubview(avatar)
		indexViews.append(avatar)
	}

	private func addDashedLineFromView(_ view: UIView, withLength length: CGFloat) {
		
		let shape = CAShapeLayer()
		let shapeFrame = CGRect(x: view.frame.maxX, y: view.frame.midY, width: length, height: 1)
		let shapePath = CGPath(rect: CGRect(origin: .zero, size: shapeFrame.size), transform: nil)
		shape.path = shapePath

		shape.strokeColor = Design.Elements.accentLineColor.cgColor
		shape.lineWidth = 1
		shape.fillColor = UIColor.clear.cgColor
		shape.lineDashPattern = [5,3]
		shape.strokeEnd = 0.5

		shape.frame = shapeFrame

		layer.addSublayer(shape)
		dashedLines.append(shape)
	}
}
