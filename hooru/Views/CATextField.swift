//  hooru
//  Created by Vu Dang on 12/27/17 using Swift 4.0.
//  Copyright © 2017 Flying Spoke Media. All rights reserved.

import UIKit

class CATextField: UITextField {

	/// By default, setting this also sets the placeholder attributes to match. That can be overridden afterwards if needed
	var entryTextAttributes = Design.defaultFontAttibutes(weight: .light, size: 18) {
		didSet {
			setDefaultAtributes()
			placeholderTextAttributes = entryTextAttributes
			setPlaceholderAttributedText()
		}
	}

	/// By default, this matches any entry attribtes set. Only set (after setting .entryTextAttributes) if placeholder should be different
	var placeholderTextAttributes = Design.defaultFontAttibutes(weight: .light, size: 18) {
		didSet {
			setPlaceholderAttributedText()
		}
	}

    override func awakeFromNib() {
		        super.awakeFromNib()
		setup()
    }

	override init(frame: CGRect) {
				super.init(frame: frame)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
				super.init(coder: aDecoder)
		setup()
	}

	override var placeholder: String? {
		didSet {
			setPlaceholderAttributedText()
		}
	}

    func showError(message: String) {
        errorMessageLabel.text = message
        Design.setErrorFont(label: errorMessageLabel, weight: .regular, size: 13)
        errorImage.isHidden = false
        errorMessageLabel.isHidden = false
    }
    
    func hideError() {
        errorImage.isHidden = true
        errorMessageLabel.isHidden = true
    }

	func sanitizedText() -> String {
				if let text = self.text {
			return text.trimmingCharacters(in: .whitespaces)
		}
		return ""
	}

	// Private! don't look
	private let errorImage = UIImageView()
	private let errorMessageLabel = UILabel()

	private func setup() {
				addErrorImage()
		addErrorLabel()
		hideError()
		setDefaultAtributes()
		setPlaceholderAttributedText()
	}

	private func setPlaceholderAttributedText() {
		if let placeholderText = placeholder {
			attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: placeholderTextAttributes)
		}
	}

	private func setDefaultAtributes() {
		
		var attributes = Dictionary<String, Any>()

		for key in entryTextAttributes.keys {
			attributes[key.rawValue] = entryTextAttributes[key]
		}

		defaultTextAttributes = attributes
		textAlignment = .center
	}

	private func addErrorImage() {
		let imageName = "imgError"
		if let image = UIImage(named: imageName) {
			errorImage.image = image
		} else {
			print("*** ERROR: Unable to find the error icon named \(imageName)")
		}

		errorImage.translatesAutoresizingMaskIntoConstraints = false
		addSubview(errorImage)

		NSLayoutConstraint.activate([
			NSLayoutConstraint(item: errorImage, attribute: .left, 		relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 20.0),
			NSLayoutConstraint(item: errorImage, attribute: .centerY, 	relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0),
			NSLayoutConstraint(item: errorImage, attribute: .width, 	relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 15.0),
			NSLayoutConstraint(item: errorImage, attribute: .height, 	relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 14.0),
			])
	}

	private func addErrorLabel() {
		errorMessageLabel.numberOfLines = 0
		errorMessageLabel.translatesAutoresizingMaskIntoConstraints = false
		errorMessageLabel.textAlignment = .center

		addSubview(errorMessageLabel)
		NSLayoutConstraint.activate([
			NSLayoutConstraint(item: errorMessageLabel, attribute: .left, 	relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0),
			NSLayoutConstraint(item: errorMessageLabel, attribute: .right, 	relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0),
			NSLayoutConstraint(item: errorMessageLabel, attribute: .top, 	relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0),
			NSLayoutConstraint(item: errorMessageLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 20.0)
			])
	}
}
