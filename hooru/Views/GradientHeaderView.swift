//  hooru
//  Created by Brad Weber on 3/13/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

@IBDesignable
class GradientHeaderView: UIView {
	private var gradientLayer: CAGradientLayer?

	override func willMove(toSuperview newSuperview: UIView?) {
		super.willMove(toSuperview: newSuperview)

		updateLayers()
	}

	override func layoutSubviews() {
		gradientLayer?.frame = bounds
	}


	private func updateLayers() {
		if gradientLayer == nil {
			let start = UIColor(hexString: "#FC466B")
			let end = UIColor(hexString: "#3F5EFB")

			gradientLayer = CAGradientLayer()
			gradientLayer!.colors = [start.cgColor, end.cgColor]
			gradientLayer!.startPoint = CGPoint(x: 0, y: 0.5)
			gradientLayer!.endPoint = CGPoint(x: 1, y: 0.5)
			gradientLayer!.frame = bounds
			gradientLayer!.opacity = 1.0

			layer.addSublayer(gradientLayer!)
		}
	}
}
