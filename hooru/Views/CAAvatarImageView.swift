//  hooru
//  Created by Erwin Mazariegos on 1/22/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class CAAvatarImageView: UIImageView {
	enum BorderMode {
		case none, full, partial(percentage: Float)
	}

	var user: Participant? {
		didSet {
			setAvatarImage()
		}
	}

	var borderMode: BorderMode = .none {
		didSet {
			switch borderMode {
			case .none:
				layer.borderWidth = 0
				completionLayer.lineWidth = 0
			case .full:
				layer.borderWidth = Design.Elements.avatarBorderWidth
				completionLayer.lineWidth = 0
			case .partial(let percentage):
				setCompletionLayer()
				layer.borderWidth = 0
				completionLayer.lineWidth = Design.Elements.avatarBorderWidth
				setCompletion(CGFloat(percentage))
			}
		}
	}

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}

	override func layoutSubviews() {
		super.layoutSubviews()

		guard case .partial = borderMode else {
			return
		}

		completionLayer.position.x = center.x
		completionLayer.position.y = center.y
		completionLayer.setAffineTransform(CGAffineTransform.init(rotationAngle: -.pi/2))
	}

	deinit {
			}

	// MARK: - Private
	private var imageToken: NSKeyValueObservation?
	private var imagePathToken: NSKeyValueObservation?
	private let completionLayer = CAShapeLayer()
	private var completionLayerPathCircumference: CGFloat = 0

	private func setup() {
		let size = self.frame.size.height
		layer.cornerRadius = size / 2
		layer.borderColor = Design.Elements.avatarBorderColor.cgColor
		clipsToBounds = true
	}

	private func setCompletionLayer() {
		guard completionLayer.path == nil else {
			return		// already set up
		}

		guard abs(bounds.height - bounds.width) < 0.01 else {
			return		// we only support square avatar bounds
		}

		let circleFrame = bounds.insetBy(dx: -4, dy: -4)
		let circlePath = CGPath(ellipseIn: circleFrame, transform: nil)
		completionLayer.path = circlePath
		completionLayerPathCircumference = circleFrame.size.height * .pi

		completionLayer.strokeColor = Design.Elements.avatarBorderColor.cgColor
		completionLayer.fillColor = UIColor.clear.cgColor

		completionLayer.frame = frame
		superview?.layer.addSublayer(completionLayer)
	}

	private func setCompletion(_ percentage: CGFloat) {
		let segmentLength: CGFloat = 4
		let segments = completionLayerPathCircumference / segmentLength

		let firstSegmentMultiple = segments * percentage
		let remainingSegments = Int(segments - firstSegmentMultiple)

		// add first segment (super long segment that looks like solid line)
		var pattern = [completionLayerPathCircumference * percentage as NSNumber]

		// add remaining regular sized segments to make the dotted pattern
		pattern.append(contentsOf: Array(repeating: segmentLength as NSNumber, count: remainingSegments))

		completionLayer.lineDashPattern = pattern
	}

	private func setAvatarImage() {
		self.user?.avatarImage(callback: { (image) in
			DispatchQueue.main.async {
				self.image = image
			}
		})
	}
}
