//  hooru
//  Created by Erwin Mazariegos on 3/6/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit
import AVKit


protocol CuePlaybackViewDelegate {
	func playbackView(_ playbackView: CuePlaybackView, didChangePlaybackTime playbackTime: TimeInterval)
}

class CuePlaybackView: UIView {
	var playbackDelegate: CuePlaybackViewDelegate?
	private var playingTimer: Timer?
	private var playbackTime: TimeInterval = 0 {
		didSet {
			if oldValue != playbackTime {
				playbackDelegate?.playbackView(self, didChangePlaybackTime: playbackTime)
			}
		}
	}

	required init?(coder aDecoder: NSCoder) {
				super.init(coder: aDecoder)
	}

	override func willMove(toWindow newWindow: UIWindow?) {
				if newWindow == nil {
			teardown()
		}
	}

	override func willMove(toSuperview newSuperview: UIView?) {
		
		if newSuperview != nil {
			setup()
		} else {
			teardown()
		}
	}

	override func layoutSubviews() {
		super.layoutSubviews()
		playerLayer.frame = bounds
	}

	deinit {
				teardown()
	}

	private func updatePlaybackTime() {
		playbackTime = CMTimeGetSeconds(videoPlayer.currentTime())
	}

	func playVideoAtUrl(_ url: URL) {
		print("url: \(url)")

		if url.pathExtension.isEmpty {
			videoPlayer = AVPlayer(url: url.appendingPathExtension("mp4"))
		} else {
			videoPlayer = AVPlayer(url: url)
		}

		playingTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
			self.updatePlaybackTime()
		})

		playerLayer.player = videoPlayer
		videoPlayer.play()
	}

	func pausePlayback() {
				videoPlayer.pause()

		playingTimer?.invalidate()
		updatePlaybackTime()
	}

	func resumePlayback() {
		guard videoPlayer.rate <= 0 else {
			return
		}

		playingTimer?.invalidate()
		playingTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
			self.updatePlaybackTime()
		})

		videoPlayer.play()
	}


	// MARK: - Private
	private let playerLayer = AVPlayerLayer()
	private var videoPlayer = AVPlayer()

	private func setup() {
		
		playerLayer.videoGravity = .resizeAspectFill
		playerLayer.frame = bounds
		layer.addSublayer(playerLayer)
	}

	private func teardown() {
				videoPlayer.pause()
		playerLayer.player = nil
		videoPlayer = AVPlayer()

		playingTimer?.invalidate()
		playingTimer = nil
	}
}
