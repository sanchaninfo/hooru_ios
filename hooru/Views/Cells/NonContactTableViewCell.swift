//  hooru
//  Created by Brad Weber on 3/16/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class NonContactTableViewCell: UITableViewCell {
	@IBOutlet weak var selectedImageView: UIImageView!
	@IBOutlet weak var emailAddressLabel: UILabel!

	var emailAddress: String? {
		didSet {
			updateDisplay()
		}
	}

	var isNonContactSelected: Bool = false {
		didSet {
			updateDisplay()
		}
	}


    override func awakeFromNib() {
        super.awakeFromNib()

		updateDisplay()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

	private func updateDisplay() {
		guard emailAddressLabel != nil else {
			return
		}

		emailAddressLabel.text = emailAddress

		selectedImageView.image = isNonContactSelected ? UIImage(named: "imgRowSelectSelected") : UIImage(named: "imgRowSelectUnselected")
	}
}
