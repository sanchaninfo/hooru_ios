//  hooru
//  Created by Erwin Mazariegos on 12/1/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class QuestionListCell: UICollectionViewCell {
	@IBOutlet weak var questionLabel: UILabel!

	var question: Question? {
		didSet {
			updateDisplay()
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()

		updateDisplay()
	}

	private func updateDisplay() {
		guard questionLabel != nil else {
			return
		}

		questionLabel.text = question?.text
	}
}
