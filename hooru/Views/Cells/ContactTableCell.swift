//  hooru
//  Created by Erwin Mazariegos on 12/6/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class ContactTableCell: UITableViewCell {

	static var identifier: String {
		return "\(self)"
	}

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!
	@IBOutlet weak var selectedImageView: UIImageView!
	@IBOutlet weak var avatarImageView: CAAvatarImageView!

	var contact = Participant() {
		didSet {
			setContactData()
		}
	}

	var currentUser = Participant()

	var isParticipant: Bool = false {
		didSet {
			guard selectedImageView != nil else { return }
			selectedImageView.image = isParticipant ? UIImage(named: "imgRowSelectSelected") : UIImage(named: "imgRowSelectUnselected")
		}
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}

	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setup()
	}

	override func prepareForReuse() {
//				contact = Participant()
	}

	func setContactData() {
		guard nameLabel != nil else { return }

		nameLabel.text = contact.fullName
		avatarImageView.user = contact

		let fontWeight: Design.FontWeights = contact == currentUser ? .regular : .black
		Design.setDefaultFont(label: nameLabel, weight: fontWeight, size: 16)

		if let email = contact.email {
			detailLabel.text = email
			Design.setDefaultFont(label: detailLabel, weight: .italic, size: 14)
			detailLabel.isHidden = false
		} else {
			detailLabel.text = ""
			detailLabel.isHidden = true
		}
	}

	func setup() {
//				contentView.backgroundColor = .clear
		backgroundColor = .clear
	}

}
