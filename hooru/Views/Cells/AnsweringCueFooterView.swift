//  hooru
//  Created by Erwin Mazariegos on 12/18/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class AnsweringCueFooterView: UICollectionReusableView {

	@IBOutlet weak var finishedLabel: UILabel!

	var currentPromptGroup = PromptGroup() {
		didSet {
		}
	}

	override func awakeFromNib() {
				super.awakeFromNib()
		Design.setDefaultFont(label: finishedLabel, weight: .light)
	}
}
