//  hooru
//  Created by Brad Weber on 3/18/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class AlbumTableViewCell: UITableViewCell {
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var countLabel: UILabel!

	var album: Album? {
		didSet {
			updateDisplay()
		}
	}


	override func awakeFromNib() {
		super.awakeFromNib()

		updateDisplay()
	}


	private func updateDisplay() {
		guard nameLabel != nil else {
			return
		}

		guard album != nil else {
			nameLabel.text = nil
			countLabel.text = nil
			return
		}

		nameLabel.text = album?.name
		countLabel.text = "\(album!.prompts.count) Q\(album!.prompts.count == 1 ? "" : "s")"
	}
}
