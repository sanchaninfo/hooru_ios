//  hooru
//  Created by Erwin Mazariegos on 12/15/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class AnsweringCueCell: UICollectionViewCell {
	@IBOutlet weak var questionLabel: UILabel!
	@IBOutlet weak var recordingImage: UIImageView!

	var question: Question? {
		didSet {
			updateDisplay()
		}
	}

	var isRecording: Bool = false {
		didSet {
			if isRecording {
				recordingImage.isHidden = false
				animateRecordingImage()
			} else {
				recordingImage.isHidden = true
				recordingImage.layer.removeAnimation(forKey: recordingImageAnimationKey)
			}
		}
	}

	override func awakeFromNib() {
        super.awakeFromNib()
		layer.borderColor = Design.Elements.accentLineColor.cgColor
		layer.borderWidth = 1
		layer.cornerRadius = 5
		recordingImage.isHidden = true

		scaleAnimation.fromValue = 0.9
		scaleAnimation.toValue = 1.2
		scaleAnimation.duration = 1.0
		scaleAnimation.autoreverses = true
		scaleAnimation.repeatCount = Float.infinity

		updateDisplay()
	}

	override func prepareForReuse() {
				recordingImage.isHidden = true
	}

	private let recordingImageAnimationKey = "recordingImageAnimationKey"
	private let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")

	private func animateRecordingImage() {
		
		guard isRecording else {
			return
		}

		if self.recordingImage.layer.animation(forKey: self.recordingImageAnimationKey) == nil {
			self.recordingImage.layer.add(self.scaleAnimation, forKey: self.recordingImageAnimationKey)
		}
	}

	private func updateDisplay() {
		guard questionLabel != nil else {
			return
		}

		questionLabel.text = question?.text
		Design.setDefaultFont(label: questionLabel, weight: .light)
	}
}
