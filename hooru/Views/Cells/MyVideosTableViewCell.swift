//  hooru
//  Created by Brad Weber on 3/15/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class MyVideosTableViewCell: UITableViewCell {
	@IBOutlet weak var avatarImageView: CAAvatarImageView!
	@IBOutlet weak var introLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!

	var promptGroup: PromptGroup? {
		didSet {
			updateDisplay()
		}
	}

    override func awakeFromNib() {
        super.awakeFromNib()

		updateDisplay()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

	private func updateDisplay() {
		guard introLabel != nil else {
			return
		}

		guard promptGroup != nil else {
			avatarImageView.isHidden = true

			introLabel.text = nil
			nameLabel.text = nil

			dateLabel.text = nil

			return
		}

		avatarImageView.user = promptGroup!.sender
		avatarImageView.isHidden = false

		introLabel.text = "You answered \(promptGroup!.prompts.count) Q\(promptGroup!.prompts.count == 1 ? "" : "s") for"
		nameLabel.text = promptGroup!.sender?.fullName

		dateLabel.text = promptGroup!.creationDate?.relativeString(.extraShort)
	}
}
