//  hooru
//  Created by Erwin Mazariegos on 12/11/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class RecipientCell: UICollectionViewCell {

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var avatarImage: CAAvatarImageView!

	var recipient: Participant = Participant() {
		didSet {
			nameLabel.text = recipient.fullName
			Design.setDefaultFont(label: nameLabel, weight: .regular, size: 14)
			avatarImage.user = recipient
		}
	}

//	override func prepareForReuse() {
//
//	}
}
