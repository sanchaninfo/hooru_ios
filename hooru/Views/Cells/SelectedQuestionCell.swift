//  hooru
//  Created by Erwin Mazariegos on 12/1/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class SelectedQuestionCell: UICollectionViewCell {
	typealias IndexedQuestion = (question: Question, index: Int)

	@IBOutlet weak var questionLabel: UILabel!
	@IBOutlet weak var deleteButton: UIButton!

	@IBAction func deleteQuestion(_ sender: UIButton) {
		deleteClosure?(questionIndex)
	}

	var indexedQuestion: IndexedQuestion? {
		didSet {
			questionIndex = indexedQuestion?.index ?? 0
			updateDisplay()
		}
	}

	var questionIndex = 0
	var deleteClosure: ((_ index: Int) -> Void)?

	private func updateDisplay() {
		guard questionLabel != nil else {
			return
		}

		questionLabel.text = indexedQuestion?.question.text
	}
}
