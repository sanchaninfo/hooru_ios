//  hooru
//  Created by Vu Dang on 1/8/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class HomeFeedCell: UICollectionViewCell {

	@IBOutlet weak var backgroundImage: UIImageView!
	@IBOutlet weak var profileImage: CAAvatarImageView!
	@IBOutlet weak var smallProfileImage: CAAvatarImageView!
	@IBOutlet weak var readIcon: UIImageView!

	@IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cueLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var pendingLabel: UILabel!
	@IBOutlet weak var newLabel: UILabel!

	enum TileState {
		case notSet, pending, read, unread
	}

	var promptGroup: PromptGroup? {
		didSet {
			updateUI()
		}
	}

	var tileState: TileState {
		if let currentPromptGroup = promptGroup {
			if currentPromptGroup.isPending {
				return .pending
			}

			if currentPromptGroup.isRead {
				return .read
			} else {
				return .unread
			}
			
		} else {
			return .notSet
		}
	}

	override func prepareForReuse() {
		promptGroup = nil
	}
    
    override func awakeFromNib() {
        super.awakeFromNib()

		Design.setDefaultFont(label: pendingLabel, weight: .regular)
		pendingLabel.backgroundColor = .clear
		pendingLabel.layer.borderColor = Design.Elements.giantButtonBorderColor.cgColor
		pendingLabel.layer.cornerRadius = pendingLabel.frame.size.height / 2
		pendingLabel.layer.borderWidth = 1

		Design.setDefaultFont(label: newLabel, weight: .bold)
		newLabel.backgroundColor = Design.Elements.newLabelBackground
		newLabel.layer.cornerRadius = newLabel.frame.size.height / 2
	}

	func updateUI() {
//		
		backgroundImage.backgroundColor = .clear

		switch tileState {
		case .notSet:
			[nameLabel, cueLabel, timeLabel].forEach({ $0?.text = nil })
			[profileImage, pendingLabel, newLabel, smallProfileImage, readIcon].forEach({ $0.isHidden = true })

		case .pending:
			backgroundImage.image = UIImage(named: "imgTilePending")
			backgroundImage.backgroundColor = UIColor.black.withAlphaComponent(0.2)
			newLabel.isHidden = true
			pendingLabel.isHidden = false
			renderPromptGroupData()

		case .read:
			backgroundImage.image = UIImage(named: "imgTileRead")
			newLabel.isHidden = true
			pendingLabel.isHidden = true
			renderPromptGroupData()

		case .unread:
			backgroundImage.image = UIImage(named: "imgTileUnread")
			newLabel.isHidden = false
			pendingLabel.isHidden = true
			renderPromptGroupData()
		}

		// mutually exclusive elements
		profileImage.isHidden = !pendingLabel.isHidden
		smallProfileImage.isHidden = !profileImage.isHidden
		readIcon.isHidden = !smallProfileImage.isHidden
	}

	private func renderPromptGroupData() {
//		
		guard let currentPromptGroup = promptGroup else {
			return
		}

		profileImage.user = currentPromptGroup.recipient
		smallProfileImage.user = currentPromptGroup.recipient
		nameLabel.text = currentPromptGroup.recipient?.fullName// + " \(tileState)"
		cueLabel.text = "\(currentPromptGroup.prompts.count) Q\(currentPromptGroup.prompts.count == 1 ? "" : "s")"
		timeLabel.text = currentPromptGroup.creationDate?.relativeString(.extraShort)
		Design.setDefaultFont(label: nameLabel, weight: .regular)
		Design.setDefaultFont(label: cueLabel, weight: .italic)
		Design.setDefaultFont(label: timeLabel, weight: .regular)
	}
}
