//  hooru
//  Created by Brad Weber on 3/18/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class AlbumCueTableViewCell: UITableViewCell {
	@IBOutlet weak var avatarImageView: CAAvatarImageView!
	@IBOutlet weak var questionLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!

	var participant: Participant? {
		didSet {
			updateParticipantDisplay()
		}
	}

	var prompt: Prompt? {
		didSet {
			updatePromptDisplay()
		}
	}


    override func awakeFromNib() {
        super.awakeFromNib()

		updateParticipantDisplay()
		updatePromptDisplay()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }


	private func updateParticipantDisplay() {
		guard avatarImageView != nil else {
			return
		}

		avatarImageView.user = participant
		nameLabel.text = participant?.fullName
	}

	private func updatePromptDisplay() {
		guard questionLabel != nil else {
			return
		}

		questionLabel.text = prompt?.question?.text
	}
}
