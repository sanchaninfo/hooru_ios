//  hooru
//  Created by Erwin Mazariegos on 12/18/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class AnsweringCueHeaderView: UICollectionReusableView {

	@IBOutlet weak var swipeInstructionLabel: UILabel!
	@IBOutlet weak var cueCountLabel: UILabel!

	var leftArrowString: NSAttributedString {
		if let image = UIImage(named: "icBack") {
			let attachment = NSTextAttachment(data: nil, ofType: nil)
			attachment.image = image
			let string = NSAttributedString(attachment: attachment)
			return string
		} else {
			return NSAttributedString()
		}
	}

	var currentPromptGroup = PromptGroup() {
		didSet {
			let isResuming = 0 < currentPromptGroup.responseProgress
			let text = isResuming ? " Swipe to Resume" : " Swipe to Begin"
			let fullText = NSMutableAttributedString(attributedString: leftArrowString)
			fullText.append(NSAttributedString(string: text, attributes: Design.defaultFontAttibutes(weight: .light, size: Int(swipeInstructionLabel.font.pointSize))))
			swipeInstructionLabel.attributedText = fullText
			
			cueCountLabel.text = "\(currentPromptGroup.prompts.count) Q\(currentPromptGroup.prompts.count == 1 ? "" : "s")"
			Design.setDefaultFont(label: cueCountLabel, weight: .italic)
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
}
