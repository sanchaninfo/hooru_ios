//
//  RequestCell.swift
//  hooru
//
//  Created by Vu Dang on 12/8/17.
//  Copyright © 2017 Flying Spoke Media. All rights reserved.
//

import UIKit

class RequestTableCell: UITableViewCell {

	static var identifier: String {
		return "\(self)"
	}

	@IBOutlet weak var dotImage: UIImageView!
	@IBOutlet weak var profileImage: CAAvatarImageView!

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var cueCountLabel: UILabel!
	@IBOutlet weak var timeLabel: UILabel!

	override func prepareForReuse() {
		self.promptGroup = PromptGroup()
	}

	var promptGroup: PromptGroup = PromptGroup() {
		didSet {
			setUI()
		}
	}

	func setUI() {
		backgroundColor = .clear

		let progress = promptGroup.responseProgress

		profileImage.user = promptGroup.sender
		profileImage.borderMode = .partial(percentage: progress)

		if 0 < progress {
			cueCountLabel.text = "\(Int(Float(promptGroup.prompts.count) * (1.0 - progress))) of \(promptGroup.prompts.count) Q\(promptGroup.prompts.count == 1 ? "" : "s") to go"
		} else {
			cueCountLabel.text = "\(promptGroup.prompts.count) Q\(promptGroup.prompts.count == 1 ? "" : "s")"
		}

		nameLabel.text = promptGroup.sender?.fullName
		timeLabel.text = promptGroup.creationDate?.relativeString(.extraShort)

		if let prompt = cueCountLabel.text, !prompt.isEmpty { Design.setDefaultFont(label: cueCountLabel, weight: .italic, size: 13)	}
		if let name = nameLabel.text, !name.isEmpty { Design.setDefaultFont(label: nameLabel, weight: .black, size: 16) }
		if let time = timeLabel.text, !time.isEmpty { Design.setDefaultFont(label: timeLabel, weight: .light, size: 14) }
	}
}
