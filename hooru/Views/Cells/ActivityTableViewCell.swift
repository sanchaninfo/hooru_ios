//  hooru
//  Created by Brad Weber on 3/15/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class ActivityTableViewCell: UITableViewCell {
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var questionLabel: UILabel!

	var activity: Activity? {
		didSet {
			updateDisplay()
		}
	}


    override func awakeFromNib() {
        super.awakeFromNib()

		updateDisplay()
    }


	private func updateDisplay() {
		guard descriptionLabel != nil else {
			return
		}

		guard activity != nil else {
			descriptionLabel.text = nil
			return
		}

		questionLabel.text = activity?.prompt?.question?.text
		dateLabel.text = activity!.created?.relativeString(.extraShort)

		let boldFont = UIFont(name: "Lato-Bold", size: 16) ?? UIFont.boldSystemFont(ofSize: 16)
		let regularFont = UIFont(name: "Lato-Regular", size: 16) ?? UIFont.systemFont(ofSize: 16)
		let color = UIColor.white

		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.lineBreakMode = .byWordWrapping
		paragraphStyle.alignment = .natural

		let boldAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: boldFont, NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.kern: -0.3]

		let regularAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: regularFont, NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.kern: -0.3]

		// Question Asker
		let questionAskerName = activity!.questionAsker?.id == AccountManager.currentLoggedInUser?.id ? "You" : activity!.questionAsker?.fullName ?? activity!.questionAsker?.email ?? ""
		let questionAskerString = NSAttributedString(string: questionAskerName, attributes: boldAttributes)

		// Video Author
		let videoAuthorName = activity!.videoAuthor?.id == AccountManager.currentLoggedInUser?.id ? "You" : activity!.videoAuthor?.fullName ?? activity!.videoAuthor?.email ?? ""
		let videoAuthorString = NSAttributedString(string: videoAuthorName, attributes: boldAttributes)


		let bodyString: NSAttributedString
		let combinedString: NSMutableAttributedString

		switch true {
		case activity!.prompt?.hasResponse:
			combinedString = NSMutableAttributedString(attributedString: videoAuthorString)
			bodyString = NSAttributedString(string: " answered a Q from ", attributes: regularAttributes)
			combinedString.append(bodyString)
			combinedString.append(questionAskerString)

		default:
			combinedString = NSMutableAttributedString(attributedString: questionAskerString)
			bodyString = NSAttributedString(string: " sent a Q to ", attributes: regularAttributes)
			combinedString.append(bodyString)
			combinedString.append(videoAuthorString)
		}

		let closing = NSAttributedString(string: ".", attributes: regularAttributes)
		combinedString.append(closing)

		descriptionLabel.attributedText = combinedString
	}
}
