//  hooru
//  Created by Erwin Mazariegos on 1/15/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class BottomShadowView: UIView {

	let shadowLayer = CAGradientLayer()

	override var backgroundColor: UIColor? {
		didSet {
			if backgroundColor != .clear {
				self.backgroundColor = .clear
			}
		}
	}

	override func didMoveToSuperview() {
				super.didMoveToSuperview()
		self.backgroundColor = .clear

		shadowLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
		layer.addSublayer(shadowLayer)
	}

	override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
		return false
	}

	override func layoutSubviews() {
				super.layoutSubviews()
		shadowLayer.frame = self.bounds
	}

}
