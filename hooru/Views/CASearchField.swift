//  hooru
//  Created by Erwin Mazariegos on 1/22/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class CASearchField: CATextField {

	var magnifierImageView = UIImageView(image: UIImage(named: "imgSearch"))
	var placeholderText: String?

	override var placeholder: String? {
		willSet {
			if let text = newValue {
				placeholderText = text
			}
		}
	}

	override func awakeFromNib() {
				super.awakeFromNib()
		setup()
	}

	override init(frame: CGRect) {
				super.init(frame: frame)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
				super.init(coder: aDecoder)
		setup()
	}

	func setup() {
		
		if let text = placeholder {
			placeholderText = text
		}

		magnifierImageView.contentMode = .scaleAspectFit
		leftView = magnifierImageView
		leftViewMode = .always
	}

	override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
		let width: CGFloat = 30
		let height: CGFloat = 18

		let x: CGFloat
		let y = (bounds.size.height - height) / 2

		if isEditing {
			placeholder = placeholderText
			x = 0
		} else {
			placeholder = nil
			x = (bounds.size.width - width) / 2
		}

		textAlignment = .left
		return CGRect(origin: CGPoint(x: x, y: y), size: CGSize(width: width, height: height))
	}
}
