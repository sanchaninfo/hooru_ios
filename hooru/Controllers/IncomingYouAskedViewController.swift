//  hooru
//  Created by Brad Weber on 3/17/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class IncomingYouAskedViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	@IBOutlet weak var emptyView: UIView!
	@IBOutlet weak var emptyHeaderLabel: UILabel!
	@IBOutlet weak var emptyDescriptionLabel: UILabel!

	@IBOutlet weak var collectionView: UICollectionView!

	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

	var currentUser = Participant()
	var promptGroups = [PromptGroup]()


    override func viewDidLoad() {
        super.viewDidLoad()

		Design.setDefaultFont(label: emptyHeaderLabel, weight: .light)
		Design.setDefaultFont(label: emptyDescriptionLabel, weight: .regular)
		emptyView.backgroundColor = Design.Overlays.backgroundColor

		hideEmptyView()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		if let user = AccountManager.currentLoggedInUser {
			currentUser = user
		} else {
			showEmptyView()
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


	func loadData() {
		loadingIndicator.startAnimating()

		guard AccountManager.currentLoggedInUser != nil else {
			loadingIndicator.stopAnimating()
			promptGroups = [PromptGroup]()
			showEmptyView()

			return
		}

		_ = PromptAPI.getIncomingYouAskedPromptGroups(completion: { (promptGroups, error) in
			DispatchQueue.main.async {
				self.promptGroups = promptGroups
				self.loadingIndicator.stopAnimating()
				self.collectionView.reloadData()

				if self.promptGroups.count == 0 {
					self.showEmptyView()
				} else {
					self.hideEmptyView()
				}
			}
		})
	}

	func showEmptyView() {
				UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
			self.emptyView.alpha = 1
		})
	}

	func hideEmptyView() {
				UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
			self.emptyView.alpha = 0
		})
	}

	// MARK: - CollectionView methods
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.width/2)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
	}
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 0.0
	}
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 0.0
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return promptGroups.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeFeedCell", for: indexPath) as? HomeFeedCell {
			let promptGroup = promptGroups[indexPath.row]
			cell.promptGroup = promptGroup

			return cell
			
		} else {
			return UICollectionViewCell()
		}
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let homeCell = collectionView.cellForItem(at: indexPath) as? HomeFeedCell, homeCell.tileState != HomeFeedCell.TileState.pending else {
			return
		}

		performSegue(withIdentifier: "ShowPlayback", sender: self)
	}


    // MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowPlayback":
			guard let selectedIndex = collectionView.indexPathsForSelectedItems else { return }

			if let playbackController = segue.destination as? PlaybackViewController {
				playbackController.currentPromptGroup = promptGroups[selectedIndex[0].row]
			}

		default:
			print("Unexpected segue: \(identifier)")
		}
	}
}
