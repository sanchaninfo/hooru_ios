//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class QuestionSelectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
	@IBOutlet weak var titleLabel: UILabel!

	@IBOutlet weak var selectedQuestionsView: UICollectionView!
	@IBOutlet weak var accentView: UIView!
	@IBOutlet weak var searchField: CASearchField!
	@IBOutlet weak var questionListView: UICollectionView!

	@IBOutlet weak var questionListViewLayout: UICollectionViewFlowLayout!
	@IBOutlet weak var nextButton: UIButton!

	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	
	@IBAction func showRecipientSelection(_ sender: UIButton) {
		performSegue(withIdentifier: "showRecipientSelectionViewController", sender: self)
	}

	@IBAction func cancel(_ sender: UIButton) {
		// we might have gotten here from "sending a response" screen, so we can't just go back
		if let presenter = presentingViewController as? UINavigationController, presenter.viewControllers.count > 1 {
			presentInitialController(inStoryboardFile: "Main")		// got here through a circuitous route
		} else {
			dismiss(animated: true)		// got here right from home screen
		}
	}

	@IBAction func unwindToCueSelection(_ segue: UIStoryboardSegue) {
		// TODO: Refresh based on current invitation?
	}

	let maxQuestionCount = 3

	var questions = [Question]() {
		didSet {
			search()
		}
	}
	private var filteredQuestions = [Question]()

	var questionListCellFullWidth: CGFloat = 0
	var questionListCelLabelAttributes = [NSAttributedStringKey : Any]()

	var currentInvitation = Invitation()  {
		didSet {
			setNextButtonState()
		}
	}

	var searchTerm = "" {
		didSet {
			search()
		}
	}


    override func viewDidLoad() {
        super.viewDidLoad()

		titleLabel.text = "Pick Up To \(maxQuestionCount)"

		accentView.backgroundColor = Design.Elements.accentLineColor

		setNextButtonState()

		if let user = AccountManager.currentLoggedInUser {
			currentInvitation.sender = user
		}

		fetchQuestions()

		searchField.entryTextAttributes = Design.defaultFontAttibutes(weight: .regular, size: 14)
		searchField.addTarget(self, action: #selector(filterQuestions(sender:)), for: .editingChanged)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		search()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let recipientSelectionController = segue.destination as? RecipientSelectionViewController {
			recipientSelectionController.currentInvitation = currentInvitation
		}
	}


	@objc func filterQuestions(sender: UITextField) {
		if let term = sender.text {
			searchTerm = term
		} else {
			searchTerm = ""
		}
	}

	private func search() {
		guard questionListView != nil else {
			return
		}

		if searchTerm.isEmpty {
			// All cues
			filteredQuestions = questions

		} else {
			filteredQuestions = questions.filter({ (question) -> Bool in
				return question.text?.lowercased().range(of: searchTerm.lowercased()) != nil
			})
		}

		filteredQuestions = filteredQuestions.filter({ (question) -> Bool in
			return !currentInvitation.questions.contains(question)
		})

		questionListView.reloadData()
	}

	func fetchQuestions() {
		loadingIndicator.startAnimating()
		_ = QuestionAPI.getQuestions { (questions, error) in
			DispatchQueue.main.async {
				self.questions = questions

				self.questionListView.reloadData()
				self.loadingIndicator.stopAnimating()
			}
		}
	}

	func unselectQuestionAtIndex(index: Int) {
		let _ = currentInvitation.questions.remove(at: index)
		selectedQuestionsView.reloadData()
		setNextButtonState()

		search()
	}

	func setNextButtonState() {
		nextButton.isEnabled = currentInvitation.questions.count > 0
	}


	// MARK: - CollectionView methods
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		switch collectionView {
		case questionListView:
			return filteredQuestions.count
		case selectedQuestionsView:
			return currentInvitation.questions.count
		default:
			return 0
		}
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		switch collectionView {
		case questionListView:
			let question = filteredQuestions[indexPath.row]
			let cellWidth = collectionView.frame.size.width
			return CGSize(width: cellWidth, height: Design.Cells.heightForVerticalQuestionText(question.text ?? "", inCellWithWidth: cellWidth))
		case selectedQuestionsView:
			return CGSize(width: 200, height: collectionView.frame.size.height)
		default:
			return .zero
		}
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		switch collectionView {
		case questionListView:
			if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionListCell", for: indexPath) as? QuestionListCell {
				cell.question = filteredQuestions[indexPath.row]

				return cell
			}

		case selectedQuestionsView:
			if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedQuestionCell", for: indexPath) as? SelectedQuestionCell {
				cell.indexedQuestion = (question: currentInvitation.questions[indexPath.row], index: indexPath.row)
				cell.deleteClosure = { index in
					self.unselectQuestionAtIndex(index: index)
				}

				return cell
			}

		default:
			break
		}

		return UICollectionViewCell()
	}

	func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
		switch collectionView {
		case questionListView:
			// Limit number of Q
			if currentInvitation.questions.count < maxQuestionCount {
				return true
			}

			let alert = UIAlertController(title: "Q Maximum", message: "You have already selected \(maxQuestionCount). Please remove one or more before selecting another." , preferredStyle: .alert)
			let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
			alert.addAction(okAction)

			present(controller: alert)

			return false

		case selectedQuestionsView:
			return false

		default:
			return false
		}
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard collectionView == questionListView else {
			return
		}

		let selectedQuestion = filteredQuestions.remove(at: indexPath.row)
		currentInvitation.questions.append(selectedQuestion)
		questionListView.reloadData()
		selectedQuestionsView.reloadData()
		setNextButtonState()
	}


	// MARK: UITextFieldDelegate
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		view.endEditing(true)

		return true
	}
}
