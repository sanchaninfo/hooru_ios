//  hooru
//  Created by Vu Dang on 2/12/18 using Swift 4.0.
//  Copyright © 2018 Flying Spokes. All rights reserved.

import UIKit

class AccountNameCreationViewController: UIViewController, UITextFieldDelegate {
	@IBOutlet weak var closeButton: UIButton!

	@IBOutlet weak var keyboardAdjustingScrollView: IAKeyboardAdjustingScrollView!
	@IBOutlet weak var keyboardAdjustingScrollViewBottomConstraint: NSLayoutConstraint!
	@IBOutlet weak var keyboardAdjustingContentView: UIView!

	@IBOutlet weak var signUpLabel: UILabel!
	@IBOutlet weak var firstNameTextField: CATextField!
	@IBOutlet weak var lastNameTextField: CATextField!
	@IBOutlet weak var nextArrowButton: UIButton!

	@IBAction func showAccountCreationViewController() {
				guard nameTextFieldsAreValid else {
			return
		}

		if firstNameTextField.sanitizedText().isEmpty {
			currentUser.firstName = nil
		} else {
			currentUser.firstName = firstNameTextField.sanitizedText()
		}
		if lastNameTextField.sanitizedText().isEmpty {
			currentUser.lastName = nil
		} else {
			currentUser.lastName = lastNameTextField.sanitizedText()
		}
		performSegue(withIdentifier: "showAccountCreationViewController", sender: nil)
	}

	@IBAction func unwindToAccountNameCreation(_ segue: UIStoryboardSegue) {
		if AccountManager.currentLoggedInUser != nil {
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
				if self.presentingViewController is WelcomeViewController {
					self.performSegue(withIdentifier: "ShowMainNavigationViewController", sender: self)
				} else {
					self.close()
				}
			})
		}
	}

	@IBAction func close() {
		dismiss(animated: true, completion: nil)
	}


	// MARK: - Variables
	var allowClose = false {
		didSet {
			guard closeButton != nil else {
				return
			}

			closeButton.isHidden = !allowClose
		}
	}

	var currentUser = Participant()

	private var nameTextFieldsAreValid: Bool {
		return !firstNameTextField.sanitizedText().isEmpty || !lastNameTextField.sanitizedText().isEmpty
	}


	// MARK: - View Controlller Life Cycle
	override func viewDidLoad() {
				super.viewDidLoad()

		Design.setDefaultFont(label: signUpLabel, weight: .light)
		firstNameTextField.entryTextAttributes = Design.defaultFontAttibutes(weight: .light, size: 18)
		lastNameTextField.entryTextAttributes = Design.defaultFontAttibutes(weight: .light, size: 18)

		disableNextArrowButton()

		closeButton.isHidden = !allowClose

		keyboardAdjustingScrollView.adjustableBottomContraint = keyboardAdjustingScrollViewBottomConstraint
	}

	override func viewDidLayoutSubviews() {
				super.viewDidLayoutSubviews()

		keyboardAdjustingScrollView.contentSize = CGSize(width: keyboardAdjustingScrollView.bounds.width, height: keyboardAdjustingContentView.bounds.height)
	}


	override func viewWillDisappear(_ animated: Bool) {
				super.viewWillDisappear(animated)
		view.endEditing(true)
	}


	// MARK: - UI Functions
	private func enableNextArrowButton() {
				nextArrowButton.isEnabled = true
		nextArrowButton.alpha = 1.0
	}

	private func disableNextArrowButton() {
				nextArrowButton.isEnabled = false
		nextArrowButton.alpha = 0.5
	}


	// MARK: - Segue Actions
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showAccountCreationViewController" {
			guard let destination = segue.destination as? AccountCreationViewController else {
				return
			}
			destination.currentUser = currentUser
		}
	}


	// MARK: - Textfield Delegates
	func textFieldDidBeginEditing(_ textField: UITextField) {
				keyboardAdjustingScrollView.activeField = textField
	}

	@IBAction func textFieldChanged(_ sender: Any) {
				guard nameTextFieldsAreValid else {
			disableNextArrowButton()
			return
		}
		enableNextArrowButton()
	}

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
				textField.resignFirstResponder()
		if textField == lastNameTextField && nameTextFieldsAreValid {
			showAccountCreationViewController()
		}
		return true
	}

	func textFieldDidEndEditing(_ textField: UITextField) {
				if textField == firstNameTextField {
			lastNameTextField.becomeFirstResponder()
		}
	}
}
