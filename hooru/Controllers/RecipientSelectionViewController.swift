//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit
import Contacts

class RecipientSelectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
	@IBOutlet weak var searchField: CATextField!
	@IBOutlet weak var addButton: UIButton!
	@IBOutlet weak var validEmailAddressLabel: UILabel!

	@IBOutlet weak var accentView: UIView!

	@IBOutlet weak var contactsTable: UITableView!
	@IBOutlet weak var contactsTableBottomConstraint: NSLayoutConstraint!

	@IBOutlet weak var backButton: UIButton!
	@IBOutlet weak var nextButton: UIButton!

	@IBAction func showRequestConfirmation(_ sender: UIButton) {
				performSegue(withIdentifier: "showRequestConfirmViewController", sender: self)
	}

	@IBAction func unwindToRecipientSelection(_ segue: UIStoryboardSegue) {  }

	private var nonContactRecipients = [String]()
	private var selectedNonContactRecipients = [String]()

	private let sectionTitles = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","#"]

	var sectionedContacts = [String: [Participant]]()
	var filteredContacts = [String: [Participant]]()
	var currentUser = Participant()
	var currentInvitation = Invitation() {
		didSet {
			nonContactRecipients = currentInvitation.nonContactRecipients
			selectedNonContactRecipients = currentInvitation.nonContactRecipients

			setNextButtonState()
		}
	}

	var searchTerm = "" {
		didSet {
			if searchTerm.isEmpty {
				addButton.isHidden = true
				validEmailAddressLabel.isHidden = true

				filteredContacts = sectionedContacts
				contactsTable.reloadData()
			} else {
				DispatchQueue.global(qos: .userInitiated).async() {
					let term = self.searchTerm.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)

					if term.count < oldValue.count {	// User is deleting letters from term, so search whole list again
						self.filteredContacts = self.sectionedContacts
					}

					self.filteredContacts.forEach({ (item) in
						let filteredSectionContacts = item.value.filter({ (participant) -> Bool in
							return (
								(participant.firstName == nil ? false : participant.firstName!.lowercased().hasPrefix(term) ) ||
								(participant.lastName == nil ? false : participant.lastName!.lowercased().hasPrefix(term) ) ||
								(participant.email == nil ? false : participant.email!.lowercased().hasPrefix(term) )
							)
						})

						if filteredSectionContacts.count == 0 {
							self.filteredContacts.removeValue(forKey: item.key)
						} else {
							self.filteredContacts[item.key] = filteredSectionContacts
						}
					})

					DispatchQueue.main.async {
						if self.filteredContacts.count <= 0 {
							// Check for valid email address
							if self.isValidEmailAddress(term) {
								self.addButton.isHidden = false
								self.validEmailAddressLabel.isHidden = true
							} else{
								self.addButton.isHidden = true
								self.validEmailAddressLabel.isHidden = false
							}

						} else {
							// As long as there are matching contacts, use those.
							self.addButton.isHidden = true
							self.validEmailAddressLabel.isHidden = true
						}

						self.contactsTable.reloadData()
					}
				}
			}
		}
	}

	override func viewDidLoad() {
		        super.viewDidLoad()

		CNContactStore().requestAccess(for: .contacts) { (granted, error) in
			if granted {
				DispatchQueue.main.async {
					self.fetchContacts()
				}
			}
		}

		contactsTable.sectionIndexBackgroundColor = .clear
		contactsTable.sectionIndexColor = Design.Elements.textColor
		contactsTable.tableFooterView = UIView()

		searchField.entryTextAttributes = Design.defaultFontAttibutes(weight: .regular, size: 14)
		searchField.textAlignment = .left
		searchField.addTarget(self, action: #selector(filterContacts(sender:)), for: .editingChanged)

		addButton.isHidden = true
		validEmailAddressLabel.isHidden = true
	}

	override func viewWillAppear(_ animated: Bool) {
				super.viewWillAppear(animated)
		if let user = AccountManager.currentLoggedInUser {
			self.currentUser = user
		}

		if currentInvitation.recipients.count > 0 {
			contactsTable?.reloadData()
		}

		registerKeyboardNotifications()
	}

	override func viewWillDisappear(_ animated: Bool) {
				super.viewWillDisappear(animated)
		deregisterKeyboardNotifications()
	}

    override func didReceiveMemoryWarning() {
		        super.didReceiveMemoryWarning()
    }

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
				if let confirmationController = segue.destination as? RequestConfirmViewController {
			confirmationController.currentInvitation = currentInvitation
		}
	}

	func registerKeyboardNotifications() {
				NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}

	func deregisterKeyboardNotifications() {
				NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}

	@objc func keyboardWillShow(notification: NSNotification) {
				if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			contactsTableBottomConstraint.constant = keyboardSize.height
		}
		searchField.layoutSubviews()	// animates the transition to editing TODO: animate the transition to not editing. Note: usual UIView layout, display methods didn't work. needs custom animation
	}

	@objc func keyboardWillHide(notification: NSNotification) {
				contactsTableBottomConstraint.constant = 0
	}

	@objc func filterContacts(sender: UITextField) {
				if let term = sender.text {
			searchTerm = term
		} else {
			searchTerm = ""
		}
	}

	func setNextButtonState() {
				guard nextButton != nil else {
			return
		}
		
		nextButton.isEnabled = 0 < (currentInvitation.recipients.count + selectedNonContactRecipients.count)
	}


	// MARK: - Non-Contacts
	@IBAction func addNonContact() {
		guard let emailAddress = searchField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {

			return
		}

		guard nonContactRecipients.index(of: emailAddress) == nil else {
			// Duplicate
			return
		}

		nonContactRecipients.append(emailAddress)
		selectedNonContactRecipients.append(emailAddress)

		currentInvitation.nonContactRecipients = selectedNonContactRecipients

		addButton.isHidden = true
		validEmailAddressLabel.isHidden = true

		searchField.text = nil
		searchTerm = ""
	}

	private func isValidEmailAddress(_ emailAddress: String) -> Bool {
		let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
			/*
			"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
			*/

		let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)

		return emailTest.evaluate(with: emailAddress)
	}


	// MARK: - Contacts
	private func fetchContacts() {
				var contacts = [CNContact]()
		let store = CNContactStore()
		let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactEmailAddressesKey] // CNContactFormatter.descriptorForRequiredKeys(for: .fullName)

		// Fetch all contacts
		let request = CNContactFetchRequest(keysToFetch: keysToFetch as [CNKeyDescriptor])
		request.predicate = nil
		request.sortOrder = .givenName

		do {
			try store.enumerateContacts(with: request, usingBlock: { (contact, _) -> Void in
				contacts.append(contact)
			})
		} catch {
			print("Error fetching contacts: \(error.localizedDescription)")
			return
		}

		// Convert contacts into Participants
		var contactParticipants = [Participant]()
		for contact in contacts {
			if contact.emailAddresses.count == 0 {
				let participant = Participant(first: contact.givenName, last: contact.familyName, emailString: nil)
				contactParticipants.append(participant)

			} else {
				// If there are multiple email addresses for a contact, we'll create a different participant for each.
				// TODO: only use email field if the email is a valid address
				for emailAddress in contact.emailAddresses {
					let participant = Participant(first: contact.givenName, last: contact.familyName, emailString: emailAddress.value as String)
					contactParticipants.append(participant)
				}
			}
		}

		// Sort the list
		contactParticipants.sort()

		// Group participants into sections
		sectionedContacts = [String: [Participant]]()

		// Initialize the sections
		for title in sectionTitles {
			sectionedContacts[title] = [Participant]()
		}

		// Load people into sections
		for participant in contactParticipants {
			let indexCharacter = participant.indexCharacter
			sectionedContacts[indexCharacter]?.append(participant)
		}

		filteredContacts = sectionedContacts
		contactsTable.reloadData()
		fetchAvatars(contacts: contactParticipants)
	}

	func fetchAvatars(contacts: [Participant]) {
		
		if AccountManager.currentLoggedInUser != nil {
			var emailAddresses = [String]()
			for contact in contacts {
				guard let contactEmail = contact.email else {
					continue
				}

				emailAddresses.append(contactEmail)
			}

			_ = UserAPI.getUserAvatarsForEmailAddresses(emailAddresses) { (avatars, error) in
				for avatar in avatars {
					for contact in contacts {
						// TODO: Should we compare IDs instead?
						if contact.email == avatar.email {
							contact.avatarFileName = avatar.avatarFileName
						}
					}
				}
			}
		}
	}

	private func participantAtIndexPath(_ indexPath: IndexPath) -> Participant? {
		guard 0 < indexPath.section else {
			return nil
		}

		// -1 to adjust for non-contacts
		guard let participants = filteredContacts[sectionTitles[indexPath.section - 1]] else {
			return nil
		}

		guard (0 ..< participants.count).contains(indexPath.row) else {
			return nil
		}

		return participants[indexPath.row]
	}


	// MARK: - UITableViewDataSource
	func numberOfSections(in tableView: UITableView) -> Int {
		return sectionTitles.count + 1 // +1 for non-contacts
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard 0 < section else {
			return nonContactRecipients.count
		}

		let indexTitle = sectionTitles[section - 1]

		return filteredContacts[indexTitle]?.count ?? 0
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.section == 0 {
			return 44
		} else {
			return 63
		}
	}

	func sectionIndexTitles(for tableView: UITableView) -> [String]? {
		return [""] + sectionTitles
	}

	func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
		guard title != "" else {
			return 0
		}

		return (sectionTitles.index(of: title) ?? sectionTitles.count - 1) + 1 // +1 for non-contacts
	}

//	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//		let titleIndex = sectionTitles[section]
//		if filteredContacts[titleIndex]?.count ?? 0 <= 0 {
//			// Hide section headers with no rows
//			return 0
//		}
//		return 0
//	}
//
//	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//		//		if let header = tableView.dequeueReusableCell(withIdentifier: "HeaderCellIdentifier") as? HeaderCellClass {
//		//			header.title = sectionTitles[section].lowercased()
//		//			return header
//		//		}
//
//		return nil
//	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard 0 < indexPath.section else {
			// Non-contact section
			if let cell = tableView.dequeueReusableCell(withIdentifier: "NonContactTableViewCell") as? NonContactTableViewCell {
				let recipient = nonContactRecipients[indexPath.row]
				cell.emailAddress = recipient
				cell.isNonContactSelected = selectedNonContactRecipients.index(of: recipient) != nil

				return cell
			}

			return UITableViewCell()
		}

		if let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableCell") as? ContactTableCell {
			if let participant = participantAtIndexPath(indexPath) {
				cell.contact = participant
				// currentInvitation.recipients.contains(participant) is not reliable
				var isParticipant = false
				currentInvitation.recipients.forEach({
					check: if $0 == participant {
						isParticipant = true
						break check
					}
				})
				cell.isParticipant = isParticipant
				cell.currentUser = currentUser
			}
			return cell
		}

		return UITableViewCell()
	}


	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
				tableView.deselectRow(at: indexPath, animated: false)

		if indexPath.section == 0 {
			// Non-Contacts
			if let cell = tableView.cellForRow(at: indexPath) as? NonContactTableViewCell {
				let emailString = nonContactRecipients[indexPath.row]

				cell.isNonContactSelected = !cell.isNonContactSelected

				if cell.isNonContactSelected {
					if selectedNonContactRecipients.index(of: emailString) == nil {
						selectedNonContactRecipients.append(emailString)
					}
				} else {
					if let idx = selectedNonContactRecipients.index(of: emailString) {
						selectedNonContactRecipients.remove(at: idx)
					}
				}

				currentInvitation.nonContactRecipients = selectedNonContactRecipients

				setNextButtonState()
			}

		} else {
			// Contacts
			if let cell = tableView.cellForRow(at: indexPath) as? ContactTableCell {
				let person = cell.contact

				guard person.email != nil else {
					return
				}

				cell.isParticipant = !cell.isParticipant

				if cell.isParticipant {
					currentInvitation.recipients.append(person)
				} else {
					if let idx = currentInvitation.recipients.index(of: person) {
						currentInvitation.recipients.remove(at: idx)
					}
				}

				setNextButtonState()
			}
		}
	}

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
				textField.resignFirstResponder()
		return true
	}

	func textFieldDidEndEditing(_ textField: UITextField) {
				textField.resignFirstResponder()
	}
}
