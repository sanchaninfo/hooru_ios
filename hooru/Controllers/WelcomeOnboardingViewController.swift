//  hooru
//  Created by Brad Weber on 2/16/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class WelcomeOnboardingViewController: UIViewController, UIScrollViewDelegate {
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var backgroundView: UIView!
	@IBOutlet weak var backgroundViewHeightConstraint: NSLayoutConstraint!

	// Page 2
	@IBOutlet weak var whoAreYouContainerLeadingConstraint: NSLayoutConstraint!
	@IBOutlet weak var handLeadingConstraint: NSLayoutConstraint!
	@IBOutlet weak var page2Button: UIButton!

	// Page 3
	@IBOutlet weak var animatedQuestionsTopConstraint: NSLayoutConstraint!
	@IBOutlet weak var animatedContactsTopConstraint: NSLayoutConstraint!
	@IBOutlet weak var page3Button: UIButton!

	@IBOutlet weak var questionsContainer: UIView!
	@IBOutlet weak var contactsContainer: UIView!
	@IBOutlet weak var responseContainer: UIView!

	@IBOutlet weak var bubble1View: UIView!
	@IBOutlet weak var bubble2View: UIView!
	@IBOutlet weak var bubble3View: UIView!

	@IBOutlet weak var phoneSubtextLabel: UILabel!

	private var backgroundGradientLayer: CAGradientLayer?

	private var hasAnimatedHand = false
	private var hasAnimatedPhone = false
	private var currentPage = 0

	private var originalWhoAreYouContainerLeadingConstraintConstant: CGFloat = 0
	private var originalHandLeadingConstraintConstant: CGFloat = 0
	private var originalContactsTopConstraintConstant: CGFloat = 0
	private var originalQuestionsTopConstraintConstant: CGFloat = 0
	private var originalPhoneSubtext: String? = ""

    override func viewDidLoad() {
		        super.viewDidLoad()
		setBackgroundColors()

		page2Button.isHidden = true
		page3Button.isHidden = true

		handLeadingConstraint.constant = scrollView.bounds.width

		originalWhoAreYouContainerLeadingConstraintConstant = whoAreYouContainerLeadingConstraint.constant
		originalHandLeadingConstraintConstant = handLeadingConstraint.constant
		originalContactsTopConstraintConstant = animatedContactsTopConstraint.constant
		originalQuestionsTopConstraintConstant = animatedQuestionsTopConstraint.constant
		originalPhoneSubtext = phoneSubtextLabel.text

		let height = view.bounds.height * 4.0
		backgroundViewHeightConstraint.constant = height

		scrollView.contentSize = CGSize(width: scrollView.bounds.width, height: height)
    }

	override func viewWillAppear(_ animated: Bool) {
				super.viewWillAppear(animated)
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		if AccountManager.currentLoggedInUser != nil {
			performSegue(withIdentifier: "ShowGetStarted", sender: self)
		}
	}

	override func viewDidLayoutSubviews() {
				if scrollView.contentOffset.y < 100 {
			scrollView.setContentOffset(.zero, animated: false)
		}

		backgroundGradientLayer?.frame = CGRect(origin: .zero, size: backgroundView.bounds.size)
	}

    override func didReceiveMemoryWarning() {
		        super.didReceiveMemoryWarning()
    }

	@IBAction func pageDown() {
				scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y + scrollView.bounds.height), animated: true)
}

	@IBAction func logIn() {
		performSegue(withIdentifier: "ShowLogIn", sender: self)
	}

	@IBAction func getStarted() {
		performSegue(withIdentifier: "ShowGetStarted", sender: self)
	}

	private func resetHandAnimation() {
				hasAnimatedHand = false
		page2Button.isHidden = true

		whoAreYouContainerLeadingConstraint.constant = originalWhoAreYouContainerLeadingConstraintConstant
		handLeadingConstraint.constant = originalHandLeadingConstraintConstant

		self.view.layoutIfNeeded()
	}

	private func animateHand() {
				guard !hasAnimatedHand else {
			return
		}

		hasAnimatedHand = true

		whoAreYouContainerLeadingConstraint.constant = -scrollView.bounds.width
		handLeadingConstraint.constant = -186 // Hand image width

		UIView.animate(withDuration: 3.0, delay: 1.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
			self.view.layoutIfNeeded()
		}, completion: { (completed) in
			self.page2Button.isHidden = false
		})
	}

	private func resetQuestionsAnimation() {
				hasAnimatedPhone = false
		page3Button.isHidden = true

		animatedQuestionsTopConstraint.constant = originalQuestionsTopConstraintConstant
		animatedContactsTopConstraint.constant = originalContactsTopConstraintConstant
		phoneSubtextLabel.text = originalPhoneSubtext

		bubble1View.alpha = 0.0
		bubble2View.alpha = 0.0
		bubble3View.alpha = 0.0
		questionsContainer.isHidden = false
		responseContainer.isHidden = true
		contactsContainer.isHidden = true

		self.view.layoutIfNeeded()
	}

	private func animatePhoneQuestions() {
				guard !hasAnimatedPhone else {
			return
		}

		guard self.currentPage == 3 else { return }

		hasAnimatedPhone = true

		animatedQuestionsTopConstraint.constant = -400

		let animationDuration: Double = 3.0
		UIView.animate(withDuration: animationDuration) {
			self.view.layoutIfNeeded()
		}

		// We are not animating to contacts in a completion block
		// for the UIView animation above because we want to start
		// the transition before the questions finish animating.
		DispatchQueue.main.asyncAfter(deadline: .now() + (animationDuration / 2.0)) {
			guard self.currentPage == 3 else { return }
			self.animateToContacts()
		}
	}

	private func animateToContacts() {
				guard self.currentPage == 3 else { return }

		// Fade out the label
		UIView.animate(withDuration: 0.5, animations: {
			self.phoneSubtextLabel.alpha = 0.0
		}) { (fadeOutFinished) in
			guard self.currentPage == 3 else { return }
			// Flip the container
			let transitionOptions: UIViewAnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
			UIView.transition(from: self.questionsContainer, to: self.contactsContainer, duration: 1.0, options: transitionOptions) { (flipFinished) in
				guard self.currentPage == 3 else { return }
				// Update the label text
				self.phoneSubtextLabel.text = "Pick a\nperson"

				// Fade in the label
				UIView.animate(withDuration: 0.5, animations: {
					guard self.currentPage == 3 else { return }
					self.phoneSubtextLabel.alpha = 1.0
				}, completion: { (fadeInFinished) in
					self.animateContacts()
				})
			}
		}
	}

	private func animateContacts() {
				guard self.currentPage == 3 else { return }

		animatedContactsTopConstraint.constant = -400

		let animationDuration: Double = 3.0
		UIView.animate(withDuration: animationDuration) {
			self.view.layoutIfNeeded()
		}

		// We are not animating to response in a completion block
		// for the UIView animation above because we want to start
		// the transition before the contacts finish animating.
		DispatchQueue.main.asyncAfter(deadline: .now() + (animationDuration / 2.0)) {
			guard self.currentPage == 3 else { return }
			self.animateToResponse()
		}
	}

	private func animateToResponse() {
				// Fade out the label
		guard self.currentPage == 3 else { return }

		UIView.animate(withDuration: 0.5, animations: {
			self.phoneSubtextLabel.alpha = 0.0
		}) { (fadeOutFinished) in
			guard self.currentPage == 3 else { return }
			// Flip the container
			let transitionOptions: UIViewAnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
			UIView.transition(from: self.contactsContainer, to: self.responseContainer, duration: 1.0, options: transitionOptions) { (flipFinished) in
				guard self.currentPage == 3 else { return }
				// Update the label text
				self.phoneSubtextLabel.text = "and\nlearn"

				// Fade in the label
				UIView.animate(withDuration: 0.5, animations: {
					self.phoneSubtextLabel.alpha = 1.0
				}, completion: { (fadeInFinished) in
					guard self.currentPage == 3 else { return }
					self.animateBubbles()
				})
			}
		}
	}

	private func animateBubbles() {
		
		guard currentPage == 3 else { return }

		// Bubble 1
		UIView.animate(withDuration: 1.5, animations: {
			self.bubble1View.alpha = 1.0
		}) { (finishedBubble1) in
			guard self.currentPage == 3 else { return }
			// Bubble 2
			UIView.animate(withDuration: 1.5, animations: {
				self.bubble1View.alpha = 0.0
				self.bubble2View.alpha = 1.0
			}) { (finishedBubble2) in
				guard self.currentPage == 3 else { return }
				// Bubble 3
				UIView.animate(withDuration: 1.5, animations: {
					self.bubble2View.alpha = 0.0
					self.bubble3View.alpha = 1.0
				}) { (finishedBubble3) in
					guard self.currentPage == 3 else { return }
					// End of animations
					self.page3Button.isHidden = false
				}
			}
		}
	}

	private func setBackgroundColors() {
				if backgroundGradientLayer == nil {
			backgroundGradientLayer = CAGradientLayer()
			backgroundView.layer.addSublayer(backgroundGradientLayer!)
		}

		backgroundGradientLayer?.colors = [UIColor(hexString: "#FC466B").cgColor, UIColor(hexString: "#3F5EFB").cgColor, UIColor(hexString: "#FC466B").cgColor]
		backgroundGradientLayer?.locations = [0.0, 0.5, 1.0]
	}


	// MARK: - UIScrollViewDelegate
	func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
		print("offset: \(scrollView.contentOffset.y)")
		didScrollToPage(Int(scrollView.contentOffset.y / scrollView.bounds.height) + 1)
	}

	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		print("target: \(targetContentOffset.pointee.y)")
		didScrollToPage(Int(targetContentOffset.pointee.y / scrollView.bounds.height) + 1)
	}

	private func didScrollToPage(_ page: Int) {
		print("page: \(page)")

		currentPage = page
		switch page {
		case 1:
			resetHandAnimation()
		case 2:
			animateHand()
			resetQuestionsAnimation()
		case 3:
			resetHandAnimation()
			animatePhoneQuestions()
		case 4:
			resetQuestionsAnimation()
		default:
			break
		}
	}


	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowLogIn":
			if let navController = segue.destination as? UINavigationController {
				if let controller = navController.topViewController as? LogInViewController {
					controller.allowClose = true
				}
			}

		default:
			print("Unexpected segue identifier: \(identifier)")
		}
	}
}
