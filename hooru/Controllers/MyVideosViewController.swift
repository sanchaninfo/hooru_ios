//  hooru
//  Created by Brad Weber on 3/15/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class MyVideosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	@IBOutlet weak var tableView: UITableView!

	private var promptGroups = [PromptGroup]() {
		didSet {
			guard tableView != nil else {
				return
			}

			tableView.reloadData()
		}
	}

	private var selectedIndex: Int?


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

	func loadData() {
		guard AccountManager.currentLoggedInUser != nil else {
			promptGroups = [PromptGroup]()
			return
		}

		_ = PromptAPI.getMyVideos(completion: { (promptGroups, error) in
			DispatchQueue.main.async {
				self.promptGroups = promptGroups.filter({ (promptGroup) -> Bool in
					// Only include complete prompt groups
					return promptGroup.responseProgress == 1
					
				}).sorted(by: { (group1, group2) -> Bool in
					// Reverse chronological
					return group2.creationDate ?? Date() < group1.creationDate ?? Date()
				})
			}
		})
	}


	// MARK: - UITableViewDataSource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return promptGroups.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyVideosTableViewCell") as? MyVideosTableViewCell else {
			return UITableViewCell()
		}

		cell.promptGroup = promptGroups[indexPath.row]

		return cell
	}


	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)

		selectedIndex = indexPath.row
		performSegue(withIdentifier: "ShowPlayback", sender: self)
	}


	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowPlayback":
			guard selectedIndex != nil else {
				return
			}

			if let destination = segue.destination as? PlaybackViewController {
				destination.currentPromptGroup = promptGroups[selectedIndex!]
				selectedIndex = nil
			}

		default:
			print("Unexpected segue identifier: \(identifier)")
		}
	}
}
