//  hooru
//  Created by Brad Weber on 2/16/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class WelcomeViewController: UIViewController {
	@IBOutlet weak var welcomeLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var signUpButton: UIButton!
	@IBOutlet weak var logInButton: UIButton!

	@IBAction func logIn() {
		performSegue(withIdentifier: "ShowLogIn", sender: self)
	}

	@IBAction func signUp() {
		performSegue(withIdentifier: "ShowSignUp", sender: self)
	}


	// MARK: - View Controlller Life Cycle
	override func viewDidLoad() {
				super.viewDidLoad()
		setFontStyles()
	}

	override func viewWillAppear(_ animated: Bool) {
				super.viewWillAppear(animated)
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		if AccountManager.currentLoggedInUser != nil {
			performSegue(withIdentifier: "ShowGetStarted", sender: self)
		}
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	func setFontStyles() { 
				Design.setDefaultFont(label: welcomeLabel, weight: .thin)
		Design.setDefaultFont(label: descriptionLabel, weight: .thin)
		Design.setDefaultFont(button: signUpButton, weight: .light, size: 18)
		Design.setDefaultFont(button: logInButton, weight: .light, size: 18)
	}

	
	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowLogIn":
			if let navController = segue.destination as? UINavigationController {
				if let controller = navController.topViewController as? LogInViewController {
					controller.allowClose = true
				}
			}

		case "ShowSignUp":
			if let navController = segue.destination as? UINavigationController {
				if let controller = navController.topViewController as? AccountNameCreationViewController {
					controller.allowClose = true
				}
			}

		default:
			print("Unexpected segue identifier: \(identifier)")
		}
	}
}
