//  hooru
//  Created by Vu Dang on 2/13/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class EditProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	@IBOutlet weak var saveButton: UIButton!

	@IBOutlet weak var keyboardAdjustingScrollView: IAKeyboardAdjustingScrollView!
	@IBOutlet weak var keyboardAdjustingScrollViewBottomConstraint: NSLayoutConstraint!
	@IBOutlet weak var keyboardAdjustingContentView: UIView!

	@IBOutlet weak var profileContainerView: UIView!
	@IBOutlet weak var profileImageView: CAAvatarImageView!
	@IBOutlet weak var profileTextButton: UIButton!

	@IBOutlet weak var firstNameTextField: CATextField!
	@IBOutlet weak var lastNameTextField: CATextField!
	@IBOutlet weak var emailTextfield: CATextField!
	@IBOutlet weak var passwordTextField: CATextField!
	@IBOutlet weak var confirmPasswordTextField: CATextField!

	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!


	@IBAction func saveProfileChanges(_ sender: Any) {
				self.view.endEditing(true)

		guard accountIsValidToSave else {
			return
		}

		if firstNameTextField.sanitizedText().isEmpty {
			currentUser.firstName = nil
		} else {
			currentUser.firstName = firstNameTextField.sanitizedText()
		}

		if lastNameTextField.sanitizedText().isEmpty {
			currentUser.lastName = nil
		} else {
			currentUser.lastName = lastNameTextField.sanitizedText()
		}

		if enteredNewPassword {
			currentUser.password = passwordTextField.sanitizedText()
		} else {
			currentUser.password = nil
		}

		_ = UserAPI.updateUser(currentUser, completion: { (error) in
			guard error == nil else {
				print(" *** ERROR: \(error.debugDescription)")
				return
			}

			DispatchQueue.main.async {
				self.performSegue(withIdentifier: "unwindToHome", sender: self)
			}
		})
	}

	
	// MARK: - Variables
	var currentUser = Participant()
	private var imagePicker: UIImagePickerController?

	private var nameTextFieldsAreValid: Bool {
		return !firstNameTextField.sanitizedText().isEmpty || !lastNameTextField.sanitizedText().isEmpty
	}

	private var passwordsAreTheSame: Bool {
		return passwordTextField.sanitizedText() == confirmPasswordTextField.sanitizedText()
	}

	private var enteredNewPassword: Bool {
		return !passwordTextField.sanitizedText().isEmpty || !confirmPasswordTextField.sanitizedText().isEmpty
	}

	private var accountIsValidToSave: Bool {
		if enteredNewPassword {
			return nameTextFieldsAreValid && AccountManager.passwordsAreValid(passwordText: passwordTextField.text, confirmPasswordText: confirmPasswordTextField.text)
		} else {
			return nameTextFieldsAreValid
		}
	}

	// MARK: - View Controlller Life Cycle
	override func viewDidLoad() {
				super.viewDidLoad()

		if let user = AccountManager.currentLoggedInUser {
			self.currentUser = user
			self.fillTextfieldWithCurrentUser()
			self.emailTextfield.isUserInteractionEnabled = false
		}

		Design.setDefaultFont(button: profileTextButton, weight: .light)
		keyboardAdjustingScrollView.adjustableBottomContraint = keyboardAdjustingScrollViewBottomConstraint
	}

	override func viewWillDisappear(_ animated: Bool) {
				super.viewWillDisappear(animated)
		view.endEditing(true)
	}

	override func viewDidLayoutSubviews() {
		keyboardAdjustingScrollView.contentSize = keyboardAdjustingContentView.bounds.size
	}


	// MARK: - UI Functions
	private func fillTextfieldWithCurrentUser() {
				profileImageView.user = currentUser
		firstNameTextField.text = currentUser.firstName
		lastNameTextField.text = currentUser.lastName
		emailTextfield.text = currentUser.email
		passwordTextField.text = nil
		confirmPasswordTextField.text = nil
	}

	private func enableSaveButton() {
		saveButton.tintColor = UIColor.white
		saveButton.isEnabled = true
	}

	private func disableSaveButton() {
		saveButton.tintColor = UIColor.white.withAlphaComponent(0.5)
		saveButton.isEnabled = false
	}


	// MARK: - Segue Actions
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "fromEditProfileToImageSource" {
			guard let destination = segue.destination as? ImageSourceViewController else {
				return
			}

			destination.previousViewController = .editProfile
		}
	}

	@IBAction func unwindToEditProfile(_ segue: UIStoryboardSegue) {
		guard let source = segue.source as? ImageSourceViewController, let imageSource = source.imagePickerSource else {
			return
		}

		if imageSource == .camera {
			pickImageFromSource(.camera)
		} else if imageSource == .photoLibrary {
			pickImageFromSource(.photoLibrary)
		}
	}

	// MARK: - Textfield Delegates
	func textFieldDidBeginEditing(_ textField: UITextField) {
				keyboardAdjustingScrollView.activeField = textField
	}

	@IBAction func textFieldChanged(_ textField: CATextField) {
		
		guard accountIsValidToSave else {
			if textField == firstNameTextField || textField == lastNameTextField {
				if !nameTextFieldsAreValid {
					firstNameTextField.showError(message: "Please enter a first or last name")
					lastNameTextField.showError(message: "Please enter a first or last name")
				} else {
					firstNameTextField.hideError()
					lastNameTextField.hideError()
				}
			} else {
				if textField == passwordTextField || textField == confirmPasswordTextField {
					textField.hideError()
				}

			}
			disableSaveButton()
			return
		}

		firstNameTextField.hideError()
		lastNameTextField.hideError()
		passwordTextField.hideError()
		confirmPasswordTextField.hideError()
		enableSaveButton()
	}

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
				textField.resignFirstResponder()
		switch textField {
		case firstNameTextField:
			lastNameTextField.becomeFirstResponder()
		case lastNameTextField:
			passwordTextField.becomeFirstResponder()
		case passwordTextField:
			confirmPasswordTextField.becomeFirstResponder()
		default:
			break
		}
		return true
	}

	func textFieldDidEndEditing(_ textField: UITextField) {
		
		guard let text = textField.text, !text.sanitized().isEmpty else {
			return
		}

		switch textField {
		case passwordTextField:
			confirmPasswordTextField.becomeFirstResponder()

			// If password is not valid, show password textfield errors
			if passwordTextField.text != passwordTextField.sanitizedText() {
				passwordTextField.showError(message: "Password cannot begin or end with white spaces")
			} else if !AccountManager.passwordIsLongEnough(passwordTextField.sanitizedText()) {
				passwordTextField.showError(message: "Password must be at least 6 characters")
			}

			// Check if there is a current password in the confirmationTextField, if there is, check if whether they are the same
			if !passwordsAreTheSame && !confirmPasswordTextField.sanitizedText().isEmpty {
				confirmPasswordTextField.showError(message: "Passwords must be the same")
			} else if passwordsAreTheSame {
				passwordTextField.hideError()
				confirmPasswordTextField.hideError()
			}

		case confirmPasswordTextField:
			if confirmPasswordTextField.text != confirmPasswordTextField.sanitizedText() {
				confirmPasswordTextField.showError(message: "Password cannot begin or end with white spaces")
			} else if !passwordsAreTheSame && !passwordTextField.sanitizedText().isEmpty {
				confirmPasswordTextField.showError(message: "Passwords must be the same")
			} else if passwordsAreTheSame {
				passwordTextField.hideError()
				confirmPasswordTextField.hideError()
			}
		default:
			break
		}
	}

	// MARK: - Avatar Handling
	@IBAction func addAvatar() {
		// If the user has a choice of photo source, display the picker buttons.
		// Otherwise, pick from the default source.
		let cameraIsAvailable = UIImagePickerController.isSourceTypeAvailable(.camera)
		let photoLibraryIsAvailable = UIImagePickerController.isSourceTypeAvailable(.photoLibrary)

		switch true {
		case cameraIsAvailable && photoLibraryIsAvailable:
			performSegue(withIdentifier: "fromEditProfileToImageSource", sender: self)
		case cameraIsAvailable:
			pickImageFromSource(.camera)
		case photoLibraryIsAvailable:
			pickImageFromSource(.photoLibrary)
		default:
			break // No photo option
		}
	}

	private func pickImageFromSource(_ source: UIImagePickerControllerSourceType) {
				PhotoManager.photoAccessIsAuthorized { [weak self] (authorized) in
			guard authorized == true else {
				return
			}

			DispatchQueue.main.async(execute: {
				guard let strongSelf = self else {
					return
				}

				strongSelf.imagePicker = UIImagePickerController()
				if let imagePicker = strongSelf.imagePicker {
					imagePicker.allowsEditing = true
					imagePicker.delegate = strongSelf

					switch source {
					case .photoLibrary:
						imagePicker.sourceType = .photoLibrary
					case .camera:
						imagePicker.sourceType = .camera
						imagePicker.cameraCaptureMode = .photo
						imagePicker.cameraDevice = .front
					default:
						print("Unhandled UIImagePickerControllerSourceType in ProfileViewController.pickImageFromSource")
					}

					strongSelf.present(strongSelf.imagePicker!, animated: true, completion: nil)
				}
			})
		}
	}

	// MARK: - UIImagePickerControllerDelegate
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		
		picker.dismiss(animated: true, completion: nil)

		guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
			return
		}

		let squareImage = image.squared()

		if let data = UIImageJPEGRepresentation(squareImage, PhotoManager.jpegCompressionQuality) {
			let fileName = Avatar.filenameForData(data)
			Avatar.cacheAvatarData(data, withFileName: fileName, oldFileName: currentUser.avatarImageName)
			currentUser.avatarImageName = fileName

			profileTextButton.titleLabel?.text = "Edit profile pic"
			Design.setDefaultFont(button: self.profileTextButton, weight: .light)
			Avatar.uploadAvatarData(data, withFileName: fileName)
			profileImageView.image = squareImage
		}
	}

	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		dismiss(animated: true, completion: nil)
	}
}
