//  hooru
//  Created by Brad Weber on 3/18/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class AlbumDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	@IBOutlet weak var nameField: UITextField!
	@IBOutlet weak var tableView: UITableView!

	var album: Album? {
		didSet {
			updateDisplay()
			tableView?.reloadData()
		}
	}

	private var selectedIndex: Int?


    override func viewDidLoad() {
        super.viewDidLoad()

		updateDisplay()
		tableView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

	private func updateDisplay() {
		guard nameField != nil else {
			return
		}

		nameField.text = album?.name
	}

	@IBAction func playAll() {
		selectedIndex = nil
		performSegue(withIdentifier: "ShowPlayback", sender: self)
	}

	@IBAction func done() {
		if nameField.text != nil && album != nil && nameField.text! != album!.name {
			_ = AlbumAPI.updateName(nameField.text!, forAlbum: album!, completion: { (album, error) in
				DispatchQueue.main.async {
					self.performSegue(withIdentifier: "UnwindToAlbums", sender: self)
				}
			})

		} else {
			performSegue(withIdentifier: "UnwindToAlbums", sender: self)
		}
	}


	// MARK: - UITableViewDataSource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return album?.prompts.count ?? 0
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumCueTableViewCell", for: indexPath) as? AlbumCueTableViewCell {
			cell.prompt = album!.prompts[indexPath.row]
			cell.participant = album!.participants[indexPath.row]
			
			return cell
		}

		return UITableViewCell()
	}

	func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
		return "Remove"
	}

	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			_ = AlbumAPI.removePrompt(album!.prompts[indexPath.row], fromAlbum: album!, completion: { (updatedAlbum, error) in
				guard error == nil else {
					return
				}

				DispatchQueue.main.async {
					tableView.beginUpdates()

					self.album!.prompts.remove(at: indexPath.row)
					tableView.deleteRows(at: [indexPath], with: .fade)

					tableView.endUpdates()
				}
			})
		}
	}


	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)

		selectedIndex = indexPath.row
		performSegue(withIdentifier: "ShowPlayback", sender: self)
	}


	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowPlayback":
			if let destination = segue.destination as? PlaybackViewController {
				guard album != nil else {
					return
				}

				if selectedIndex == nil {
					// Play the whole album
					destination.album = album
				} else {
					destination.albumItem = (prompt: album!.prompts[selectedIndex!], participant: album!.participants[selectedIndex!])
				}

				selectedIndex = nil
			}

		default:
			print("Unexpected segue identifier: \(identifier)")
		}
	}
}
