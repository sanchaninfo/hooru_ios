//  hooru
//  Created by Vu Dang on 2/12/18 using Swift 4.0.
//  Copyright © 2018 Flying Spokes. All rights reserved.

import UIKit

class ImageSourceViewController: UIViewController {
    @IBOutlet weak var descriptionLabel: UILabel!

    @IBAction func backToPreviousViewController(_ sender: UIButton) {
        unwindToPreviousViewController()
    }
    
    @IBAction func cameraAsSourceSelected(_ sender: UIButton) {
        imagePickerSource = .camera
        unwindToPreviousViewController()
    }
    
    @IBAction func photoLibraryAsSourceSelected(_ sender: UIButton) {
        imagePickerSource = .photoLibrary
        unwindToPreviousViewController()
    }
    
    private func unwindToPreviousViewController() {
        guard let viewController = previousViewController else {
            print("ERROR: A previous View Controller must be specified")
            return
        }
        
        switch viewController {
        case .accountCreation:
            performSegue(withIdentifier: "unwindToAccountCreation", sender: self)
        case .editProfile:
            performSegue(withIdentifier: "unwindToEditProfile", sender: self)
        }
    }
    
    // MARK: - Variables
    
    var currentUser = Participant()
    var imagePickerSource: UIImagePickerControllerSourceType?
    var previousViewController: ViewControllerType?
    
    enum ViewControllerType {
        case editProfile, accountCreation
    }
    
    // MARK: - View Controlller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Design.setDefaultFont(label: descriptionLabel, weight: .light)
    }
}
