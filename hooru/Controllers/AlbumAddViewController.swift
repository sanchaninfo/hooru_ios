//  hooru
//  Created by Brad Weber on 3/18/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class AlbumAddViewController: UIViewController, UITextFieldDelegate {
	@IBOutlet weak var saveButton: UIButton!
	@IBOutlet weak var nameField: CATextField!

	var prompt: Prompt?

	var name: String? {
		didSet {
			updateSaveButtonState()
		}
	}

	var album: Album?

	weak var sourceController: UIViewController?


    override func viewDidLoad() {
        super.viewDidLoad()

		updateSaveButtonState()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


	private func updateSaveButtonState() {
		guard saveButton != nil else {
			return
		}

		saveButton.isEnabled = name != nil && !name!.isEmpty
	}

	private func close() {
		// Would like to be able to get presentingViewController,
		// but it is not set as I'd expect when this runs.
		let controller = sourceController ?? presentingViewController

		switch true {
		case controller is PlaybackViewController || album != nil:
			// If we came from the player or we added a new album, return
			// to the player.
			performSegue(withIdentifier: "UnwindToPlayback", sender: self)

		case controller is AlbumPickerViewController:
			performSegue(withIdentifier: "UnwindToAlbumPicker", sender: self)

		default:
			break
		}
	}

	@IBAction func cancel() {
		name = nil
		close()
	}

	@IBAction func save() {
		guard prompt != nil else {
			return
		}

		guard name != nil else {
			let alert = UIAlertController(title: "Required", message: "Album name is required.", preferredStyle: .alert)
			let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
			alert.addAction(okAction)

			present(controller: alert)

			return
		}

		if AccountManager.currentLoggedInUser != nil {
			_ = AlbumAPI.createAlbum(name!, withPrompts: [prompt!]) { (album, error) in
				DispatchQueue.main.async {
					guard album != nil else {
						let alert = UIAlertController(title: "Error", message: "Your album could not be created. Please try again.", preferredStyle: .alert)
						let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
						alert.addAction(okAction)

						self.present(controller: alert)

						return
					}

					self.album = album
					self.close()
				}
			}
		}
	}


	// MARK: - UITextFieldDelegate
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard let text = textField.text else {
			return true
		}

		let nsString = NSString(string: text)
		let newText = nsString.replacingCharacters(in: range, with: string)
		name = String(newText)

		return true
	}
}
