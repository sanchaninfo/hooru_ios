//  hooru
//  Created by Brad Weber on 3/17/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class IncomingTheyAskedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	@IBOutlet weak var introLabel: UILabel!
	@IBOutlet weak var table: UITableView!

	var promptGroups = [PromptGroup]() {
		didSet {
			table.reloadData()
			updateIntroMessage()
		}
	}

	private var selectedIndex: Int?


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


	@IBAction func unwindToIncomingTheyAsked(_ segue: UIStoryboardSegue) {

	}

	func loadData() {
		guard let user = AccountManager.currentLoggedInUser else {
			promptGroups = [PromptGroup]()
			return
		}

		_ = PromptAPI.getIncomingTheyAskedPromptGroups(completion: { (promptGroups, error) in
			DispatchQueue.main.async {
				user.hasPendingInvitations = 0 < promptGroups.count

				self.promptGroups = promptGroups.filter({ (promptGroup) -> Bool in
					return promptGroup.responseProgress < 1 // Only include incomplete prompt groups
				})
			}
		})
	}


	private func updateIntroMessage() {
		guard introLabel != nil else {
			return
		}

		guard 0 < promptGroups.count else {
			introLabel.attributedText = nil
			introLabel.text = "Waiting for friends to send new Qs to you."

			return
		}

		let lightFont = UIFont(name: "Lato-Light", size: 20) ?? UIFont.systemFont(ofSize: 20)
		let boldFont = UIFont(name: "Lato-Bold", size: 20) ?? UIFont.systemFont(ofSize: 20)
		let color = UIColor.white

		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.lineBreakMode = .byWordWrapping
		paragraphStyle.alignment = .center

		let lightAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: lightFont, NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.kern: -0.3]

		let boldAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: boldFont, NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.kern: -0.3]


		var notStartedCount = 0
		var inProgressCount = 0

		for promptGroup in promptGroups {
			let progress = promptGroup.responseProgress

			if progress == 0 {
				notStartedCount += 1
			}

			if 0 < progress && progress < 1 {
				inProgressCount += 1
			}
		}

		let attributedString = NSMutableAttributedString()
		switch true {
		case 0 < notStartedCount && 0 < inProgressCount:
			attributedString.append(NSAttributedString(string: "You have ", attributes: lightAttributes))
			attributedString.append(NSAttributedString(string: "\(inProgressCount)", attributes: boldAttributes))
			attributedString.append(NSAttributedString(string: " incomplete\n", attributes: lightAttributes))
			attributedString.append(NSAttributedString(string: "and ", attributes: lightAttributes))
			attributedString.append(NSAttributedString(string: "\(notStartedCount)", attributes: boldAttributes))
			attributedString.append(NSAttributedString(string: " unanswered Qs", attributes: lightAttributes))

		case 0 < notStartedCount:
			attributedString.append(NSAttributedString(string: "You have ", attributes: lightAttributes))
			attributedString.append(NSAttributedString(string: "\(notStartedCount)", attributes: boldAttributes))
			attributedString.append(NSAttributedString(string: " unanswered Q\(notStartedCount == 0 ? "" : "s")", attributes: lightAttributes))

		case 0 < inProgressCount:
			attributedString.append(NSAttributedString(string: "You have ", attributes: lightAttributes))
			attributedString.append(NSAttributedString(string: "\(inProgressCount)", attributes: boldAttributes))
			attributedString.append(NSAttributedString(string: " incomplete Q\(inProgressCount == 1 ? "" : "s")", attributes: lightAttributes))

		default:
			print("No Qs for the user to answer")
		}

		introLabel.attributedText = attributedString
	}


	// MARK: - UITableViewDataSource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return promptGroups.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if let cell = tableView.dequeueReusableCell(withIdentifier: "RequestTableCell", for: indexPath) as? RequestTableCell {
			cell.promptGroup = promptGroups[indexPath.row]

			return cell
		}

		return UITableViewCell()
	}


	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)
		selectedIndex = indexPath.row

		performSegue(withIdentifier: "ShowRecord", sender: self)
	}


    // MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowRecord":
			guard selectedIndex != nil else {
				return
			}

			if let controller = segue.destination as? RecordViewController {
				controller.currentPromptGroup = promptGroups[selectedIndex!]
			}

		default:
			print("Unexpected segue identifier: \(identifier)")
		}
	}
}
