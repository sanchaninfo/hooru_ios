//
//  AccountCreationViewController.swift
//  hooru
//
//  Created by Vu Dang on 11/27/17.
//  Copyright © 2017 Flying Spoke Media. All rights reserved.
//

import UIKit

class AccountCreationViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var keyboardAdjustingScrollView: IAKeyboardAdjustingScrollView!
    @IBOutlet weak var keyboardAdjustingScrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var keyboardAdjustingContentView: UIView!
    
    @IBOutlet weak var profileContainerView: UIView!
    @IBOutlet weak var profileImageView: CAAvatarImageView!
    @IBOutlet weak var profileTextButton: UIButton!
    
    @IBOutlet weak var emailTextfield: CATextField!
    @IBOutlet weak var passwordTextField: CATextField!
    @IBOutlet weak var confirmPasswordTextField: CATextField!
    
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBAction func createAccount() {
		        
        guard accountIsValidToCreate else {
            return
        }
        
        currentUser.email = emailTextfield.sanitizedText()
        currentUser.password = passwordTextField.sanitizedText()
        
        loadingIndicator.startAnimating()

		_ = UserAPI.registerUser(currentUser) { (error) in
			DispatchQueue.main.async {
				self.loadingIndicator.stopAnimating()

				guard error == nil else {
					// TODO: Alert with error
					// OLD: self.alertForError(error: error)
					return
				}

				let alert = UIAlertController(title: "Thank you!", message: "Your account has been registered.\n\nPlease verify your account by clicking the link in the email we just sent you, then come back and log in.", preferredStyle: .alert)

				alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
					DispatchQueue.main.async {
						self.performSegue(withIdentifier: "ShowLogIn", sender: self)
					}
				}))

				self.present(alert, animated: true, completion: nil)
			}
		}
	}

	/*
	func alertForError(error: APIService.APIError) {
				APIService.showAlertForError(error, withTitle: "Could Not Log In", fromController: self)
		DispatchQueue.main.async {
			self.loadingIndicator.stopAnimating()
		}
	}
	*/
	
    
    // MARK: - Variables
    var currentUser = Participant()
	private var imagePicker: UIImagePickerController?
    
    private var passwordsAreTheSame: Bool {
        return passwordTextField.sanitizedText() == confirmPasswordTextField.sanitizedText()
    }
    
    private var accountIsValidToCreate: Bool {
        return AccountManager.emailIsValid(emailTextfield.sanitizedText()) && AccountManager.passwordsAreValid(passwordText: passwordTextField.text, confirmPasswordText: confirmPasswordTextField.text)
    }

    // MARK: - View Controlller Life Cycle
    override func viewDidLoad() {
		        super.viewDidLoad()

        disableNextButton()
        Design.setDefaultFont(button: profileTextButton, weight: .light)
        keyboardAdjustingScrollView.adjustableBottomContraint = keyboardAdjustingScrollViewBottomConstraint
    }
    
    override func viewWillAppear(_ animated: Bool) {
		        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
		        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        keyboardAdjustingScrollView.contentSize = CGSize(width: keyboardAdjustingScrollView.bounds.width, height: keyboardAdjustingContentView.bounds.height)
    }
    
    // MARK: - UI Functions
    
    private func enableNextButton() {
        nextButton.tintColor = UIColor.white.withAlphaComponent(1.0)
        nextButton.isEnabled = true
    }
    
    private func disableNextButton() {
        nextButton.tintColor = UIColor.white.withAlphaComponent(0.5)
        nextButton.isEnabled = false
    }
    
    // MARK: - Segue Actions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromAccountCreationToImageSource" {
            guard let destination = segue.destination as? ImageSourceViewController else {
                return
            }
            
            destination.previousViewController = .accountCreation
        }
    }
    
    @IBAction func unwindToAccountCreation(_ segue: UIStoryboardSegue) {
		        guard let source = segue.source as? ImageSourceViewController, let imageSource = source.imagePickerSource else {
            return
        }
        
        if imageSource == .camera {
            pickImageFromSource(.camera)
        } else if imageSource == .photoLibrary {
            pickImageFromSource(.photoLibrary)
        }
    }
    
    // MARK: - Textfield Delegates
	func textFieldDidBeginEditing(_ textField: UITextField) {
				keyboardAdjustingScrollView.activeField = textField
	}

    @IBAction func textFieldChanged(_ textField: CATextField) {
                
        guard accountIsValidToCreate else {
            textField.hideError()
            disableNextButton()
            return
        }
        
        emailTextfield.hideError()
        passwordTextField.hideError()
        confirmPasswordTextField.hideError()
        enableNextButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                textField.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
                
        if let text = textField.text {
            if text.isEmpty {
                return
            }
        }
        
        switch textField {
        case emailTextfield:
            passwordTextField.becomeFirstResponder()
            if !AccountManager.emailIsValid(emailTextfield.sanitizedText()) {
                emailTextfield.showError(message: "Invalid email address")
            }
        case passwordTextField:
            confirmPasswordTextField.becomeFirstResponder()
            
            // If password is not valid, show password textfield errors
            if passwordTextField.text != passwordTextField.sanitizedText() {
                passwordTextField.showError(message: "Password cannot begin or end with white spaces")
            } else if !AccountManager.passwordIsLongEnough(passwordTextField.sanitizedText()) {
                passwordTextField.showError(message: "Password must be at least 6 characters")
            }
            
            // Check if there is a current password in the confirmationTextField, if there is, check if whether they are the same
            if !passwordsAreTheSame && !confirmPasswordTextField.sanitizedText().isEmpty {
                confirmPasswordTextField.showError(message: "Passwords must be the same")
            } else if passwordsAreTheSame {
                passwordTextField.hideError()
                confirmPasswordTextField.hideError()
            }
            
        case confirmPasswordTextField:
            if confirmPasswordTextField.text != confirmPasswordTextField.sanitizedText() {
                confirmPasswordTextField.showError(message: "Password cannot begin or end with white spaces")
            } else if !passwordsAreTheSame && !passwordTextField.sanitizedText().isEmpty {
                confirmPasswordTextField.showError(message: "Passwords must be the same")
            } else if passwordsAreTheSame {
                passwordTextField.hideError()
                confirmPasswordTextField.hideError()
            }
        default:
            print("ERROR: triggered because textfield did not fall under any of the switch cases")
        }
    }

	// MARK: - Avatar Handling
	@IBAction func addAvatar() {
				// If the user has a choice of photo source, display the picker buttons.
		// Otherwise, pick from the default source.
		let cameraIsAvailable = UIImagePickerController.isSourceTypeAvailable(.camera)
		let photoLibraryIsAvailable = UIImagePickerController.isSourceTypeAvailable(.photoLibrary)

		switch true {
		case cameraIsAvailable && photoLibraryIsAvailable:
            performSegue(withIdentifier: "fromAccountCreationToImageSource", sender: self)
		case cameraIsAvailable:
			pickImageFromSource(.camera)
		case photoLibraryIsAvailable:
            pickImageFromSource(.photoLibrary)
		default:
			break // No photo option
		}
	}

	private func pickImageFromSource(_ source: UIImagePickerControllerSourceType) {
				PhotoManager.photoAccessIsAuthorized { [weak self] (authorized) in
			guard authorized == true else {
				return
			}

            DispatchQueue.main.async(execute: {
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.imagePicker = UIImagePickerController()
                if let imagePicker = strongSelf.imagePicker {
                    imagePicker.allowsEditing = true
                    imagePicker.delegate = strongSelf
                    
                    switch source {
                    case .photoLibrary:
                        imagePicker.sourceType = .photoLibrary
                    case .camera:
                        imagePicker.sourceType = .camera
                        imagePicker.cameraCaptureMode = .photo
                        imagePicker.cameraDevice = .front
                    default:
                        print("Unhandled UIImagePickerControllerSourceType in ProfileViewController.pickImageFromSource")
                    }
                    
                    strongSelf.present(strongSelf.imagePicker!, animated: true, completion: nil)
                }
            })
		}
	}

	// MARK: - UIImagePickerControllerDelegate
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		
		picker.dismiss(animated: true, completion: nil)

		guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
			return
		}

		let squareImage = image.squared()

		if let data = UIImageJPEGRepresentation(squareImage, PhotoManager.jpegCompressionQuality) {
			let fileName = Avatar.filenameForData(data)
			Avatar.cacheAvatarData(data, withFileName: fileName, oldFileName: currentUser.avatarImageName)
			currentUser.avatarImageName = fileName

			profileTextButton.titleLabel?.text = "Edit profile pic"
			Design.setDefaultFont(button: self.profileTextButton, weight: .light)
			Avatar.uploadAvatarData(data, withFileName: fileName)
            profileImageView.image = squareImage
		}
	}

	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
				dismiss(animated: true, completion: nil)
	}
}
