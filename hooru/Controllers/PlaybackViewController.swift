//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit
import Photos

class PlaybackViewController: UIViewController, UICollectionViewDataSource, HorizontalPromptListCollectionDelegate, CuePlaybackViewDelegate {
	@IBOutlet weak var cancelButton: UIButton!
	@IBOutlet weak var senderAvatar: CAAvatarImageView!
	@IBOutlet weak var playbackTimeLabel: UILabel!
	@IBOutlet weak var videoView: CuePlaybackView!

	@IBOutlet weak var promptListContainerView: AnimatableTransitionView!
	@IBOutlet weak var promptListCollectionView: HorizontalPromptListCollectionView!

	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

	@IBAction func back(_ sender: UIButton) {
				dismiss()
	}

	var currentPromptGroup: PromptGroup? {
		didSet {
			promptListCollectionView?.reloadData()
		}
	}

	var album: Album? {
		didSet {
			promptListCollectionView?.reloadData()
		}
	}

	var albumItem: (prompt: Prompt, participant: Participant)? {
		didSet {
			promptListCollectionView?.reloadData()
		}
	}

	private var prompts: [Prompt] {
		var thePrompts = [Prompt]()

		switch true {
		case currentPromptGroup != nil:
			thePrompts = currentPromptGroup!.prompts

		case album != nil:
			thePrompts = album!.prompts

		case albumItem != nil:
			thePrompts = [albumItem!.prompt]

		default:
			// Use the default empty array
			break
		}

		return thePrompts
	}

	private var participants: [Participant] {
		var theParticipants = [Participant]()

		switch true {
		case currentPromptGroup != nil:
			// Ensure that the participant element count
			// matches the prompt element count.
			for (_, _) in prompts.enumerated() {
				guard currentPromptGroup!.sender != nil else {
					continue
				}

				theParticipants.append(currentPromptGroup!.sender!)
			}

		case album != nil:
			theParticipants = album!.participants

		case albumItem != nil:
			theParticipants = [albumItem!.participant]

		default:
			// Use the default empty array
			break
		}

		return theParticipants
	}


	var currentPromptListIndex = -1 {
		didSet {
			playVideoAtIndex(currentPromptListIndex)
			promptListCollectionView.scrollToPromptAtIndex(currentPromptListIndex)
		}
	}

	override func viewDidLoad() {
				super.viewDidLoad()

		markRequestAsRead()

		promptListCollectionView.register(UINib(nibName: AnsweringCueCell.identifier, bundle: nil), forCellWithReuseIdentifier: AnsweringCueCell.identifier)
		promptListCollectionView.sectionDelegate = self
	}

	override func viewDidAppear(_ animated: Bool) {
				super.viewDidAppear(animated)
		videoView.playbackDelegate = self
		promptListCollectionView.alpha = 1

		if currentPromptListIndex < 0 {
			currentPromptListIndex = 0
		}
	}

	override func viewWillDisappear(_ animated: Bool) {
				promptListCollectionView.alpha = 0		// since clipsToBounds is false, we need to hide it so it's not visible during transition
		videoView.playbackDelegate = nil
	}


	@IBAction func saveRequest() {
		videoView.pausePlayback()
		performSegue(withIdentifier: "ShowSaveDestinationPicker", sender: self)
	}

	@IBAction func unwindToPlayback(_ segue: UIStoryboardSegue) {
		switch segue.source.self {
		case is SaveDestinationPickerViewController:
			if let saveDestination = (segue.source as! SaveDestinationPickerViewController).saveDestination {
				switch saveDestination {
				case .cameraRoll:
					saveVideoToCameraRoll() { (success) in
						self.videoView.resumePlayback()
					}

				case .appAlbum:
					guard let user = AccountManager.currentLoggedInUser else {
						self.videoView.resumePlayback()
						return
					}

					DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
						_ = AlbumAPI.getAlbumsForUser(user, completion: { (albums, error) in
							DispatchQueue.main.async {
								if albums.count == 0 {
									// Add album
									self.performSegue(withIdentifier: "ShowAlbumAdd", sender: self)

								} else {
									// Pick album
									self.performSegue(withIdentifier: "ShowAlbumPicker", sender: self)
								}
							}
						})
					})
				}

			}

		case is AlbumAddViewController:
			if let album = (segue.source as? AlbumAddViewController)?.album {
				print("Added album \(album.name ?? "")")
			}

			playVideoAtIndex(currentPromptListIndex)

		case is AlbumPickerViewController:
			if let album = (segue.source as? AlbumPickerViewController)?.selectedAlbum {
				print("Picked album \(album.name ?? "")")
				_ = AlbumAPI.addPrompt(prompts[self.currentPromptListIndex], toAlbum: album, completion: { (album, error) in
					guard error == nil else {
						return
					}

				})
			}

			playVideoAtIndex(currentPromptListIndex)

		default:
			playVideoAtIndex(currentPromptListIndex)
			break
		}
	}

	private func saveVideoToCameraRoll(_ callback: @escaping (Bool) -> ()) {
		PhotoManager.photoAccessIsAuthorized { (authorized) in
			guard authorized else {
				return
			}

			self.loadingIndicator.startAnimating()

			VideoService.downloadFileAt(self.prompts[self.currentPromptListIndex].responseRemoteURL) { (localURL) in
				DispatchQueue.main.async {
					guard localURL != nil else {
						self.loadingIndicator.stopAnimating()
						callback(false)

						return
					}

					PHPhotoLibrary.shared().performChanges({
						PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: localURL!)
					}) { saved, error in
						DispatchQueue.main.async {
							self.loadingIndicator.stopAnimating()

							if !saved || error != nil {
								print("Error saving Q: \(error!.localizedDescription)")

								let alert = UIAlertController(title: "Error Saving", message: "The Q could not be saved. Please try again.", preferredStyle: .alert)
								let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
									callback(false)
								})
								alert.addAction(okAction)

								self.present(alert, animated: true, completion: nil)

							} else {
								callback(true)
							}

							VideoService.deleteLocalFileAt(localURL)
						}
					}
				}
			}
		}
	}

	private func refreshAvatarAtIndex(_ index: Int) {
		guard senderAvatar != nil else {
			return
		}

		guard (0 ..< participants.count).contains(index) else {
			return
		}
		
		senderAvatar.user = participants[index]
	}

	func playVideoAtIndex(_ index: Int) {
		
		// TODO: BDW - User can select no video (one left of first) and be forced
		// out of this view when trying to view the last one because index is
		// incremented below.
		guard 0 <= index else {
			return
		}

		refreshAvatarAtIndex(index)

		guard index < prompts.count, 0 < prompts.count else {
			loadingIndicator.stopAnimating()
			videoView.isHidden = true
			// back(cancelButton)
			return
		}

		loadingIndicator.startAnimating()

		let prompt = prompts[index]
		if let remoteUrl = prompt.responseRemoteURL {
			loadingIndicator.stopAnimating()
			videoView.isHidden = false
			videoView.playVideoAtUrl(remoteUrl)
		} else {
			videoView.isHidden = true
			// currentPromptListIndex += 1
		}
	}

	func markRequestAsRead() {
				guard currentPromptGroup?.id != nil else {
			return
		}

		currentPromptGroup!.isRead = true

		if AccountManager.currentLoggedInUser != nil {
			_ = PromptAPI.markPromptGroupAsRead(currentPromptGroup!) { (success, error) in
				if !success {
					print("Error marking prompt group as read")
				}
			}
		}
	}


	// MARK: - UICollectionViewDataSource methods
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return prompts.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AnsweringCueCell.identifier, for: indexPath) as? AnsweringCueCell {
			cell.question = prompts[indexPath.row].question

			return cell
		}

		return UICollectionViewCell()
	}


	// MARK: - Scroll view delegate
	func didScrollToItemIndex(_ index: Int) {
		print("Q index: \(index)")
		guard index != currentPromptListIndex else {
			return
		}
		
		currentPromptListIndex = index

		videoView.pausePlayback()
		playVideoAtIndex(index)
	}


	// MARK: - CuePlaybackViewDelegate
	func playbackView(_ playbackView: CuePlaybackView, didChangePlaybackTime playbackTime: TimeInterval) {
		let componentsFormatter = DateComponentsFormatter()
		componentsFormatter.allowedUnits = [.minute, .second]
		componentsFormatter.formattingContext = .standalone
		componentsFormatter.zeroFormattingBehavior = .pad

		playbackTimeLabel.text = componentsFormatter.string(from: playbackTime)
	}


	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowAlbumAdd":
			if let destination = segue.destination as? AlbumAddViewController {
				destination.prompt = prompts[self.currentPromptListIndex]
				destination.sourceController = self
			}

		case "ShowAlbumPicker":
			if let destination = segue.destination as? AlbumPickerViewController {
				destination.prompt = prompts[self.currentPromptListIndex]
			}

		default:
			print("Unexpected segue identifier: \(identifier)")
		}
	}
}
