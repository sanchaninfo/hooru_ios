//  hooru
//  Created by Brad Weber on 3/17/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class SaveDestinationPickerViewController: UIViewController {
	enum SaveDestination {
		case cameraRoll
		case appAlbum
	}

	var saveDestination: SaveDestination?


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

	@IBAction func close() {
		performSegue(withIdentifier: "UnwindToPlayback", sender: self)
	}

	@IBAction func pickCameraRoll() {
		saveDestination = .cameraRoll
		close()
	}

	@IBAction func pickAppAlbum() {
		saveDestination = .appAlbum
		close()
	}
}
