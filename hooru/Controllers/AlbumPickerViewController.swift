//  hooru
//  Created by Brad Weber on 3/18/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class AlbumPickerViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	@IBOutlet weak var tableView: UITableView!

	var prompt: Prompt?

	private var albums = [Album]() {
		didSet {
			guard tableView != nil else {
				return
			}

			tableView.reloadData()
		}
	}

	var selectedAlbum: Album?


    override func viewDidLoad() {
        super.viewDidLoad()

		loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


	func loadData() {
		if let user  = AccountManager.currentLoggedInUser {
			_ = AlbumAPI.getAlbumsForUser(user) { (albums, error) in
				DispatchQueue.main.async {
					guard error == nil else {
						return
					}

					self.albums = albums
				}
			}
		}

		/*
		var testAlbums = [Album]()

		var album = Album()
		album.id = 1
		album.name = "April and Gordon"
		testAlbums.append(album)

		album = Album()
		album.id = 2
		album.name = "Car Talk"
		testAlbums.append(album)

		album = Album()
		album.id = 3
		album.name = "Kids telling stories"
		testAlbums.append(album)

		album = Album()
		album.id = 4
		album.name = "Parents telling stories"
		testAlbums.append(album)

		album = Album()
		album.id = 5
		album.name = "My family memories"
		testAlbums.append(album)

		albums = testAlbums
		*/
	}


	@IBAction func unwindToAlbumPicker(_ segue: UIStoryboardSegue) {

	}


	// MARK: - UITableViewDataSource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return albums.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumTableViewCell") as? AlbumTableViewCell else {
			return UITableViewCell()
		}

		cell.album = albums[indexPath.row]

		return cell
	}


	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)

		selectedAlbum = albums[indexPath.row]
		performSegue(withIdentifier: "UnwindToPlayback", sender: self)
	}


	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowAddAlbum":
			if let destination = segue.destination as? AlbumAddViewController {
				destination.prompt = prompt
				destination.sourceController = self
			}

		default:
			print("Unexpected segue identifier: \(identifier)")
		}
	}
}
