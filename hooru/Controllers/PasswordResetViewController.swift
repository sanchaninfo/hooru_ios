//  hooru
//  Created by Vu Dang on 2/9/18 using Swift 4.0.
//  Copyright © 2018 Flying Spokes. All rights reserved.

import UIKit

class PasswordResetViewController: UIViewController {
	@IBOutlet weak var keyboardAdjustingScrollView: IAKeyboardAdjustingScrollView!
	@IBOutlet weak var keyboardAdjustingScrollViewBottomConstraint: NSLayoutConstraint!
	@IBOutlet weak var keyboardAdjustingContentView: UIView!

	@IBOutlet weak var passwordHelpTitleLabel: UILabel!
	@IBOutlet weak var passwordHelpDescriptionLabel: UILabel!
	@IBOutlet weak var emailTextField: CATextField!

	@IBAction func resetPassword(_ sender: UIButton) {
				let cleanEmail = emailTextField.sanitizedText()
		if AccountManager.emailIsValid(cleanEmail) {
			emailTextField.hideError()

			_ = UserAPI.resetPassword(email: cleanEmail, completion: { (error) in
				DispatchQueue.main.async {
					if error != nil {
						// TODO: Alert
					} else {
						self.didUserResetPassword = true
						self.performSegue(withIdentifier: "unwindToLogin", sender: self)
					}
				}
			})

		} else {
			emailTextField.showError(message: "Invalid email address")
		}
	}


	// MARK: - Variables
	var didUserResetPassword = false
	var emailAddress: String? {
		didSet {
			setEmailTextIfPossible()
		}
	}

	// MARK: - View Controlller Life Cycle
	override func viewDidLoad() {
		super.viewDidLoad()

		Design.setDefaultFont(label: passwordHelpTitleLabel, weight: .light)
		Design.setDefaultFont(label: passwordHelpDescriptionLabel, weight: .regular)

		keyboardAdjustingScrollView.adjustableBottomContraint = keyboardAdjustingScrollViewBottomConstraint

		setEmailTextIfPossible()
	}

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()

		keyboardAdjustingScrollView.contentSize = CGSize(width: keyboardAdjustingScrollView.bounds.width, height: keyboardAdjustingContentView.bounds.height)
	}

	override func viewWillDisappear(_ animated: Bool) {
				super.viewWillDisappear(animated)
		view.endEditing(true)
	}

	func setEmailTextIfPossible() {
		guard emailTextField != nil else {
			return
		}

		emailTextField.text = emailAddress
	}

	func textFieldDidBeginEditing(_ textField: UITextField) {
				keyboardAdjustingScrollView.activeField = textField
	}
}
