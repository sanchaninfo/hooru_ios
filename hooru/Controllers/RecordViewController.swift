//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit
import MobileCoreServices

// TODO: move this to its own file
class AnimatableTransitionView: UIView {
	override var isHidden: Bool {
		didSet {
			if isHidden {
				self.hide()
			} else {
				self.show()
			}
		}
	}

	var appearanceAnimnation: (() -> Void)? = nil
	var disappearanceAnimnation: (() -> Void)? = nil

	private func show() {
				UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
			self.alpha = self.appearanceAnimnation == nil ? 1 : 0
			self.appearanceAnimnation?()
		}) { (completed) in
			super.isHidden = false
		}
	}

	private func hide() {
				UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
			self.alpha = self.disappearanceAnimnation == nil ? 0 : 1
			self.disappearanceAnimnation?()
		}) { (completed) in
			super.isHidden = true
		}
	}
}

class RecordViewController: UIViewController, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, HorizontalPromptListCollectionDelegate {
	@IBOutlet weak var overlayView: UIView!
	@IBOutlet weak var overlayBackgroundView: AnimatableTransitionView!

	@IBOutlet weak var firstTimeView: AnimatableTransitionView!
	@IBOutlet weak var firstTimeHeaderLabel: UILabel!
	@IBOutlet weak var firstTimeInstructionsTextView: UITextView!
	@IBOutlet weak var firstTimeDismissButton: GiantButton!

	@IBOutlet weak var controlsView: AnimatableTransitionView!
	@IBOutlet weak var cancelButton: UIButton!
	@IBOutlet weak var cameraSwitchButton: UIButton!

	@IBOutlet weak var pausedView: AnimatableTransitionView!
	@IBOutlet weak var pausedButtonsContainerView: UIStackView!
	@IBOutlet weak var takeABreakButton: GiantButton!
	@IBOutlet weak var resumeButton: GiantButton!

	@IBOutlet weak var reRecordView: AnimatableTransitionView!
	@IBOutlet weak var reRecordButtonsContainer: UIStackView!
	@IBOutlet weak var reRecordConfirmContainer: UIStackView!
	@IBOutlet weak var reRecordConfirmTextView: UITextView!

	@IBOutlet weak var finishedView: AnimatableTransitionView!
	@IBOutlet weak var sendContainer: UIStackView!
	@IBOutlet weak var sentContainer: UIStackView!
	@IBOutlet weak var sentHeaderLabel: UILabel!
	@IBOutlet weak var sentTextView: UITextView!

	@IBOutlet weak var playbackView: CuePlaybackView!

	@IBOutlet weak var promptListContainerView: AnimatableTransitionView!
	@IBOutlet weak var promptListCollectionView: HorizontalPromptListCollectionView!

	@IBOutlet weak var senderView: UIView!
	@IBOutlet weak var senderNameLabel: UILabel!
	@IBOutlet weak var senderAvatar: UIImageView!

	@IBOutlet weak var recordingStatusLabel: UILabel!

	@IBOutlet weak var overlayBottomConstraint: NSLayoutConstraint!
	@IBOutlet weak var senderViewBottomConstaint: NSLayoutConstraint!
	@IBOutlet weak var recordingStatusLabelBottomConstraint: NSLayoutConstraint!

	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

	@IBAction func stop(_ sender: UIButton) {
		
		switch currentViewState {
        case .firstTime, .ready, .watching, .sent, .reRecord, .reRecordConfirm, .done:
			uploadPendingVideos()
			performSegue(withIdentifier: "UnwindToIncomingTheyAsked", sender: self)

		case .answering, .answeringExpired:
			// TODO: this will stop recording for the current cue. Do proper recoding pause after switching to AVCaptureDevice and AVCaptureSession
			if deviceCanRecordMovies {
				videoRecorder.stopVideoCapture()
			}
			isRecording = false
            currentViewState = .paused

		case .paused:
			break
		}
	}

	@IBAction func switchCamera(_ sender: UIButton) {
				if deviceCanRecordMovies {
			videoRecorder.cameraDevice = videoRecorder.cameraDevice == .front ? .rear : .front
		}
	}

	@IBAction func takeABreak(_ sender: GiantButton) {
				uploadPendingVideos()
		performSegue(withIdentifier: "UnwindToIncomingTheyAsked", sender: self)
	}

	@IBAction func resumeRecording(_ sender: GiantButton) {
				// TODO: this will restart recording for the current cue. Do proper recording resume after switching to AVCaptureDevice and AVCaptureSession
		currentViewState = .answering
		startRecordingForPromptIndex(promptIndex: currentPromptListIndex)
	}

	@IBAction func watch(_ sender: GiantButton) {
				currentViewState = .watching
		playVideoAtIndex(currentPromptListIndex)
	}

	@IBAction func createNewRequest(_ sender: GiantButton) {
				presentInitialController(inStoryboardFile: "Request")
	}

	@IBAction func showReadyView(_ sender: GiantButton) 	{	currentViewState = .ready	}
	@IBAction func reRecord(_ sender: GiantButton) 			{	currentViewState = .reRecordConfirm	}
	@IBAction func cancelReRecord(_ sender: GiantButton) 	{	currentViewState = .reRecord	}

	@IBAction func sendAnswers(_ sender: GiantButton) {
		
		guard currentViewState == .done else {
			return
		}

		loadingIndicator.startAnimating()
		var pendingUpdateResponsesCount = 0

		// possibly redundant updates to responses. VideoService will return immidiately if already sent
		currentPromptGroup.prompts.forEach {
			pendingUpdateResponsesCount += 1
			VideoService.updateResponseForPrompt($0, completion: { (success) in
				// ignoring errors for now
				pendingUpdateResponsesCount -= 1
				if pendingUpdateResponsesCount == 0 {
					// all responses sent
					self.currentPromptGroup.status = "Sent"
					if AccountManager.currentLoggedInUser != nil {
						_ = PromptAPI.markPromptGroupAsSent(self.currentPromptGroup) { (success, error) in
							if success {
								DispatchQueue.main.async {
									self.loadingIndicator.stopAnimating()
									self.currentViewState = .sent
								}
							}
						}
					}
				}
			})
		}
	}

	enum ViewState {
		case firstTime, ready, answering, answeringExpired, paused, done, sent, reRecord, reRecordConfirm, watching
	}

	let videoRecorder = UIImagePickerController()
	let maxVideoDuration: TimeInterval = 30 // seconds
	var videoRecordingStartTime = Date()

	var deviceCanRecordMovies: Bool {
		if UIImagePickerController.isSourceTypeAvailable(.camera),
			let availableTypes = UIImagePickerController.availableMediaTypes(for: .camera),
			availableTypes.contains(kUTTypeMovie as String) {
			return true
		} else {
			print("********** ERROR Camera not available, or movie recording not available")
			return false
		}
	}

	var currentPromptListIndex = -1			// header cell
	var firstTimeInstructions = ""
	var userIsFirstTimer: Bool {
		if let user = AccountManager.currentLoggedInUser {
			return !user.hasAnsweredAtLeastOnce
		} else {
			return true
		}
	}

	var currentViewState = ViewState.ready {
		didSet {
			DispatchQueue.main.async {
				self.updateUI()
			}
		}
	}

	var originalSenderViewBottomConstant: CGFloat = 0

	// we need this hack because even though videoRecorder.startVideoCapture() returns a bool, you can't use that to control flow (doesn't execute "false" condition).
	// Calling that when recoding is in progress resutls in an error that wipes the delegate -didFinish call, among other nasties
	var isRecording = false

	var currentPromptGroup = PromptGroup() {
		didSet {
			if userIsFirstTimer {
				setFirstTimeInstructions()
			}
			setSenderView()
		}
	}

	override func viewDidLoad() {
		        super.viewDidLoad()

		setupRecorder()

		originalSenderViewBottomConstant = senderViewBottomConstaint.constant
		overlayBackgroundView.backgroundColor = Design.Overlays.backgroundColor

		firstTimeInstructionsTextView.textContainerInset = UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 18)
		Design.setDefaultFont(label: firstTimeHeaderLabel, weight: .light)

		reRecordConfirmTextView.textContainerInset = UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 18)
		Design.setDefaultFont(textView: reRecordConfirmTextView, weight: .light)

		sentTextView.textContainerInset = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: -10)
		Design.setDefaultFont(textView: sentTextView, weight: .light)
		Design.setDefaultFont(label: sentHeaderLabel, weight: .light)

		// TODO: set up appearance/disappearance transitions: effects, movement, etc to the state-specific subviews.
		promptListCollectionView.register(UINib(nibName: AnsweringCueCell.identifier, bundle: nil), forCellWithReuseIdentifier: AnsweringCueCell.identifier)

		promptListCollectionView.register(UINib(nibName: AnsweringCueHeaderView.identifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: AnsweringCueHeaderView.identifier)

		promptListCollectionView.register(UINib(nibName: AnsweringCueFooterView.identifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: AnsweringCueFooterView.identifier)

		promptListCollectionView.sectionDelegate = self
		promptListCollectionView.allowBacktrack = true

		setSenderView()

		// set view state last
		if userIsFirstTimer {
			currentViewState = .firstTime
		} else {
			currentViewState = .ready
		}
    }

	override func viewDidAppear(_ animated: Bool) {
				super.viewDidAppear(animated)
	}

	override func viewWillDisappear(_ animated: Bool) {
				super.viewWillDisappear(animated)
	}

	override func didReceiveMemoryWarning() {
				super.didReceiveMemoryWarning()
	}

	func setupRecorder() {
		
		// TODO: switch to using AVCaptureDevice and AVCaptureSession for better control,
		// including responding to max duration elapsed (AVCaptureFileOutputRecordingDelegate fileOutput(_:didFinishRecordingTo:from:error:) )
		// and pausing and resuming

		if deviceCanRecordMovies {
			videoRecorder.sourceType = .camera
			videoRecorder.cameraDevice = .front
			videoRecorder.videoMaximumDuration = maxVideoDuration
			videoRecorder.mediaTypes = [kUTTypeMovie as String]
			videoRecorder.showsCameraControls = false
			videoRecorder.cameraOverlayView = overlayView
			videoRecorder.delegate = self
		}
	}

	func updateUI() {
		
		// set most common states, then modify as needed
		var addCamera = false
		cancelButton.isHighlighted = false	// image change from "cancel" to "pause" is tied to highlight state of the button

		[firstTimeView, controlsView, pausedView, promptListContainerView, cancelButton, senderView, recordingStatusLabel,
		 finishedView, sendContainer, sentContainer,
		 reRecordView, reRecordButtonsContainer, reRecordConfirmContainer,
		 playbackView].forEach({ $0.isHidden = true })

		senderViewBottomConstaint.constant = originalSenderViewBottomConstant
		overlayBottomConstraint.constant = 0

		let viewsToShow: [UIView]

		switch currentViewState {
		case .firstTime:
			firstTimeInstructionsTextView.text = firstTimeInstructions
			Design.setDefaultFont(textView: firstTimeInstructionsTextView, weight: .regular)
			viewsToShow = [firstTimeView, controlsView, cancelButton]

		case .ready:
			viewsToShow = [promptListContainerView, controlsView, senderView, cancelButton]
			addCamera = true

		case .answering, .answeringExpired:
			viewsToShow = [promptListContainerView, controlsView, senderView, recordingStatusLabel, cameraSwitchButton, cancelButton, recordingStatusLabel]
			cancelButton.isHighlighted = true

			let controlsBottom = view.frame.size.height - controlsView.frame.maxY
			senderViewBottomConstaint.constant = controlsBottom + recordingStatusLabel.frame.size.height
			overlayBottomConstraint.constant = controlsBottom
			recordingStatusLabelBottomConstraint.constant = controlsBottom
			addCamera = true

		case .paused:
			recordingStatusLabel.text = "Paused"
			recordingStatusLabelBottomConstraint.constant = view.frame.size.height - pausedButtonsContainerView.frame.minY + 20
			viewsToShow = [pausedView, recordingStatusLabel]

		case .reRecord:
			viewsToShow = [promptListContainerView, reRecordView, reRecordButtonsContainer, controlsView, cancelButton]

		case .reRecordConfirm:
			viewsToShow = [promptListContainerView, reRecordView, reRecordConfirmContainer, controlsView, cancelButton]

		case .watching:
			viewsToShow = [promptListContainerView, playbackView, controlsView, cancelButton]

		case .done:
			viewsToShow = [promptListContainerView, controlsView, cancelButton, finishedView, sendContainer]
			uploadPendingVideos()

		case .sent:
			viewsToShow = [cancelButton, finishedView, sentContainer]
		}

		Design.setDefaultFont(label: recordingStatusLabel, weight: .light)

		viewsToShow.forEach({ $0.isHidden = false })

		if addCamera {
			addCameraView()
		}
	}

	func playVideoAtIndex(_ index: Int) {
		
		guard index >= 0 else {
			return
		}

		guard index < currentPromptGroup.prompts.count, currentPromptGroup.prompts.count > 0 else {
			loadingIndicator.stopAnimating()
			return
		}

		loadingIndicator.startAnimating()

		let prompt = currentPromptGroup.prompts[index]
		if let remoteUrl = prompt.responseRemoteURL {
			loadingIndicator.stopAnimating()
			playbackView.playVideoAtUrl(remoteUrl)
			promptListCollectionView.scrollToPromptAtIndex(index)
		}
	}

	func addCameraView() {
				if deviceCanRecordMovies {
			self.overlayView.frame = self.videoRecorder.view.bounds
			self.overlayView.translatesAutoresizingMaskIntoConstraints = true
			self.overlayView.setNeedsLayout()
			view.addSubview(videoRecorder.view)
		}
	}

	func setFirstTimeInstructions() {
		firstTimeInstructions = "To begin, follow the prompt to swipe to the left and recording of your answer will start."
		switch currentPromptGroup.prompts.count {
		case 2:
			firstTimeInstructions += "\n\nThen swipe left to answer the remaining Qs."
		case 3...:
			firstTimeInstructions += "\n\nThen swipe left to answer each of the remaining \(currentPromptGroup.prompts.count - 1) Qs."
		default:
			break
		}

		firstTimeInstructions += "\n\nSwipe left again when done."
	}

	func setSenderView() {
		
		guard senderNameLabel != nil else {
			return
		}

		senderAvatar.layer.cornerRadius = senderAvatar.bounds.height / 2

		promptListCollectionView.requestor = currentPromptGroup.sender

		if currentPromptGroup.sender?.avatarImageName != nil {
			currentPromptGroup.sender?.avatarImage(callback: { (image) in
				self.senderAvatar.image = image
			})

			senderAvatar.isHidden = false
			senderNameLabel.isHidden = true
		} else {
			senderNameLabel.text = currentPromptGroup.sender?.fullName
			Design.setDefaultFont(label: senderNameLabel, weight: .regular)
			senderAvatar.isHidden = true
			senderNameLabel.isHidden = false
		}
	}

	func startRecordingForPromptIndex(promptIndex: Int) {
		
		guard deviceCanRecordMovies else {
			return
		}

		guard case .answering = currentViewState else {
			return
		}

		if isRecording {
			print("recording in process, queueing Q")	// will be started in -didFinishPickingMediaWithInfo
		} else {
			hideRecordingIndicatorForQuestionCellsExcept(currentPromptGroup.prompts[promptIndex].question)
			videoRecorder.view.tag = promptIndex			// keep track of which prompt the answer is for
			if videoRecorder.startVideoCapture() {
				startVideoCounter()
				isRecording = true
			}	// don't try using the else condition, it is never executed
		}
	}

	func startVideoCounter() {
				videoRecordingStartTime = Date()
		updateVideoCounter()
		animateRecordingStart()
	}

	func animateRecordingStart() {
		
		let currentBackgroundColor = controlsView.backgroundColor

		UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseIn, animations: {
			self.controlsView.backgroundColor = UIColor.green.withAlphaComponent(0.5)
		}) { (completed) in
			UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
				self.controlsView.backgroundColor = currentBackgroundColor
			}) { (completed) in
			}
		}

		UIView.animate(withDuration: 0.3, delay: 0.15, options: .curveEaseIn, animations: {
			self.recordingStatusLabel.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
		}) { (completed) in
			UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5, options: .curveEaseOut, animations: {
				self.recordingStatusLabel.transform = .identity
			}) { (completed) in
			}
		}
	}

	@objc
	func updateVideoCounter() {
		switch currentViewState {
		case .answering:
			let currentTime = Date()
			let elapsedTime = currentTime.timeIntervalSince(videoRecordingStartTime)

			let remainingTime = max(0, maxVideoDuration - elapsedTime)
			let remainingMinutes = floor(remainingTime / 60)
			let remainingSeconds = remainingTime - remainingMinutes * 60
			let secondsString = "\(remainingSeconds < 10 ? "0" : "")\(Int(remainingSeconds))"

			recordingStatusLabel.text = "\(Int(remainingMinutes)):\(secondsString)"
			if remainingTime < 1 {
				videoRecorder.stopVideoCapture()
				isRecording = false
				currentViewState = .answeringExpired
				hideRecordingIndicatorForQuestionCellsExcept()
			} else {
				perform(#selector(updateVideoCounter), with: nil, afterDelay: 1)
			}
		default:
			break
		}

		Design.setDefaultFont(label: recordingStatusLabel, weight: .light)
	}

	// MARK: - UICollectionViewDataSource
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return currentPromptGroup.prompts.count
	}

	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		switch kind {
		case UICollectionElementKindSectionHeader:
			if let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: AnsweringCueHeaderView.identifier, for: indexPath) as? AnsweringCueHeaderView {
				view.currentPromptGroup = currentPromptGroup

				return view
			}

		case UICollectionElementKindSectionFooter:
			if let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: AnsweringCueFooterView.identifier, for: indexPath) as? AnsweringCueFooterView {
				view.currentPromptGroup = currentPromptGroup

				return view
			}

		default:
			break
		}

		return UICollectionReusableView()
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AnsweringCueCell.identifier, for: indexPath) as? AnsweringCueCell {
			cell.question = currentPromptGroup.prompts[indexPath.row].question
			cell.isRecording = true

			return cell
		}
		
		return UICollectionViewCell()
	}

	// MARK: - video processing
	func exportVideoAt(url: URL, forPrompt prompt: Prompt) {
				VideoService.exportVideo(url, forPrompt: prompt, autoUpload: true, autoSend: true) { exportedVideoURL in

			if let savedPathUrl = exportedVideoURL {
				print("video saved locally to \(savedPathUrl)")
				if prompt != self.currentPromptGroup.prompts.last {
					DispatchQueue.main.async {
						if let promptIndex = self.currentPromptGroup.prompts.index(of: prompt) {
							self.startRecordingForPromptIndex(promptIndex: promptIndex + 1)
						}
					}
				}
			}
		}
	}

	func uploadPendingVideos() {
				currentPromptGroup.prompts.forEach { self.uploadVideoForPrompt($0) }
	}

	func uploadVideoForPrompt(_ prompt: Prompt) {
				VideoService.uploadVideoForPrompt(prompt, autoSend: true, completion: { (success) in
			if success {
				print("video uploaded to \(prompt.responseRemoteURL?.absoluteString ?? "")")
			}
		})
	}

	// MARK: - Scroll view delegate
	func didScrollToItemIndex(_ index: Int) {
		print("going to index \(index)")

		currentPromptListIndex = index
		if deviceCanRecordMovies {
			videoRecorder.stopVideoCapture()
		}
		hideRecordingIndicatorForQuestionCellsExcept()

		switch index {
		case -1:								// header view
			print("going to header")
			currentViewState = .ready
			isRecording = false
		case currentPromptGroup.prompts.count:			// footer view
			print("going to footer")
			currentViewState = .done
			isRecording = false
		default:
			print("going to Q index \(index)")

			let prompt = currentPromptGroup.prompts[index]

			if prompt.responseRemoteURL == nil {
				if currentViewState != .answering {
					currentViewState = .answering
				}
				startRecordingForPromptIndex(promptIndex: index)
				promptListCollectionView.scrollToPromptAtIndex(index)

			} else {
				if currentViewState != .reRecord {
					currentViewState = .reRecord
				}
			}
		}
	}

	func hideRecordingIndicatorForQuestionCellsExcept(_ question: Question? = nil) {
		
		if let cells = promptListCollectionView.visibleCells as? [AnsweringCueCell] {
			cells.forEach({ $0.isRecording = question == nil ? false : ($0.question!.id == question!.id) })
		}
	}

	// MARK: - image picker delegate
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		
		guard info[UIImagePickerControllerMediaType] as? NSString == kUTTypeMovie, let url = info[UIImagePickerControllerMediaURL] as? URL else {
			return
		}

		isRecording = false

		let promptIndex = picker.view.tag
		let prompt = currentPromptGroup.prompts[promptIndex]
		exportVideoAt(url: url, forPrompt: prompt)

		switch currentPromptListIndex {
		case -1, currentPromptGroup.prompts.count:
			break
		default:
			startRecordingForPromptIndex(promptIndex: currentPromptListIndex)
		}
	}

// If we want to save the video to the user's photo album....

//		if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path) {
//			let number = NSNumber(value: picker.view.tag)
//			let pointer = UnsafeMutablePointer<NSNumber>.allocate(capacity: 1)
//			pointer.initialize(to: number)
//			let rawPointer = UnsafeMutableRawPointer(pointer)
//			UISaveVideoAtPathToSavedPhotosAlbum(path, self, #selector(video(videoPath:didFinishSavingWithError:contextInfo:)), rawPointer)
//		}


//	@objc func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: UnsafeMutableRawPointer) {
//
//		guard error == nil else {
//			print("** Error saving movie file to \(videoPath)")
//			return
//		}
//
//		let cueNumberPointer = info.load(as: UnsafeMutablePointer<NSNumber>.self)
//		let cueNumber = cueNumberPointer.pointee as NSNumber
//		let cueIndex = cueNumber.intValue
//		let cue = currentRequest.cues[cueIndex]
//		cue.answerUrl = URL(string: videoPath as String)
//		print("cue: \(cue), path: \(videoPath)")
//
//		switch currentViewState {
//		case .answering:
//			videoRecorder.startVideoCapture()
//		default:
//			break
//		}
//	}


}
