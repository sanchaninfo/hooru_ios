//  hooru
//  Created by Brad Weber on 3/18/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class AlbumsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	@IBOutlet weak var tableView: UITableView!

	private var albums = [Album]() {
		didSet {
			guard tableView != nil else {
				return
			}

			tableView.reloadData()
		}
	}

	private var selectedIndex: Int?


	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}


	func loadData() {
		if let user = AccountManager.currentLoggedInUser {
			_ = AlbumAPI.getAlbumsForUser(user) { (albums, error) in
				DispatchQueue.main.async {
					guard error == nil else {
						return
					}

					self.albums = albums
				}
			}
		} else {
			albums = [Album]()
		}
	}

	@IBAction func unwindToAlbums(_ segue: UIStoryboardSegue) {
		if segue.source is AlbumDetailViewController {
			loadData()
		}
	}


	// MARK: - UITableViewDataSource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return albums.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumTableViewCell") as? AlbumTableViewCell else {
			return UITableViewCell()
		}

		cell.album = albums[indexPath.row]

		return cell
	}

	func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
		return "Remove"
	}

	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			_ = AlbumAPI.deleteAlbum(albums[indexPath.row], completion: { (success, error) in
				guard error == nil else {
					return
				}

				DispatchQueue.main.async {
					tableView.beginUpdates()

					self.albums.remove(at: indexPath.row)
					tableView.deleteRows(at: [indexPath], with: .fade)

					tableView.endUpdates()
				}
			})
		}
	}


	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)

		selectedIndex = indexPath.row
		performSegue(withIdentifier: "ShowAlbumDetail", sender: self)
	}


	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowAlbumDetail":
			guard selectedIndex != nil else {
				return
			}

			if let destination = segue.destination as? AlbumDetailViewController {
				destination.album = albums[selectedIndex!]
			}

		default:
			print("Unexpected segue identifier: \(identifier)")
		}
	}
}
