//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class RequestConfirmViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	@IBOutlet weak var backButton: UIButton!
	@IBOutlet weak var doneButton: UIButton!

	@IBOutlet weak var confirmationLabel: UILabel!
	@IBOutlet weak var questionListView: UICollectionView!

	@IBOutlet weak var sendLabel: UILabel!

	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!


	var currentInvitation = Invitation() {
		didSet {
			selectedRecipients = Array(currentInvitation.recipients)
			updateConfirmationMessage()
		}
	}

	var selectedRecipients = [Participant]()
	var sendInProgress = false


    override func viewDidLoad() {
		        super.viewDidLoad()

		Design.setDefaultFont(label: sendLabel, weight: .regular)

		updateConfirmationMessage()
    }

    override func didReceiveMemoryWarning() {
		        super.didReceiveMemoryWarning()
    }


	private func updateConfirmationMessage() {
		guard confirmationLabel != nil else {
			return
		}
		
		let boldFont = UIFont(name: "Lato-Bold", size: 18) ?? UIFont.boldSystemFont(ofSize: 18)
		let regularFont = UIFont(name: "Lato-Regular", size: 18) ?? UIFont.systemFont(ofSize: 18)
		let color = UIColor.white

		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.lineBreakMode = .byWordWrapping
		paragraphStyle.alignment = .center

		let boldAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: boldFont, NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.kern: -0.3]

		let regularAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: regularFont, NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.kern: -0.3]

		let intro = NSAttributedString(string: currentInvitation.questions.count == 1 ? "You are sending a Q to\n" : "You are sending \(currentInvitation.questions.count) Qs to\n", attributes: regularAttributes)

		let firstRecipient: String!
		if 0 < currentInvitation.nonContactRecipients.count {
			firstRecipient = currentInvitation.nonContactRecipients.first!
		} else {
			firstRecipient = currentInvitation.recipients.first?.fullName ?? ""
		}

		let firstRecipientString = NSAttributedString(string: firstRecipient, attributes: boldAttributes)
		let andString = NSAttributedString(string: " and ", attributes: regularAttributes)

		// Already accounted for one above
		let othersCount = currentInvitation.nonContactRecipients.count + currentInvitation.recipients.count - 1
		let othersString = NSAttributedString(string: "\(othersCount) other\(othersCount == 1 ? "" : "s")", attributes: boldAttributes)

		let combinedString = NSMutableAttributedString(attributedString: intro)
		combinedString.append(firstRecipientString)

		if 0 < othersCount {
			combinedString.append(andString)
			combinedString.append(othersString)
		}

		confirmationLabel.attributedText = combinedString
	}


	@IBAction func done() {
		
		guard AccountManager.currentLoggedInUser != nil else {
			// Prompt the user to log in or sign up
			performSegue(withIdentifier: "ShowAccountRequired", sender: self)
			return
		}

		guard !sendInProgress else {
			return
		}

		currentInvitation.sender = AccountManager.currentLoggedInUser!

		sendInProgress = true
		loadingIndicator.startAnimating()

		_ = InvitationAPI.sendInvitation(currentInvitation) { (success, error) in
			DispatchQueue.main.async {
				self.sendInProgress = false
				self.loadingIndicator.stopAnimating()

				if success {
					// we might have gotten here from "sending a response" screen, so we can't just go back
					if let presenter = self.presentingViewController as? UINavigationController, presenter.viewControllers.count > 1 {
						self.presentInitialController(inStoryboardFile: "Main")		// got here through a circuitous route
					} else {
						self.dismiss(animated: true)		// got here right from home screen
					}
				} else {
					print("InvitationAPI.sendInvitation failed")
				}
			}
		}
	}


	func unselectPromptAtIndex(index: Int) {
		
		let isLastQuestion = currentInvitation.questions.count == 1
		let addendum = isLastQuestion ? "\nYou will be taken back to select new Qs." : ""

		let alert = UIAlertController(title: "Delete Q?", message: "Are you sure you want to remove this Q?\(addendum)", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
			self.currentInvitation.questions.remove(at: index)
			if isLastQuestion {
				if let questionsController = self.navigationController?.viewControllers[0] as? QuestionSelectionViewController {
					questionsController.currentInvitation = self.currentInvitation
				}

				self.navigationController?.popToRootViewController(animated: true)

			} else {
				self.questionListView.reloadData()

				DispatchQueue.main.async {
					self.questionListView.collectionViewLayout.invalidateLayout()
				}
			}
		}))
		
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		present(alert, animated: true, completion: nil)
	}


	@IBAction func unwindToRequestConfirm(_ segue: UIStoryboardSegue) {
		if let source = segue.source as? AccountRequiredViewController {
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
				if source.prefersLogIn {
					self.performSegue(withIdentifier: "ShowLogIn", sender: self)
				} else if source.prefersSignUp {
					self.performSegue(withIdentifier: "ShowSignUp", sender: self)
				}
			})
		}
	}


	// MARK: - CollectionView methods
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return currentInvitation.questions.count
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		switch collectionView {
		case questionListView:
			let question = currentInvitation.questions[indexPath.row]
			let cellWidth = collectionView.frame.size.width - 100

			return CGSize(width: cellWidth, height: Design.Cells.heightForVerticalQuestionText(question.text ?? "", inCellWithWidth: cellWidth))

		default:
			return .zero
		}
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		print("row: \(indexPath.row)")

		switch collectionView {
		case questionListView:
			if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedQuestionCell", for: indexPath) as? SelectedQuestionCell {
				cell.indexedQuestion = (question: currentInvitation.questions[indexPath.row], index: indexPath.row)
				cell.deleteClosure = { index in
					self.unselectPromptAtIndex(index: index)
				}

				return cell
			}

		default:
			break
		}

		return UICollectionViewCell()
	}

	func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
				return false
	}


	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "ShowLogIn":
			if let navController = segue.destination as? UINavigationController {
				if let controller = navController.topViewController as? LogInViewController {
					controller.allowClose = true
				}
			}

		case "ShowSignUp":
			if let navController = segue.destination as? UINavigationController {
				if let controller = navController.topViewController as? AccountNameCreationViewController {
					controller.allowClose = true
				}
			}

		default:
			print("Unexpected segue identifier: \(identifier)")
		}
	}
}
