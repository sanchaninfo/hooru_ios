//  hooru
//  Created by Brad Weber on 3/13/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

enum SidebarNavigationItem {
	case close
	case incoming
	case myVideos
	case activity
	case albums
	case editProfile
	case logOut
}

protocol SidebarViewControllerDelegate {
	func sidebar(_ sidebar: SidebarViewController, didSelectNavigationItem item: SidebarNavigationItem)
}

class SidebarViewController: UIViewController {
	var navigationDelegate: SidebarViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

	@IBAction func close() {
		navigationDelegate?.sidebar(self, didSelectNavigationItem: .close)
	}

	@IBAction func navigateToIncoming() {
		navigationDelegate?.sidebar(self, didSelectNavigationItem: .incoming)
	}

	@IBAction func navigateToMyVideos() {
		navigationDelegate?.sidebar(self, didSelectNavigationItem: .myVideos)
	}

	@IBAction func navigateToActivity() {
		navigationDelegate?.sidebar(self, didSelectNavigationItem: .activity)
	}

	@IBAction func navigateToAlbums() {
		navigationDelegate?.sidebar(self, didSelectNavigationItem: .albums)
	}

	@IBAction func navigateToEditProfile() {
		navigationDelegate?.sidebar(self, didSelectNavigationItem: .editProfile)
	}

	@IBAction func logOut() {
		navigationDelegate?.sidebar(self, didSelectNavigationItem: .logOut)
	}
}
