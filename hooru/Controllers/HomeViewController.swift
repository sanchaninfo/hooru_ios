//  hooru
//  Created by Erwin Mazariegos on 11/27/17.
//  Copyright (c) 2017 Flying Spoke Media. All rights reserved.

import UIKit

class HomeViewController: UIViewController, SidebarViewControllerDelegate {
	enum PresentationMode {
		case activity
		case albums
		case incoming
		case myVideos

		func displayName() -> String {
			switch self {
			case .activity:
				return "Activity"
			case .albums:
				return "Albums"
			case .incoming:
				return "Incoming"
			case .myVideos:
				return "My Videos"
			}
		}
	}

	enum IncomingFilter {
		case youAsked
		case theyAsked
	}

	@IBOutlet weak var incomingToggleContainer: UIView!

	@IBOutlet weak var incomingYouAskedButton: UIButton!
	@IBOutlet weak var incomingYouAskedBackgroundView: UIView!
	@IBOutlet weak var incomingYouAskedNewIndicatorImageView: UIImageView!

	@IBOutlet weak var incomingTheyAskedButton: UIButton!
	@IBOutlet weak var incomingTheyAskedBackgroundView: UIView!
	@IBOutlet weak var incomingTheyAskedNewIndicatorImageView: UIImageView!

	@IBOutlet weak var sidebarButton: UIButton!
	@IBOutlet weak var sidebarContainerLeadingConstraint: NSLayoutConstraint!


	// Container Views
	@IBOutlet weak var incomingYouAskedContainerView: UIView!
	private weak var incomingYouAskedController: IncomingYouAskedViewController?

	@IBOutlet weak var incomingTheyAskedContainerView: UIView!
	private weak var incomingTheyAskedController: IncomingTheyAskedViewController?

	@IBOutlet weak var activityContainerView: UIView!
	private weak var activityController: ActivityViewController?

	@IBOutlet weak var albumsContainerView: UIView!
	private weak var albumsController: AlbumsViewController?

	@IBOutlet weak var myVideosContainerView: UIView!
	private weak var myVideosController: MyVideosViewController?


	@IBOutlet weak var presentationModeLabel: UILabel!
	private var presentationMode: PresentationMode = .incoming {
		didSet {
			if oldValue != presentationMode {
				updateDisplayForPresentationMode()
			}
		}
	}

	private var incomingFilter: IncomingFilter = .youAsked {
		didSet {
			if oldValue != incomingFilter {
				updateDisplayForPresentationMode()
			}
		}
	}


	@IBAction func showCreateRequest(_ sender: UIButton) {
				performSegue(withIdentifier: "ShowRequestNavigationController", sender: self)
	}

	@IBAction func unwindToHome(_ segue: UIStoryboardSegue) {  }

	var currentUser = Participant()
	var promptGroups = [PromptGroup]()

    override func viewDidLoad() {
		        super.viewDidLoad()

		NotificationCenter.default.addObserver(self, selector: #selector(handleUserChange(_:)), name: .onLoggedInUserDidChange, object: nil)

		sidebarContainerLeadingConstraint.constant = -view.bounds.width

		incomingToggleContainer.layer.cornerRadius = incomingToggleContainer.bounds.height / 2
	}

	override func viewWillAppear(_ animated: Bool) {
				super.viewWillAppear(animated)

		updateDisplayForPresentationMode()
		
		if let user = AccountManager.currentLoggedInUser {
			currentUser = user
		} else {
			hideSidebar()
			performSegue(withIdentifier: "ShowRequestNavigationController", sender: self)
		}
	}

    override func didReceiveMemoryWarning() {
		        super.didReceiveMemoryWarning()
    }


	@IBAction func selectIncomingYouAsked() {
		incomingFilter = .youAsked
	}

	@IBAction func selectIncomingTheyAsked() {
		incomingFilter = .theyAsked
	}

	private func updateDisplayForPresentationMode() {
		guard AccountManager.currentLoggedInUser != nil else {
			[presentationModeLabel, incomingToggleContainer, sidebarButton,
			 incomingYouAskedContainerView, incomingTheyAskedContainerView,
			 activityContainerView, albumsContainerView, myVideosContainerView].forEach { (theView) in
				theView?.isHidden = true
			}

			return
		}

		sidebarButton.isHidden = false
		presentationModeLabel.isHidden = false
		presentationModeLabel.text = presentationMode.displayName()

		switch presentationMode {
		case .incoming:
			incomingToggleContainer.isHidden = false

			if incomingFilter == .youAsked {
				incomingYouAskedButton.alpha = 1
				incomingYouAskedBackgroundView.backgroundColor = UIColor(hexString: "FC466B")
				incomingYouAskedBackgroundView.alpha = 1

				incomingTheyAskedButton.alpha = 0.5
				incomingTheyAskedBackgroundView.backgroundColor = UIColor.white
				incomingTheyAskedBackgroundView.alpha = 0.1

				incomingYouAskedContainerView.isHidden = false
				incomingTheyAskedContainerView.isHidden = true

				incomingYouAskedController?.loadData()

			} else {
				incomingYouAskedButton.alpha = 0.5
				incomingYouAskedBackgroundView.backgroundColor = UIColor.white
				incomingYouAskedBackgroundView.alpha = 0.1

				incomingTheyAskedButton.alpha = 1
				incomingTheyAskedBackgroundView.backgroundColor = UIColor(hexString: "FC466B")
				incomingTheyAskedBackgroundView.alpha = 1

				incomingYouAskedContainerView.isHidden = true
				incomingTheyAskedContainerView.isHidden = false

				incomingTheyAskedController?.loadData()
			}

			activityContainerView.isHidden = true
			albumsContainerView.isHidden = true
			myVideosContainerView.isHidden = true

		case .activity:
			incomingToggleContainer.isHidden = true

			incomingYouAskedContainerView.isHidden = true
			incomingTheyAskedContainerView.isHidden = true

			activityContainerView.isHidden = false
			albumsContainerView.isHidden = true
			myVideosContainerView.isHidden = true

			activityController?.loadData()

		case .albums:
			incomingToggleContainer.isHidden = true

			incomingYouAskedContainerView.isHidden = true
			incomingTheyAskedContainerView.isHidden = true

			activityContainerView.isHidden = true
			albumsContainerView.isHidden = false
			myVideosContainerView.isHidden = true

			albumsController?.loadData()

		case .myVideos:
			incomingToggleContainer.isHidden = true

			incomingYouAskedContainerView.isHidden = true
			incomingTheyAskedContainerView.isHidden = true

			activityContainerView.isHidden = true
			albumsContainerView.isHidden = true
			myVideosContainerView.isHidden = false

			myVideosController?.loadData()
		}
	}


	@IBAction func showSidebar() {
		sidebarContainerLeadingConstraint.constant = 0

		UIView.animate(withDuration: 0.5) {
			self.view.layoutIfNeeded()
		}
	}

	private func hideSidebar() {
		sidebarContainerLeadingConstraint.constant = -view.bounds.width

		UIView.animate(withDuration: 0.5) {
			self.view.layoutIfNeeded()
		}
	}


	@objc func handleUserChange(_ notification: Notification) {
		switch presentationMode {
		case .incoming:
			if incomingFilter == .youAsked {
				incomingYouAskedController?.loadData()
			} else {
				incomingTheyAskedController?.loadData()
			}

		case .activity:
			activityController?.loadData()

		case .albums:
			albumsController?.loadData()

		case .myVideos:
			myVideosController?.loadData()
		}
	}


	// MARK: - SidebarViewControllerDelegate
	func sidebar(_ sidebar: SidebarViewController, didSelectNavigationItem item: SidebarNavigationItem) {
		switch item {
		case .close:
			break

		case .activity:
			presentationMode = .activity

		case .editProfile:
			performSegue(withIdentifier: "ShowEditProfile", sender: self)

		case .incoming:
			presentationMode = .incoming

		case .albums:
			presentationMode = .albums

		case .myVideos:
			presentationMode = .myVideos

		case .logOut:
			AccountManager.logOut()
			performSegue(withIdentifier: "ShowLogIn", sender: self)
		}

		hideSidebar()
	}


	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else {
			return
		}

		switch identifier {
		case "Sidebar":
			if let destination = segue.destination as? SidebarViewController {
				destination.navigationDelegate = self
			}

		case "IncomingYouAsked":
			if let destination = segue.destination as? IncomingYouAskedViewController {
				incomingYouAskedController = destination
			}

		case "IncomingTheyAsked":
			if let destination = segue.destination as? IncomingTheyAskedViewController {
				incomingTheyAskedController = destination
			}

		case "Activity":
			if let destination = segue.destination as? ActivityViewController {
				activityController = destination
			}

		case "Albums":
			if let destination = segue.destination as? AlbumsViewController {
				albumsController = destination
			}

		case "MyVideos":
			if let destination = segue.destination as? MyVideosViewController {
				myVideosController = destination
			}

		default:
			print("Unexpected segue: \(identifier)")
		}
	}


	deinit {
		NotificationCenter.default.removeObserver(self, name: .onLoggedInUserDidChange, object: nil)
	}
}
