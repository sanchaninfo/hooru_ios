//
//  LogInViewController.swift
//  hooru
//
//  Created by Vu Dang on 11/27/17.
//  Copyright © 2017 Flying Spoke Media. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController, UITextFieldDelegate {
	@IBOutlet weak var closeButton: UIButton!

	@IBOutlet weak var keyboardAdjustingScrollView: IAKeyboardAdjustingScrollView!
	@IBOutlet weak var keyboardAdjustingScrollViewBottomConstraint: NSLayoutConstraint!
	@IBOutlet weak var keyboardAdjustingContentView: UIView!

	@IBOutlet weak var emailTextField: CATextField!
	@IBOutlet weak var passwordTextField: CATextField!
	@IBOutlet weak var loginArrowButton: UIButton!
	
	@IBOutlet weak var logInLabel: UILabel!
	@IBOutlet weak var checkEmailLabel: UILabel!
	
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!


	var allowClose = false {
		didSet {
			guard closeButton != nil else {
				return
			}

			closeButton.isHidden = !allowClose
		}
	}

	var currentUser = Participant() {
		didSet {
			setEmailTextIfPossible()
		}
	}
	
	override func viewDidLoad() {
				super.viewDidLoad()

		keyboardAdjustingScrollView.adjustableBottomContraint = keyboardAdjustingScrollViewBottomConstraint

		Design.setDefaultFont(label: logInLabel, weight: .light)
		Design.setDefaultFont(label: checkEmailLabel, weight: .thin)
		
		disableNextArrowButton()

		closeButton.isHidden = !allowClose
	}
	
	override func viewWillAppear(_ animated: Bool) {
				super.viewWillAppear(animated)
		
		if let user = AccountManager.unvalidatedUser() {
			self.currentUser = user
			self.setEmailTextIfPossible()
		}
	}

	override func viewDidAppear(_ animated: Bool) {
				super.viewDidAppear(animated)
		if !emailTextField.sanitizedText().isEmpty {
			passwordTextField.becomeFirstResponder()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
				super.viewWillDisappear(animated)
		view.endEditing(true)
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		keyboardAdjustingScrollView.contentSize = CGSize(width: keyboardAdjustingScrollView.bounds.width, height: keyboardAdjustingContentView.bounds.height)
	}


	@IBAction func close() {
		dismiss(animated: true, completion: nil)
	}

	@IBAction func logIn(_ sender: Any) {
				if loginArrowButton.isEnabled {
			loadingIndicator.startAnimating()

			_ = UserAPI.login(email: emailTextField.sanitizedText(), password: passwordTextField.sanitizedText()) { (error) in
				DispatchQueue.main.async {
					self.loadingIndicator.stopAnimating()

					guard error == nil else {
						// TODO: Alert failed login
						return
					}

					guard (AccountManager.currentLoggedInUser?.isVerified ?? false) == true else {
						// TODO: Alert account not yet verified
						return
					}

					print("currentLoggedInUser token: \(AccountManager.currentLoggedInUser?.webToken ?? "")")

					if (self.presentingViewController as? UINavigationController)?.topViewController is AccountCreationViewController {
						self.performSegue(withIdentifier: "UnwindToAccountNameCreation", sender: self)
					} else {
						self.close()
					}
				}
			}
		}
	}

	/*
	func alertForError(_ error: APIService.APIError) {
				DispatchQueue.main.async {
			self.loadingIndicator.stopAnimating()
		}
		if case APIService.APIError.users(underlyingError: .badLogin(code: .USER_NOT_VALIDATED)) = error {
			APIService.resendVerification(user: currentUser, completion: { (responseError) in
				if let newError = responseError {
					APIService.showAlertForError(newError, withTitle: "Could Not Log In", fromController: self)
				} else {
					APIService.showAlertForError(error, withTitle: "Could Not Log In", fromController: self)
				}
			})
		} else {
			APIService.showAlertForError(error, withTitle: "Could Not Log In", fromController: self)
		}
	}
	*/

	func setEmailTextIfPossible() {
				if let email = currentUser.email {
			emailTextField.text = email
		}
	}
	
	private func enableNextArrowButton() {
				loginArrowButton.isEnabled = true
		loginArrowButton.alpha = 1.0
	}
	
	private func disableNextArrowButton() {
				loginArrowButton.isEnabled = false
		loginArrowButton.alpha = 0.5
	}


	// MARK: - Segue Actions
	@IBAction func unwindToLogin(_ segue: UIStoryboardSegue) {
		guard let source = segue.source as? PasswordResetViewController else {
			return
		}
		
		if source.didUserResetPassword == true {
			checkEmailLabel.isHidden = false
		}
	}


	// MARK: - Textfield Delegates
	func textFieldDidBeginEditing(_ textField: UITextField) {
				keyboardAdjustingScrollView.activeField = textField
	}

	@IBAction func textFieldChanged(_ sender: Any) {
				if AccountManager.emailIsValid(emailTextField.sanitizedText()) && passwordTextField.sanitizedText().count > 5 {
			enableNextArrowButton()
		} else {
			disableNextArrowButton()
		}
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
				textField.resignFirstResponder()
		if textField == passwordTextField {
			logIn(loginArrowButton)
		}
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
				keyboardAdjustingScrollView.activeField = nil
		if textField == emailTextField {
			passwordTextField.becomeFirstResponder()
		}
	}


	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

	}
}

extension LogInViewController: ReauthenticationHandler {
	func reauthenticate() {
				DispatchQueue.main.async {
			let loginController = self.getController(ofClass: LogInViewController.self, inStoryboardFile: "AccountManagement")
			if let window =	UIApplication.shared.keyWindow, let rootController = window.rootViewController as? UINavigationController {
				rootController.visibleViewController?.show(controller: loginController)
			}
		}
	}
}


