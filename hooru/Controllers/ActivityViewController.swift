//  hooru
//  Created by Brad Weber on 3/15/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class ActivityViewController: UIViewController, UITableViewDataSource {
	@IBOutlet weak var tableView: UITableView!

	private var activities = [Activity]() {
		didSet {
			guard tableView != nil else {
				return
			}

			tableView.reloadData()
		}
	}


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


	func loadData() {
		guard AccountManager.currentLoggedInUser != nil else {
			activities = [Activity]()
			return
		}

		_ = ActivityAPI.getActivities(completion: { (activities, error) in
			DispatchQueue.main.async {
				self.activities = activities.sorted(by: { (activity1, activity2) -> Bool in
					let created1 = activity1.created
					let created2 = activity2.created

					switch true {
					case created1 == nil && created2 == nil:
						return true
					case created1 == nil:
						return true
					case created2 == nil:
						return false
					default:
						return created2! < created1! // Reverse chronological
					}
				})
			}
		})
	}


	// MARK: - UITableViewDataSource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return activities.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityTableViewCell") as? ActivityTableViewCell else {
			return UITableViewCell()
		}

		cell.activity = activities[indexPath.row]

		return cell
	}
}
