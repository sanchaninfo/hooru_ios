//  hooru
//  Created by Brad Weber on 3/15/18 using Swift 4.0.
//  Copyright © 2018 Flying Spoke Media. All rights reserved.

import UIKit

class AccountRequiredViewController: UIViewController {
	var prefersSignUp = false
	var prefersLogIn = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

	@IBAction func signUp() {
		prefersSignUp = true
		prefersLogIn = false

		performSegue(withIdentifier: "UnwindToRequestConfirm", sender: self)
	}

	@IBAction func logIn() {
		prefersSignUp = false
		prefersLogIn = true

		performSegue(withIdentifier: "UnwindToRequestConfirm", sender: self)
	}
}
